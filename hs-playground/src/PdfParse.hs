{-# LANGUAGE OverloadedStrings #-}
module PdfParse where

import           Control.Applicative      ((*>))
import           Control.Concurrent.Async (Concurrently (..))
import           Data.Conduit             (await, yield, (.|), runConduit)
import qualified Data.Conduit.Binary      as CB
import qualified Data.Conduit.List        as CL
import           Data.Conduit.Process     (ClosedStream (..), streamingProcess
                                          , proc, waitForStreamingProcess
                                          , Inherited(..), shell)
import qualified Data.Text                as T

delimiter = T.pack "д"

jarApp :: T.Text
jarApp = "/Users/blah/projects/tightrope-services/pdfparse/target/uberjar/pdfparse-0.1.0-SNAPSHOT-standalone.jar"

command :: T.Text -> Int -> Int -> T.Text
command fileName startPage endPage = T.unwords ["java -jar", jarApp
                                     , "-s", T.pack $ show startPage
                                     , "-e", T.pack $ show endPage
                                     , "-f", fileName]

main = do
  let fileName  = "/Users/blah/Downloads/Eli-Lilly.pdf"
      startPage = 1
      endPage   = 2
  (Inherited, src, ClosedStream, cph) <-
    streamingProcess (shell $ T.unpack $ command fileName startPage endPage)

  runConduit $ src .| CL.mapM_ print
  waitForStreamingProcess cph





{-
java -jar /Users/blah/projects/tightrope-services/pdfparse/target/uberjar/pdfparse-0.1.0-SNAPSHOT-standalone.jar -s 2 -e 4 -f /Users/blah/Downloads/Eli-Lilly.pdf
-}
