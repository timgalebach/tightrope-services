{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
module Lens where

import           Control.Lens
import qualified Data.Conduit.Binary      as CB

data Atom = Atom { _element :: String, _point :: Point } deriving (Show)
data Point = Point { _x :: Double, _y :: Double } deriving (Show)

makeLenses ''Atom
makeLenses ''Point

atom = Atom { _element = "C", _point = Point { _x = 1.0, _y = 2.0 } }
