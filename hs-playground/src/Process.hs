{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BangPatterns #-}
module Process where

import           Control.Applicative      ((*>))
import           Control.Concurrent.Async (Concurrently (..), concurrently, race)
import           Data.Conduit             (await, yield, (.|), runConduit)
import qualified Data.Conduit.Binary      as CB
import qualified Data.Conduit.List        as CL
import           Data.Conduit.Process     (ClosedStream (..), streamingProcess
                                          , proc, waitForStreamingProcess
                                          , Inherited(..), shell)
import           System.IO                (stdin)
import           Say

main :: IO ()
main = do
  putStrLn "Just wrapping cat. Use Ctrl-D to exit."

  (Inherited, src, ClosedStream, cph) <-
    streamingProcess (shell "cat Setup.hs")

  runConduit $ src .| CL.mapM_ print
  waitForStreamingProcess cph >> putStrLn "done"
