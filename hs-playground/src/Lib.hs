{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
module Lib ( someFunc ) where

import Control.Concurrent.Async.Lifted.Safe (concurrently, race, Concurrently(..), runConcurrently)
import Control.Monad.Reader (MonadReader, MonadIO, ask, liftIO, runReaderT)
import Control.Concurrent.STM (atomically, modifyTVar', TVar, newTVarIO, readTVarIO)
import Control.Concurrent (threadDelay)
import Control.Applicative ((<|>))
import Control.Exception (assert)
import Control.Lens ((^.))
import Data.Aeson (toJSON)
import Data.Void (absurd)
import Network.Wreq

someFunc :: IO ()
someFunc = putStrLn "someFunc"

counter :: IO a
counter =
    let loop i = do
            putStrLn $ "counter: " ++ show i
            threadDelay 1000000
            loop $! i + 1
     in loop 1

withCounter :: IO a -> IO a
withCounter = fmap (either absurd id) . race counter


modify :: (MonadReader (TVar Int) m, MonadIO m)
       => (Int -> Int)
       -> m ()
modify f = do
  ref <- ask
  liftIO $ atomically $ modifyTVar' ref f

main :: IO ()
main = do
  ref <- newTVarIO 4
  let action = concurrently (modify (+ 1)) (modify (+ 2))
  runReaderT action ref
  readTVarIO ref >>= print
