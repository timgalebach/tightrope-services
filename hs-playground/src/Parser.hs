module Parser where

import Text.Megaparsec
--import Text.Megaparsec.String
import Text.Megaparsec.Text
import Data.Text as T
import System.Environment

main s = do
  parseTest singleLetterP s

singleLetterP :: Parser Char
singleLetterP = char 'h' <|> char 'j'
