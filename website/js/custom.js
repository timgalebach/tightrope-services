(function ($) {
    
    // Navigation scroll
     $('.navbar-nav li a').bind('click', function(event) {
        $('.navbar-nav li').removeClass('active');
        $(this).closest('li').addClass('active');
        var $anchor = $(this);
        var nav = $($anchor.attr('href'));
        if (nav.length) {
        $('html, body').stop().animate({                
            scrollTop: $($anchor.attr('href')).offset().top             
        }, 2100, 'easeInOutExpo');
        
        event.preventDefault();
        }

        var navMain = $(".navbar-collapse"); 
        // avoid dependency on #id
        // "a:not([data-toggle])" - to avoid issues
        // when you have dropdown inside navbar
        navMain.on("click", "a:not([data-toggle])", null, function () {
        navMain.collapse('hide');
        });


    });

     // Button scroll
     $('.btn-banner').bind('click', function(event) {
        var $anchor = $(this);
        var press = $($anchor.attr('href'));
        if (press.length) {
        $('html, body').stop().animate({                
            scrollTop: $($anchor.attr('href')).offset().top             
        }, 2100, 'easeInOutExpo');
        
        event.preventDefault();
        }
    });   
    
})(jQuery);


//For shrinking navigation as you scroll

$(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');    
  } else {
    $('nav').removeClass('shrink');
  }
});

//Back to top arrow

$(window).scroll(function() {
if ($(this).scrollTop() > 200) {
  $('.back-to-top').css('display', 'block');  
  $('.back-to-top').fadeIn(200);
} else {
  $('.back-to-top').fadeOut(200);
}
});

$('.back-to-top').click(function(event) {
event.preventDefault();
$('html, body').animate({scrollTop: 0}, 2000);
});


