module DoctorInfo.Main exposing (..)

import Json.Decode     as JD exposing (int, string, float, Decoder, list, oneOf, null)
import Json.Decode.Pipeline as JDP exposing (decode, required, optional, hardcoded)
import Json.Encode     as JE
import Html              exposing (..)
import Html.Attributes   exposing (..)
import Html.Events       exposing (..)
import Http


type alias Model =
    {
        doctor  : Maybe Doctor
    ,   apiInfo : ApiInfo
    ,   error : Bool
    ,   loading : Bool
    ,   id : Int
    }

type alias ApiInfo =
    {
        token : Maybe String
    ,   url : String
    }

type alias Doctor =
    {
        id    : Int
    ,   first : String
    ,   middle : String
    ,   last : String
    ,   line1 : String
    ,   line2 : String
    ,   city : String
    ,   state : String
    ,   zip : String
    ,   lat : Float
    ,   lng : Float
    ,   payers : List Payer
    }

type alias Payer =
    {
        payerName : String
    ,   payerId   : Int
    ,   amount    : Float
    }

type Msg
    = GetDoctor Int
    | NewDoctor (Result Http.Error Doctor)

init : String -> Model
init apiUrl =
    { doctor = Nothing, apiInfo = (ApiInfo Nothing apiUrl), error = False, id = -1
    , loading = False}

updateApiToken : Maybe String -> Model -> Model
updateApiToken token model =
    { model | apiInfo = ApiInfo token model.apiInfo.url }

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        GetDoctor id -> ( { model | id = id, loading = True }, getDoctor model id )

        NewDoctor (Err err) -> ( { model | error = True, loading = False }, Cmd.none )

        NewDoctor (Ok doctor) ->
            ( { model | doctor = (Just doctor), loading = False}
            , Cmd.none )


getDoctor : Model -> Int -> Cmd Msg
getDoctor model id =
    case model.apiInfo.token of
        Nothing -> Cmd.none
        Just idToken ->
            let
                req = Http.request
                      { method = "POST"
                      , url = model.apiInfo.url ++ "/doctors/detail"
                      , headers = [Http.header "Authorization" idToken]
                      , body = Http.jsonBody <| jsonParams id
                      , expect = Http.expectJson doctorDecoder
                      , timeout = Nothing
                      , withCredentials = False
                      }
            in
                Http.send NewDoctor req

nullOr : Decoder a -> a -> Decoder a
nullOr decoder default =
    oneOf [ null default, decoder]

payerDecoder : Decoder Payer
payerDecoder =
    decode Payer
        |> JDP.required "payerName" (nullOr string "")
        |> JDP.required "payerId"   int
        |> JDP.required "amount"    float

doctorDecoder : Decoder Doctor
doctorDecoder =
    decode Doctor
        |> JDP.required "id" (nullOr int 0)
        |> JDP.required "first"  (nullOr string "")
        |> JDP.required "middle"  (nullOr string "")
        |> JDP.required "last"  (nullOr string "")
        |> JDP.required "line1"  (nullOr string "")
        |> JDP.required "line2"  (nullOr string "")
        |> JDP.required "city"  (nullOr string "")
        |> JDP.required "state"  (nullOr string "")
        |> JDP.required "zip"  (nullOr string "")
        |> JDP.required "lat"  (nullOr float 0.0)
        |> JDP.required "lng"  (nullOr float 0.0)
        |> JDP.required "payers" (JD.list payerDecoder)

jsonParams : Int -> JE.Value
jsonParams id =
    JE.object [ ("id", JE.int id) ]

loading : Model -> Html Msg
loading m = text <| if m.loading then "Loading..." else ""

showPayerLi : Payer -> Html Msg
showPayerLi p =
    li [ class "small" ]
        [ span [ style [("font-weight", "bold")]]
              [ text <| "$" ++ (toString p.amount) ++ ", " ]
        , span [] [ text p.payerName ]]

showDoctor : Doctor -> Html Msg
showDoctor d =
    div [] [
          h4 [] [ text <| d.first ++ " " ++ d.last ]
        , ul [
             style [ ("list-style-type", "none")
                   , ("padding", "2px")
                   , ("margin", "0")]
            ]
              (List.map showPayerLi d.payers)
        ]

view : Model -> Html Msg
view model =
    div [ style [("margin-top", "15px")]]
        [
          h3 [] [ text "Doctor Payment Detail" ]
        , case model.doctor of
              Nothing -> text "No doctor selected."
              Just d  -> showDoctor d
        , h6 [] [ loading model ]
        ]
