port module Tab.Ports exposing (..)

import Tab.Types exposing (Bounds, City, Doctor, Coord)

--sends map changes out
port moveCityMap : Bounds -> Cmd msg

port sendCities : List City -> Cmd msg
port sendDoctors : List Doctor -> Cmd msg

port sendCityMapCenter    : Coord -> Cmd msg
port sendDoctorMapCenter  : Coord -> Cmd msg
port sendDoctorCity : City -> Cmd msg

port sendError : String -> Cmd msg

--listens to incoming map changes
port cityMapMoved : (Bounds -> msg) -> Sub msg
port doctorMapMoved : (Bounds -> msg) -> Sub msg

port doctorClick  : (Int -> msg)  -> Sub msg
port cityMapClick : (City -> msg) -> Sub msg

--sends out when tabs are ready to be loaded
port appReady : Bounds -> Cmd msg
