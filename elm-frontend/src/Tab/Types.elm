module Tab.Types exposing (..)

import List      exposing (foldl)
import Http
import Filter.Types as FT

type alias CitiesTab =
    {
        title : String
    ,   id    : String
    ,   cities : List City
    ,   bounds : Bounds
    }

type alias DoctorsTab =
    {
        title : String
    ,   id    : String
    ,   doctors : List Doctor
    ,   bounds : Bounds
    ,   filter : FT.Model
    }

type alias DoctorTab =
    {
        title : String
    ,   id : String
    ,   doctor : Maybe Doctor
    }

type alias Bounds =
    {
        sw_lat : Float
    ,   sw_lng : Float
    ,   ne_lat : Float
    ,   ne_lng : Float
    }

type alias Coord =
    {
        lat : Float
    ,   lng : Float
    }

type alias Doctor =
    {
        id    : Int
    ,   first : String
    ,   middle : String
    ,   last : String
    ,   line1 : String
    ,   line2 : String
    ,   city : String
    ,   state : String
    ,   zip : String
    ,   lat : Float
    ,   lng : Float
    ,   amount : Float
    }

type alias City =
    {
        city : String
    ,   state : String
    ,   numDocs : Int
    ,   lat : Float
    ,   lng : Float
    }

totalDocs : List City -> Int
totalDocs = foldl (\city acc -> city.numDocs + acc) 0

type alias Model =
    {
        selected : TabId
    ,   loading : Bool
    ,   error : Bool
    ,   cities : CitiesTab
    ,   doctors : DoctorsTab
    ,   doctor : DoctorTab
    ,   apiUrl : String
    ,   idToken : Maybe String
    }

type alias UpdateConfig msg = { authErrorMsg : msg
                              , newDoctorsMsg : msg
                              , doctorClickMsg : (Int -> msg)}

type TabId
    = CitiesId
    | DoctorsId


type Msg
    = Select TabId
    | CityMapMoved Bounds
    | DoctorMapMoved Bounds
    | RefreshDoctors
    | RefreshCities
    | CityMapClick City
    | DoctorClick Int
    | GetCity String
    | FilterChanged FT.Model
    | NewCity   (Result Http.Error City)
    | NewCities (Result Http.Error (List City))
    | NewDoctors (Result Http.Error (List Doctor))

newDoctor : Maybe Doctor
newDoctor =
    Just {
            id = -1
        ,   first = ""
        ,   middle = ""
        ,   last = ""
        ,   line1 = ""
        ,   line2 = ""
        ,   city = ""
        ,   state = ""
        ,   zip = ""
        ,   lat = 0.0
        ,   lng = 0.0
        ,   amount = 0.0
        }

newCitiesTab : Bounds -> CitiesTab
newCitiesTab bounds =
    { title = "Cities", id = "cityMap", cities = [], bounds = bounds }

newDoctorsTab : Bounds -> FT.Model -> DoctorsTab
newDoctorsTab bounds filter =
    DoctorsTab "Doctors" "docMap" [] bounds filter

centerBounds : Bounds
centerBounds =
    Bounds 0 0 0 0

setCities : List City -> Model -> Model
setCities cities model =
    let
        oldCities = model.cities
        newCities = { oldCities | cities = cities}
    in
        { model | cities = newCities
        , error = False, loading = False}

setDoctors : List Doctor -> Model -> Model
setDoctors doctors model =
    let
        oldDoctors = model.doctors
        newDoctors = { oldDoctors | doctors = doctors}
    in
        { model | doctors = newDoctors
        , error = False, loading = False}

setDoctorsFilter : FT.Model -> Model -> Model
setDoctorsFilter f model =
    let oldDoctors = model.doctors
        newDoctors = { oldDoctors | filter = f }
    in { model | doctors = newDoctors, error = False, loading = False }

setDoctorsBounds : Bounds -> Model -> Model
setDoctorsBounds b model =
    let oldDoctors = model.doctors
        newDoctors = { oldDoctors | bounds = b }
    in { model | doctors = newDoctors, error = False, loading = False }

setCitiesBounds : Bounds -> Model -> Model
setCitiesBounds b model =
    let oldCities = model.cities
        newCities = { oldCities | bounds = b }
    in { model | cities = newCities, error = False, loading = False }

onP : TabId -> TabId -> Bool
onP t1 t2 = t1 == t2

goCityMsg : String -> Msg
goCityMsg cityName = GetCity cityName

return : (Model, Cmd Msg) -> (Model, Cmd Msg, Maybe superMsg)
return (model, cmd) =
    (model, cmd, Nothing)
