module Tab.View exposing (..)

import Html              exposing (..)
import Html.Attributes   exposing (..)
import Html.Events       exposing (..)
import Tab.Types         exposing (..)
import List

width = "540px"
tabTopZIndex = "10"
overlayTopZIndex = "11"

contentStyle : Attribute msg
contentStyle =
    style [ ("width", "100%")
          , ("height", "450px")
          , ("background-color", "grey")
--          , ("border-radius", "23px")
          ]

errorStyle : Attribute msg
errorStyle =
  style
    [ ("background-color", "red")
    , ("color", "white")
    , ("font-style", "italic")
    , ("width", "100%")
    ]

displayStyle : Bool -> Attribute msg
displayStyle selected =
    if selected
    then style [ ("position", "absolute")
               , ("z-index", tabTopZIndex)
               , ("width", width)
               ]
    else style [ ("position", "absolute")
               , ("z-index", "-1")
               , ("width", width)
               ]

tabContainerStyle : Attribute msg
tabContainerStyle =
    style [
         ("width", width)
        ]

tabULStyle : Attribute msg
tabULStyle =
    style [ ("list-style-type", "none")
          , ("margin", "0")
          , ("padding", "0")
          , ("overflow", "hidden")
          , ("border", "1px solid #ccc")
          , ("background-color", "#f1f1f1")
        ]

tabStyle : Attribute msg
tabStyle =
    style [ ("float", "left") ]

tabLinkStyle : Bool -> Attribute msg
tabLinkStyle selected =
    let base =
            [ ("display", "inline-block")
            ,  ("color", "black")
            ,  ("text-align", "center")
            ,  ("padding", "14px 16px")
            ,  ("text-decoration", "none")
            ,  ("transition", "0.3s")
            ,  ("font-size", "17px")
            ]
    in
        if selected
        then style (base ++ [ ("background-color", "#ccc") ])
        else style base

debugDocs model =
    div []
        [
         text <| toString model.doctors
        ]

tabWidget : Model -> Html Msg
tabWidget model =
    div [ tabContainerStyle ]
        [
          errorHeader model
--        , debugDocs model
        , tabs model
        , div []
            [
              contentDiv model.cities.id (onP model.selected CitiesId)
            , contentDiv model.doctors.id (onP model.selected DoctorsId)
            , loadingOverlayDiv model.loading
            ]]

tabs : Model -> Html Msg
tabs model =
    ul [ tabULStyle ]
        [ tab (onP model.selected CitiesId) model.cities.title (Select CitiesId)
        , tab (onP model.selected DoctorsId) model.doctors.title (Select DoctorsId) ]

tab : Bool -> String -> Msg -> Html Msg
tab selected title selectMsg =
    li [ tabStyle ] [ a [ tabLinkStyle selected
                        , onClick selectMsg
                        , href "javascript:void(0)" ]
                          [ text title ]]

loadingOverlayDiv : Bool -> Html Msg
loadingOverlayDiv selected =
    div [ style [("width", width)
                , ("padding-top", "210px")
                , ("padding-left", "260px")
                , ("height", "450px")
                , ("background-color", "grey")
                , ("opacity", "0.8")
                , ("position", "absolute")
                , ("z-index", if selected then overlayTopZIndex else "-1")]
        ] [ loadingIcon selected ]

contentDiv : String -> Bool -> Html Msg
contentDiv tabId selected =
    div [ id tabId
        , contentStyle
        , displayStyle selected ] []

errorHeader model =
    div [ errorStyle ] [ if model.error
                         then text "Move map to retry connection..."
                         else text ""]

loadingIcon loadingP =
    div [] [ if loadingP
             then img [ src "loading.gif"
                      , style [("width", "20px")]] []
             else span [] [] ]
