module Tab.Rest exposing (..)

import Http
import Json.Decode exposing (int, string, float, Decoder, list, oneOf, null)
import Json.Decode.Pipeline exposing (decode, required, optional, hardcoded)
import Json.Encode     as JE
import Task
import Tab.Ports       exposing (sendError)
import Tab.Types       exposing (..)
import Filter.Types    as FT
import Filter.Main     as FM
import Regex           exposing (regex, contains)
--import Login.State exposing (logout)

--expired token for testing
token = "eyJraWQiOiJOZWxKTkRqUk9mcVNmekQrSnlwNWlOWU5kK2pTVGJWWURMcWtFQys5UERNPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJmZGE3NTQzYi04NTFjLTRmM2UtOTI5Zi1jZWVmZDFhZTA3MzUiLCJhdWQiOiI0bjgzcjJxaGNubHUzbWhkcmNvMm5tanFzIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNDgyMjA2NTQxLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0xLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMV9CVzI1ZU1IOXYiLCJwaG9uZV9udW1iZXJfdmVyaWZpZWQiOnRydWUsImNvZ25pdG86dXNlcm5hbWUiOiJ0aW10aW1lIiwicGhvbmVfbnVtYmVyIjoiKzE0MjQ1MjcxODI4IiwiZXhwIjoxNDgyMjEwMTQxLCJpYXQiOjE0ODIyMDY1NDEsImVtYWlsIjoidGltZ2FsZWJhY2hAZ21haWwuY29tIn0.Q3FEXn0d9z3_5V111U4GZJKvqbWYE-ccw3wW4UP8v0wTj1jYY8qHL0QxiXgawSfocBggncdCuQgISVhACmJVn4acuMfZRWKUF8B_q5UMb744fTjRTnGZ_oA7-QWksCPNT-cSi8qGrKaaZ04GCWk8kExOeMvdjhGFJyBBcH6hdvYQqdNyPNQkyL3cvsh60thC5O9brKUzL8T2ncTQdVXvDSxygxJHFRqTt1kSPHXKTroZeg0zCVQhKdhtZFkwI_0moDS3AH-YTZoY66A9omGwQysMK8qaaFfQ5dei7yDdRUuQKRgFWPrgMMmpXQDayiftOdMLnI4jxaDyZ-snV9vtfA"

exposeHttpError : Model -> String -> (Model, Cmd msg)
exposeHttpError model msg =
    ( { model | error = True, loading = False}
    , sendError <| msg)

handleHttpError : UpdateConfig a -> Model -> Http.Error -> (Model, Cmd Msg, Maybe a)
handleHttpError ucfg model error =
    case error of
        Http.BadPayload msg _ ->
            return <| exposeHttpError model msg

        Http.BadStatus response ->
            return <|
                exposeHttpError model <| "Code: " ++ (toString response.status.code)

        --treat the below as an auth error
        Http.NetworkError ->
            (model, Cmd.none, Just ucfg.authErrorMsg)

        _ -> return <| exposeHttpError model "other error"


getCities : String -> Bounds -> String -> Cmd Msg
getCities apiUrl bounds token =
    let
        req = Http.request
              { method = "POST"
              , url = apiUrl ++ "/city/bybounds"
              , headers = [Http.header "Authorization" token]
              , body = Http.jsonBody <| jsonCityByBounds bounds
              , expect = Http.expectJson (list cityDecoder)
              , timeout = Nothing
              , withCredentials = False
              }
    in
        Http.send NewCities req

getCity : String -> String -> String -> Cmd Msg
getCity apiUrl cityName token =
    let
        req = Http.request
              { method = "POST"
              , url = apiUrl ++ "/city/byname"
              , headers = [Http.header "Authorization" token]
              , body = Http.jsonBody <| jsonCityByName cityName
              , expect = Http.expectJson cityDecoder
              , timeout = Nothing
              , withCredentials = False
              }
    in
        Http.send NewCity req

getDoctorsBounds : String -> Bounds -> FT.Model -> String -> Cmd Msg
getDoctorsBounds apiUrl bounds filter token =
    let
        limit = 100
        req = Http.request
              { method = "POST"
              , url = apiUrl ++ "/doctors/bybounds"
              , headers = [Http.header "Authorization" token]
              , body = Http.jsonBody <| jsonParams bounds limit filter
              , expect = Http.expectJson (list doctorDecoder)
              , timeout = Nothing
              , withCredentials = False
              }
    in
        Http.send NewDoctors req


cityDecoder : Decoder City
cityDecoder =
    decode City
        |> required "city" string
        |> required "state" string
        |> required "num_docs" int
        |> required "lat" float
        |> required "lng" float

doctorDecoder : Decoder Doctor
doctorDecoder =
    decode Doctor
        |> required "id" (nullOr int 0)
        |> required "first"  (nullOr string "")
        |> required "middle"  (nullOr string "")
        |> required "last"  (nullOr string "")
        |> required "line1"  (nullOr string "")
        |> required "line2"  (nullOr string "")
        |> required "city"  (nullOr string "")
        |> required "state"  (nullOr string "")
        |> required "zip"  (nullOr string "")
        |> required "lat"  (nullOr float 0.0)
        |> required "lng"  (nullOr float 0.0)
        |> required "amount" (nullOr float 0.0)

nullOr : Decoder a -> a -> Decoder a
nullOr decoder default =
    oneOf [ null default, decoder]

latLngString : Bounds -> String
latLngString b =
    "?sw_lat=" ++ (toString b.sw_lat) ++
    "&sw_lng=" ++ (toString b.sw_lng) ++
    "&ne_lat=" ++ (toString b.ne_lat) ++
    "&ne_lng=" ++ (toString b.ne_lng)

cityString : Bounds -> String
cityString b =
    "/city" ++ (latLngString b)

oneCityString : String -> String
oneCityString cityName =
    "/city/byname?name=" ++ cityName

jsonCityByBounds : Bounds -> JE.Value
jsonCityByBounds b =
    JE.object [("bounds", JE.list (List.map JE.float
                                       [b.sw_lat, b.sw_lng, b.ne_lat, b.ne_lng]))]

jsonCityByName : String -> JE.Value
jsonCityByName cityName =
    JE.object [ ("cityName", JE.string cityName) ]

jsonParams : Bounds -> Int -> FT.Model -> JE.Value
jsonParams b limit filter =
    let (pMin, pm) = FM.paymentRange filter
        pMax = if pm == (FM.rangeMax filter) then 1800899000 else pm
    in JE.object
        [
         ("bounds",
              JE.list (List.map JE.float [b.sw_lat, b.sw_lng, b.ne_lat, b.ne_lng]))
        , ("payRange", JE.list [JE.int pMin, JE.int pMax])
        , ("limit" , JE.int limit)
        , ("excludePayers", JE.list
               <| List.map (JE.int << .id) (FM.excludePayers filter))
        , ("includePayers", JE.list
               <| List.map (JE.int << .id) (FM.includePayers filter))
        ]
