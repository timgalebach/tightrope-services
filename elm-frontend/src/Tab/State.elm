module Tab.State exposing (..)

import Tab.Ports exposing (moveCityMap, cityMapMoved, sendCities, sendDoctors
                          , appReady ,cityMapClick, sendDoctorCity, doctorMapMoved
                          , doctorClick, sendCityMapCenter, sendDoctorMapCenter)
import Tab.Types exposing (..)
import Tab.Rest  exposing (..)
import Filter.Types as FT
import List
import Http

subscriptions : (Msg -> superMsg) -> Sub superMsg
subscriptions superConstructor =
    Sub.batch [
          cityMapMoved (superConstructor << CityMapMoved)
        , cityMapClick (superConstructor << CityMapClick)
        , doctorClick  (superConstructor << DoctorClick)
        , doctorMapMoved (superConstructor << DoctorMapMoved)
        ]

init : String -> FT.Model -> ( Model, Cmd Msg )
init apiUrl filter =
    let
        square = Bounds 0 0 0 0
    in
        {   selected = CitiesId
        ,   loading = False
        ,   error = False
        ,   doctors = newDoctorsTab square filter
        ,   doctor = DoctorTab "Current Doctor" "currDoctor" newDoctor
        ,   cities = newCitiesTab square
        ,   apiUrl = apiUrl
        ,   idToken = Nothing
        } ! [ appReady square ]

numDocs : Model -> Int
numDocs model =
    List.length model.doctors.doctors

selectTab : TabId -> Model -> Model
selectTab tabId model =
    { model | selected = tabId }

updateApiToken token model = { model | idToken = token }

runCmdWithToken : UpdateConfig authMsg -> Model -> (String -> Cmd Msg)
             -> (Model, Cmd Msg, Maybe authMsg)
runCmdWithToken ufcg model tokenCmdToRun =
    case model.idToken of
        Nothing -> (model, Cmd.none, Just ufcg.authErrorMsg)
        Just token ->
            ( { model | loading = True }, tokenCmdToRun token, Nothing )

toggleLoad model = { model | loading = not model.loading }

update : UpdateConfig authMsg -> Msg -> Model -> (Model, Cmd Msg, Maybe authMsg)
update ufcg msg model =
    case msg of
        Select tabId ->
            ( selectTab tabId model, Cmd.none, Nothing)

        GetCity cityName -> runCmdWithToken ufcg model
                            (getCity model.apiUrl cityName)

        RefreshCities -> runCmdWithToken ufcg model (getCities model.apiUrl
                                                         model.cities.bounds)

        RefreshDoctors -> runCmdWithToken ufcg model
                          (getDoctorsBounds model.apiUrl model.doctors.bounds
                               model.doctors.filter)

        CityMapMoved newBounds ->
            update ufcg RefreshCities (setCitiesBounds newBounds model)

        DoctorMapMoved newBounds ->
            update ufcg RefreshDoctors (setDoctorsBounds newBounds model)

        FilterChanged newFilter ->
            update ufcg RefreshDoctors (setDoctorsFilter newFilter model)

        CityMapClick city ->
            (selectTab DoctorsId model, sendDoctorCity city, Nothing)

        DoctorClick id ->
            (model, Cmd.none, Just (ufcg.doctorClickMsg id))

        NewCity (Ok city) ->
            case model.selected of
                CitiesId  ->
                    ( toggleLoad model, sendCityMapCenter (Coord city.lat city.lng)
                    , Nothing )
                DoctorsId ->
                    ( toggleLoad model, sendDoctorMapCenter (Coord city.lat city.lng)
                    , Nothing )


        NewCity (Err err) ->
            handleHttpError ufcg model err

        NewCities (Ok cityList) ->
            ( setCities cityList model, sendCities cityList, Nothing )

        NewCities (Err err) ->
            handleHttpError ufcg model err

        NewDoctors (Ok docList) ->
            ( setDoctors docList model, sendDoctors docList
            , Just ufcg.newDoctorsMsg )

        NewDoctors (Err err) ->
            handleHttpError ufcg model err
