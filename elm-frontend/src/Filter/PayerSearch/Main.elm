module Filter.PayerSearch.Main exposing (..)

import Html                 exposing (..)
import Html.Attributes as H exposing (..)
import Html.Events          exposing (on, onClick, onInput)
import Http
import Autocomplete
import Json.Encode     as JE
import Json.Decode as JD exposing (int, string, float, Decoder, oneOf, null)
import Json.Decode.Pipeline as JDP exposing (decode, optional, hardcoded)


type alias Model =
    {
        payers : List Payer
    ,   selectedPayers : List Payer
    ,   howManyToShow : Int
    ,   query : String
    ,   error : Bool
    ,   autoState : Autocomplete.State
    ,   apiInfo : ApiInfo
    ,   placeholder : String
    }

type alias Payer =
    {
        id   : Int
    ,   name : String
    }

type alias ApiInfo =
    {
        token : Maybe String
    ,   url : String
    }

type Msg
    = SetAutoState Autocomplete.Msg
    | SelectPayer String
    | SetQuery String
    | RemovePayer Payer
    | PayerCompletions (Result Http.Error (List Payer))

updateConfig : Autocomplete.UpdateConfig Msg Payer
updateConfig =
    Autocomplete.updateConfig
        { toId = .name
        , onKeyDown =
            \code maybePayerName ->
                if code == 13 then
                    Maybe.map SelectPayer maybePayerName
                else
                    Nothing
        , onTooLow = Nothing
        , onTooHigh = Nothing
        , onMouseEnter = \_ -> Nothing
        , onMouseLeave = \_ -> Nothing
        , onMouseClick = \payerName -> Just <| SelectPayer payerName
        , separateSelections = False
        }

init : String -> String -> Model
init apiUrl ph =
    { payers = [], autoState = Autocomplete.empty
    , howManyToShow = 5, query = "", error = False
    , apiInfo = ApiInfo Nothing apiUrl
    , selectedPayers = []
    , placeholder = ph
    }

updateApiToken : Maybe String -> Model -> Model
updateApiToken token model =
    { model | apiInfo = ApiInfo token model.apiInfo.url }

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetAutoState autoMsg ->
            let ( newState, maybeMsg ) =
                    Autocomplete.update updateConfig autoMsg
                        model.howManyToShow model.autoState model.payers
                newModel = { model | autoState = newState }
            in case maybeMsg of
                   Nothing -> (newModel, Cmd.none)
                   Just updateMsg -> update updateMsg newModel

        SetQuery newQuery -> resetMenu model newQuery

        PayerCompletions (Err err) ->
            ( { model | error = True }, Cmd.none )

        PayerCompletions (Ok payerList) ->
            ( { model | payers = payerList, error = False }, Cmd.none )

        SelectPayer payerName -> addPayer model payerName

        RemovePayer payer ->
            { model | selectedPayers = List.filter (\p -> p /= payer)
                                       model.selectedPayers } ! []

payerByName : String -> List Payer -> Payer
payerByName pName =
    (Maybe.withDefault (Payer -1 "none"))
    << List.head
    << (List.filter (\payer -> pName == payer.name))

addPayer : Model -> String -> (Model, Cmd Msg)
addPayer model payerName =
    let newP = payerByName payerName model.payers
        newPayersList = model.selectedPayers ++ [newP]
    in ({ model | selectedPayers = newPayersList, query = "", payers = []}
       , Cmd.none)

setQuery : Model -> String -> Model
setQuery model query =
    { model | query = query, payers = [] }

resetMenu : Model -> String -> ( Model, Cmd Msg )
resetMenu model query =
    case query of
        "" -> ( { model | query = "", payers = [] }, Cmd.none)
        _  -> ({ model | query = query }
              , getPayerCompletions model.apiInfo query )

getPayerCompletions : ApiInfo -> String -> Cmd Msg
getPayerCompletions apiInfo query =
    case apiInfo.token of
        Nothing -> Cmd.none
        Just token ->
            let req =
                    Http.request
                        { method = "POST"
                        , url = apiInfo.url ++ "/payers/search"
                        , headers = [Http.header "Authorization" token]
                        , body = Http.jsonBody <| jsonPayerSearch query
                        , expect = Http.expectJson (JD.list payerDecoder)
                        , timeout = Nothing
                        , withCredentials = False
                        }
            in Http.send PayerCompletions req

payerDecoder : Decoder Payer
payerDecoder =
    decode Payer
        |> JDP.required "payerId" int
        |> JDP.required "payerName" string

jsonPayerSearch : String -> JE.Value
jsonPayerSearch payerQuery =
    JE.object [ ("payerName", JE.string payerQuery) ]

viewConfig : Autocomplete.ViewConfig Payer
viewConfig =
  let
    customizedLi keySelected mouseSelected payer =
      { attributes = [ classList [ ("autocomplete-item", True), ("is-selected", keySelected || mouseSelected) ] ]
      , children = [ Html.text <| .name payer ]
      }
  in
    Autocomplete.viewConfig
      { toId = .name
      , ul = [ class "autocomplete-list" ] -- set classes for your list
      , li = customizedLi -- given selection states and a person, create some Html!
      }

view : Model -> Html Msg
view model =
    div [] [
          div [ class "col-md-6" ]
              [
               input [ onInput SetQuery, placeholder model.placeholder
                     , value model.query ] []
              , Html.map
                  SetAutoState (Autocomplete.view viewConfig model.howManyToShow
                                    model.autoState model.payers)
              ]
        , div [ class "col-md-6" ]
              [
               ul [ style [ ("list-style-type", "none")
                          , ("padding", "2px")
                          , ("margin", "0")
                          ]
                  , class "small" ]
                   (List.indexedMap selectedPayerLi model.selectedPayers)
              ]
        ]

selectedPayerLi : Int -> Payer -> Html Msg
selectedPayerLi index p =
    li [ stripe index ]
        [
          text p.name
        , span [ onClick (RemovePayer p) ]
            [ a [ href "#" ] [ text "remove" ]]
        ]

stripe : Int -> Attribute msg
stripe index =
    if index % 2 == 0
    then style [ ("background-color", "#0099ff")
               , ("list-style-position", "inside")
               , ("border", "1px solid white")
               , ("color", "white")]
    else style [ ("background-color", "#ff6666")
               , ("list-style-position", "inside")
               , ("border", "1px solid white")
               , ("color", "white")]
