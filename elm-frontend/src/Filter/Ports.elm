port module Filter.Ports exposing (..)

import Filter.Types exposing (..)

port toJsPayment : (Int, Int) -> Cmd msg
port fromJsPayment : ((Int, Int, Int) -> msg) -> Sub msg

--just pass null to it: only for initializing
port initFilters : (Maybe Int -> msg) -> Sub msg
