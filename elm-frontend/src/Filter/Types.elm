module Filter.Types exposing (..)

import Filter.PayerSearch.Main as PSM

--;;;Types
type alias Model =
    {
        paymentMin : Int
    ,   paymentMax : Int
    ,   rangeMax : Int
    ,   exclude : PSM.Model
    ,   include : PSM.Model
    }

type Msg
    = PaymentRange (Int, Int, Int)
    | ApplyFilters
    | InitFilters (Maybe Int)
    | ExcludeMsg PSM.Msg
    | IncludeMsg PSM.Msg

type ReturnMsg
    = ReturnApplyFilters
    | ReturnNone
