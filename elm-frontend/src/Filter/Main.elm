module Filter.Main exposing (..)

import Filter.Ports             exposing (..)
import Filter.Types             exposing (..)
import Html                     exposing (..)
import Response                 exposing (mapCmd, mapModel)
import Html.Attributes as H     exposing (..)
import Html.Events              exposing (on, onClick)
import Filter.PayerSearch.Main  as PSM

--;;;State
subscriptions : (Msg -> superMsg) -> Sub superMsg
subscriptions superConstructor =
    Sub.batch [
          fromJsPayment (superConstructor << PaymentRange)
        , initFilters  (superConstructor << InitFilters)
        ]

init : String -> Model
init apiUrl =
    {
        paymentMin = 0
    ,   paymentMax = 0
    ,   rangeMax = 0
    ,   exclude = PSM.init apiUrl "Company to exclude"
    ,   include = PSM.init apiUrl "Company to include"
    }

updateApiToken : Maybe String -> Model -> Model
updateApiToken token model =
    { model | exclude = PSM.updateApiToken token model.exclude
    , include = PSM.updateApiToken token model.include }

paymentRange model = (model.paymentMin, model.paymentMax)
rangeMax     model = model.rangeMax

excludePayers model = model.exclude.selectedPayers
includePayers model = model.include.selectedPayers

update : Msg -> Model -> (Model, Cmd Msg, ReturnMsg)
update msg model =
    case msg of
        PaymentRange (pMin, pMax, rangeMax) ->
            return <| ( { model | paymentMin = pMin, paymentMax = pMax
                        , rangeMax = rangeMax}, Cmd.none )

        ApplyFilters ->
            ( model, Cmd.none, ReturnApplyFilters )

        InitFilters _ -> update ApplyFilters model

        ExcludeMsg psmsg ->
            return (PSM.update psmsg model.exclude
                   |> mapModel (\x -> { model | exclude = x })
                   |> mapCmd   ExcludeMsg)

        IncludeMsg psmsg ->
            return (PSM.update psmsg model.include
                   |> mapModel (\x -> { model | include = x })
                   |> mapCmd   IncludeMsg)

--;;;Views

eltStyle : Attribute msg
eltStyle =
    style [ ("margin", "5px") ]

view : Model -> Html Msg
view model =
    div [ style [("background-color", "#f2f2f2")]]
        [ h3 [] [ text <| "Filter Doctors"
                ]
        , span [] [ text <| "$" ++ (toString model.paymentMin) ++ ", "
                        ++ "$" ++ (toString model.paymentMax)
                  ]
        , div [ id "paymentSlider"
              , style [("margin-bottom", "35px")]] []
        , div [ eltStyle, class "col-md-12" ]
            [ PSM.view model.exclude |> Html.map ExcludeMsg ]
        , div [ eltStyle, class "col-md-12" ]
            [ PSM.view model.include |> Html.map IncludeMsg ]
        , div [ eltStyle, class "col-md-12" ]
            [ button [ class "btn btn-primary"
                     , onClick ApplyFilters ]
                  [ text "Apply Filters" ]]]

return : (Model, Cmd msg) -> (Model, Cmd msg, ReturnMsg)
return (a, b) = (a, b, ReturnNone)
