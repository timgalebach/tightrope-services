module CitySearch.Rest exposing (..)

import CitySearch.Types exposing (..)
import Http
import Json.Encode     as JE
import Json.Decode exposing (int, string, float, Decoder, list)
import Json.Decode.Pipeline exposing (decode, required, optional, hardcoded)


getCityCompletions : ApiInfo -> String -> Cmd Msg
getCityCompletions apiInfo query =
    case apiInfo.token of
        Nothing -> Cmd.none
        Just token ->
            let req =
                    Http.request
                        { method = "POST"
                        , url = apiInfo.url ++ "/city/search"
                        , headers = [Http.header "Authorization" token]
                        , body = Http.jsonBody <| jsonCitySearch query
                        , expect = Http.expectJson (list cityDecoder)
                        , timeout = Nothing
                        , withCredentials = False
                        }
            in Http.send CityCompletions req

cityDecoder : Decoder City
cityDecoder =
    decode City
        |> required "city" string
        |> required "state" string
        |> required "lat" float
        |> required "lng" float

jsonCitySearch : String -> JE.Value
jsonCitySearch cityName =
    JE.object [("cityName", JE.string cityName)]
