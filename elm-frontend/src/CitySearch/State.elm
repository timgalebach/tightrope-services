module CitySearch.State exposing (..)

import Autocomplete
import CitySearch.Types exposing (..)
import CitySearch.Rest  exposing (..)
import Dom
import Task

subscriptions : (Msg -> superMsg) -> Model -> Sub superMsg
subscriptions superConstructor model =
    Sub.map (superConstructor << SetAutoState) Autocomplete.subscription

init apiUrl =
    {
        cities = []
    ,   selected = Nothing
    ,   howManyToShow = 5
    ,   query = ""
    ,   error = False
    ,   autoState = Autocomplete.empty
    ,   apiInfo = ApiInfo Nothing apiUrl
    }

updateApiToken : Maybe String -> Model -> Model
updateApiToken token model =
    { model | apiInfo = ApiInfo token model.apiInfo.url }

updateConfig : Autocomplete.UpdateConfig Msg City
updateConfig =
    Autocomplete.updateConfig
        { toId = cityName
        , onKeyDown =
            \code maybeId ->
                if code == 13 then
                    Maybe.map SelectCity maybeId
                else
                    Nothing
        , onTooLow = Nothing
        , onTooHigh = Nothing
        , onMouseEnter = \_ -> Nothing
        , onMouseLeave = \_ -> Nothing
        , onMouseClick = \id -> Just <| SelectCity id
        , separateSelections = False
        }

update : Msg -> Model
       -> ( Model, Cmd Msg, ReturnMsg )
update msg model =
    case msg of
        --add in a command to pull all the cities that match
        SetQuery newQuery ->
            return <| resetMenu model newQuery

        CityCompletions (Err err) ->
            return <| { model | error = True } ! []
        CityCompletions (Ok cityList) ->
            return <| { model | cities = cityList, error = False } ! []

        SetAutoState autoMsg ->
            let
                ( newState, maybeMsg ) =
                    Autocomplete.update updateConfig autoMsg
                        model.howManyToShow model.autoState model.cities
                newModel = { model | autoState = newState }
            in
                case maybeMsg of
                    Nothing -> return <| newModel ! []

                    Just updateMsg ->
                        update updateMsg newModel

        SelectCity cityName ->
            ((setQuery model cityName), Cmd.none, CitySelect cityName)

setQuery : Model -> String -> Model
setQuery model query =
    { model | query = query, cities = [] }

resetMenu : Model -> String -> ( Model, Cmd Msg )
resetMenu model query =
    case query of
        "" -> { model | query = "", cities = [], selected = Nothing } ! []
        _  -> { model | query = query, selected = Nothing } !
              [ getCityCompletions model.apiInfo query ]

return : (Model, Cmd Msg) -> (Model, Cmd Msg, ReturnMsg)
return (a, b) = (a, b, None)
