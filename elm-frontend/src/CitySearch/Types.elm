module CitySearch.Types exposing (..)

import Autocomplete
import Http

type alias Model =
    {
        cities : List City
    ,   apiInfo : ApiInfo
    ,   selected : Maybe City
    ,   howManyToShow : Int
    ,   query : String
    ,   error : Bool
    ,   autoState : Autocomplete.State
    }

type Msg
    = SetAutoState Autocomplete.Msg
    | SetQuery String
    | SelectCity String
    | CityCompletions (Result Http.Error (List City))

type alias City =
    {
        city : String
    ,   state : String
    ,   lat : Float
    ,   lng : Float
    }

type alias ApiInfo =
    {
        token : Maybe String
    ,   url : String
    }

type ReturnMsg
    = CitySelect String
    | None

blankCity = City "" "" 0.0 0.0

cityName : City -> String
cityName city =
    city.city ++ ", " ++ city.state

cityByName : String -> List City -> City
cityByName name =
    (Maybe.withDefault <| blankCity)
    << List.head
    << List.filter (\city -> (cityName city) == name)
