module CitySearch.View exposing (..)

import CitySearch.Types exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Autocomplete
import String

viewConfig : Autocomplete.ViewConfig City
viewConfig =
  let
    customizedLi keySelected mouseSelected city =
      { attributes = [ classList [ ("autocomplete-item", True), ("is-selected", keySelected || mouseSelected) ] ]
      , children = [ Html.text <| cityName city ]
      }
  in
    Autocomplete.viewConfig
      { toId = cityName
      , ul = [ class "autocomplete-list" ] -- set classes for your list
      , li = customizedLi -- given selection states and a person, create some Html!
      }

errorMsg error = if error then text "ERROR" else text ""

-- and let's show it! (See an example for the full code snippet)
view : Model -> Html Msg
view { error, autoState, query, cities, howManyToShow } =
  div []
      [
       input [ onInput SetQuery, placeholder "Search by City..."
             , value query ] []
      , Html.map SetAutoState (Autocomplete.view viewConfig howManyToShow autoState cities)
      ]
