module Types exposing (..)

import Http
import List    exposing (foldl)
import Login.Types
import Tab.Types
import CitySearch.Types
import DoctorInfo.Main as DI
import Filter.Types    as FT

type alias Model =
    {
        login : Login.Types.Model
    ,   citySearch : CitySearch.Types.Model
    ,   mapTab : Tab.Types.Model
    ,   filter : FT.Model
    ,   doctorInfo : DI.Model
    ,   flag : Bool
    }

type Msg
    = LoginMsg Login.Types.Msg
    | TabMsg   Tab.Types.Msg
    | CitySearchMsg CitySearch.Types.Msg
    | FilterMsg FT.Msg
    | DoctorInfoMsg DI.Msg
    | CitySelect String
    | BadCreds
    | ApplyFilters
    | NewDoctors
    | NewDoctor Int
    | LoginComplete
