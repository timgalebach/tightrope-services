module Login.View exposing (..)

import Html               exposing (..)
import Html.Attributes    exposing (..)
import Html.Events        exposing (..)
import Login.Types        exposing (..)

overlayStyle : Attribute msg
overlayStyle =
    style [ ("height", "100%")
          , ("width", "100%")
          , ("position", "fixed")
          , ("z-index", "2000")
          , ("top", "0")
          , ("left", "0")
          , ("background-color", "rgb(0,0,0)")
          , ("background-color", "rgba(0,0,0, 0.9)")
          , ("overflow-y", "hidden")
          , ("transition", "0.5s")
          ]

overlayContentStyle : Attribute msg
overlayContentStyle =
    style [ ("position", "relative")
          , ("top", "25%")
          , ("width", "200px")
          , ("margin", "auto")
--          , ("text-align", "center")
          , ("margin-top", "30px")
          ]

loginWidget : Model -> Html Msg
loginWidget lm =
    div [] [ logoutButton lm
           , loginOverlay lm
           ]

loginOverlay : Model -> Html Msg
loginOverlay lm =
    div [ overlayStyle
        , if loggedInP lm
          then style [("height", "0%")]
          else style [("height", "100%")]]
        [ div [ overlayContentStyle ]
              [ badLogin lm
              , div [ class "form-group" ]
                  [ label [ for "username" ] [ text "Username" ]
                  , input [ class "form-control", type_ "text", id "username"
                          , placeholder "Username", onInput Username
                          , value lm.username ] [] ]
              , div [ class "form-group" ]
                  [ label [ for "password" ] [ text "Password" ]
                  , input [ class "form-control", type_ "password", id "password"
                          , placeholder "Password", onInput Password
                          , value lm.password ] []
                  ]
              , button [ class "btn btn-primary", onClick Login ]
                  [ text "Login" ]]]

debugModel : Model -> Html Msg
debugModel lm =
    span [] [ text <| toString lm.idToken, text lm.username, text lm.password ]

badLogin : Model -> Html Msg
badLogin lm =
    case lm.loginError of
        Nothing -> span [] []
        Just error ->
            div [ class "alert alert-danger" ] [ text error ]

logoutButton : Model -> Html Msg
logoutButton lm =
    if loggedInP lm
    then button [ style [("margin", "10px"), ("margin-left", "0px")]
                , class "btn btn-sm btn-primary", onClick <| Logout 200 ]
              [ text "Logout" ]
    else span [] []
