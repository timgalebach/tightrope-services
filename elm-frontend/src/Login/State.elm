module Login.State exposing (..)

import Login.Ports
import Login.Types  exposing (..)

subscriptions : (Msg -> msg) -> Sub msg
subscriptions superConstructor =
    Sub.batch
        [
          Login.Ports.freshIdToken (superConstructor << IdToken)
        , Login.Ports.fromJsLoginComplete (superConstructor << LoginComplete)
        ]

init =
    {
        idToken = Nothing
    ,   loggedIn = False
    ,   username = ""
    ,   password = ""
    ,   loginError = Nothing
    }

clearInputs : Model -> Model
clearInputs model =
    { model | username = "", password = "" }

doLogin : Model -> Cmd msg
doLogin lmodel =
    Login.Ports.sendCreds lmodel

logout : Model -> (Model, Cmd msg)
logout lm =
    ( init, Login.Ports.setTokenCookie Nothing )

update : Msg -> Model -> (Model, Cmd Msg, ReturnMsg)
update lmsg lmodel =
    case lmsg of
        Username un -> ( { lmodel | username = un }, Cmd.none, None)

        Password p  -> ( { lmodel | password = p }, Cmd.none, None)

        Login -> ( lmodel, doLogin lmodel, None )

        Logout _ ->
            wrapR (logout lmodel)

        LoginComplete _ -> (lmodel, Cmd.none, ReturnLoginComplete)

        IdToken Nothing ->
            let m = clearInputs lmodel
            in ( { m | idToken = Nothing, loggedIn = False
                 , loginError = Just "Bad username or password" }
               , Login.Ports.setTokenCookie Nothing, None)

        IdToken token ->
            let m = clearInputs lmodel
            in ({ m | idToken = token, loggedIn = True
                , loginError = Nothing }
               , Login.Ports.setTokenCookie token, None)

wrapR : (Model, Cmd Msg) -> (Model, Cmd Msg, ReturnMsg)
wrapR (a, b) = (a, b, None)
