module Login.Types exposing (..)

type alias Model =
    {
        idToken : Maybe String
    ,   loggedIn : Bool
    ,   loginError : Maybe String
    ,   username : String
    ,   password : String
    }

idToken : Model -> Maybe String
idToken lm = lm.idToken

loggedInP model = model.loggedIn

logout = Logout 2

type Msg
    = Username String
    | Password String
    | IdToken (Maybe String)
    | LoginComplete Int --Int is just for the port, means nothing
    | Login
    | Logout Int --Int means nothing, just to use Task.succeed

type ReturnMsg
    = ReturnLoginComplete
    | None
