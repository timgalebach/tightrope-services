port module Login.Ports exposing (..)

import Login.Types

--id token coming in from JS
port freshIdToken : ((Maybe String) -> msg) -> Sub msg

port fromJsLoginComplete : (Int -> msg) -> Sub msg

--to be stored in a cookie
port setTokenCookie : (Maybe String) -> Cmd msg

port sendCreds : Login.Types.Model -> Cmd msg
