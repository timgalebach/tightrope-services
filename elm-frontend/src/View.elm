module View exposing (..)

import Html              exposing (..)
import Html.Attributes   exposing (..)
import Html.Events       exposing (..)
import Types             exposing (..)
import Login.View
import Login.Types       as LT
import Filter.Main       as F
import DoctorInfo.Main   as DI
import Tab.View          exposing (tabWidget)
import Tab.State         exposing (numDocs)
import CitySearch.View


mapTabStyle : Attribute msg
mapTabStyle =
    style [ ("width", "550px")
          , ("height", "450px")
          , ("background-color", "grey")
          , ("border-radius", "23px")
          ]

debugModels model =
    let n      = numDocs model.mapTab
        plural = if n == 1 then "" else "s"
        maxP   = if n == 100 then True else False
        plusSign = if maxP then "+" else ""
    in div []
        [  text <| if model.flag then "FLAG" else ""
         , text <| (toString n) ++ plusSign ++ " doctor" ++ plural ++ " found."
        ]

rootView : Model -> Html Msg
rootView model =
    div [ style [("margin", "8px")]
        , class "container"]
        [ div [ class "row" ]
              [ div [class "col-md-7"]
                    [ Login.View.loginWidget model.login |> Html.map LoginMsg
                    , CitySearch.View.view model.citySearch
                       |> Html.map CitySearchMsg
                    , debugModels model
                    , tabWidget model.mapTab |> Html.map TabMsg
                    ]
              , div [ class "col-md-5" ]
                  [ F.view model.filter |> Html.map FilterMsg
                  , DI.view model.doctorInfo |> Html.map DoctorInfoMsg]
              ]
        ]
