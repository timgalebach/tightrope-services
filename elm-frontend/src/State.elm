module State exposing (..)

import Http
import Task
import Types          exposing (..)
import Response       exposing (..)
import Filter.Main    as F
import Filter.Types   as FT
import DoctorInfo.Main as DI
import Login.State
import Login.Types    as LT
import Tab.Types      as TT
import Tab.State      as TS
import DoctorInfo.Main as DI
import CitySearch.State as CSS
import CitySearch.Types as CST

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ Login.State.subscriptions LoginMsg
              , TS.subscriptions TabMsg
              , CSS.subscriptions CitySearchMsg model.citySearch
              , F.subscriptions FilterMsg
              ]

apiUrl = "http:///54.159.120.64/api"
--awsUrl = "https://bxbdg0p25e.execute-api.us-east-1.amazonaws.com/auth2"

initialState : ( Model, Cmd Msg )
initialState =
    let
        filter = F.init apiUrl
        (mapTabState, mapTabCmd) = TS.init apiUrl filter
    in
        ( { flag = False
          , login = Login.State.init
          , mapTab = mapTabState
          , citySearch = CSS.init apiUrl
          , filter = filter
          , doctorInfo = DI.init apiUrl
          }
        , Cmd.batch [
               Cmd.map TabMsg mapTabCmd
              ]
        )

passIdTokenToChildren : Maybe String -> Model -> Model
passIdTokenToChildren idToken model =
    { model | citySearch = CSS.updateApiToken idToken model.citySearch
    ,         mapTab = TS.updateApiToken idToken model.mapTab
    ,         filter =  F.updateApiToken idToken model.filter
    ,         doctorInfo = DI.updateApiToken idToken model.doctorInfo
    }


clearChildIdTokens : Model -> Model
clearChildIdTokens m =
    passIdTokenToChildren Nothing m

updateChildIdTokens : Model -> Model
updateChildIdTokens m =
    passIdTokenToChildren (LT.idToken m.login) m

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        BadCreds ->
            update (LoginMsg LT.logout) (clearChildIdTokens model)

        --todo: handle this and pass the doctors somewhere
        NewDoctors ->
            ( model, Cmd.none )

        NewDoctor id -> update (DoctorInfoMsg (DI.GetDoctor id)) model

        --pass id token down to children
        --refresh all cities
        LoginComplete ->
            let newState = updateChildIdTokens model
            in (newState, Cmd.none)

        ApplyFilters ->
            update (TabMsg (TT.FilterChanged model.filter)) model

        LoginMsg loginMsg ->
            let (s, c, rmsg) = Login.State.update loginMsg model.login
                newModel     = { model | login = s }
            in case rmsg of
                   LT.None -> ( newModel, Cmd.map LoginMsg c)
                   LT.ReturnLoginComplete ->
                       update LoginComplete newModel

        TabMsg tabMsg ->
            let ( state, cmd, maybeMsg) =
                    TS.update tabConfig tabMsg model.mapTab
            in
                case maybeMsg of
                    Nothing ->
                        (state, cmd)
                            |> mapModel (\x -> { model | mapTab = x })
                            |> mapCmd   TabMsg
     --do the gymnastics to run the original command returned in the 1st call
                    Just msg ->
                        let (newModel, newCmd) = update msg { model | mapTab = state }
                        in (newModel, Cmd.batch [ Cmd.map TabMsg cmd, newCmd ])

        FilterMsg filterMsg ->
            let (s, cmd, rmsg) = F.update filterMsg model.filter
            in
                case rmsg of
                    FT.ReturnApplyFilters ->
                        update ApplyFilters { model | filter = s }

                    FT.ReturnNone -> (s, cmd)
                      |> mapModel (\x -> { model | filter = x })
                      |> mapCmd   FilterMsg

        DoctorInfoMsg diMsg ->
            DI.update diMsg model.doctorInfo
                |> mapModel (\x -> { model | doctorInfo = x })
                |> mapCmd   DoctorInfoMsg


        CitySearchMsg csMsg ->
            let (state, cmd, returnMsg ) =
                    CSS.update csMsg model.citySearch
            in
                case returnMsg of
                    CST.CitySelect city ->
                        update (CitySelect city) { model | citySearch = state }

                    CST.None -> (state, cmd)
                         |> mapModel (\x -> { model | citySearch = x })
                         |> mapCmd   CitySearchMsg

        CitySelect city ->
            update (TabMsg (TT.goCityMsg city)) model

tabConfig : TT.UpdateConfig Msg
tabConfig = { authErrorMsg = BadCreds
            , newDoctorsMsg = NewDoctors
            , doctorClickMsg = NewDoctor}
