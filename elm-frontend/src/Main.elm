module Main exposing (main)


import Html          exposing (..)
import Html.Events   exposing (..)
import State         exposing (..)
import View          exposing (..)


main =
  Html.program
    { init = State.initialState
    , view = View.rootView
    , update = State.update
    , subscriptions = State.subscriptions
    }
