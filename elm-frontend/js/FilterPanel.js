function FilterPanelInit() {
    console.log("FilterPanelInit()");
    var paymentSlider = document.getElementById('paymentSlider');

    var rangeMax = 500000;
    var init = { payment: [50000, 200000] };

    noUiSlider.create(paymentSlider, {
	start: init.payment,
	connect: true,
        step: 1000,
	range: {
	    'min': 0,
	    'max': rangeMax
        },
    });

    var vs = init.payment;
    vs.push(rangeMax);
    elmApp.ports.fromJsPayment.send(vs);
    elmApp.ports.initFilters.send(null);

    paymentSlider.noUiSlider.on('update', function(values, h) {
        for (var i=0; i<values.length; i++) { values[i] = Math.round(values[i]); }
        values.push(rangeMax);
        elmApp.ports.fromJsPayment.send(values);
    });
}
