AWSCognito.config.region = 'us-east-1';

var poolData = {
    UserPoolId : 'us-east-1_BW25eMH9v',
    ClientId : '4n83r2qhcnlu3mhdrco2nmjqs',
    Paranoia : 7
};

function onSuccess(result) { console.log(result.idToken.getJwtToken()); }
function onFailure(err) { console.log(err) };

function gotIdToken(result) {
    elmApp.ports.freshIdToken.send(result.idToken.getJwtToken());
}

function nothingToken(err) {
    elmApp.ports.freshIdToken.send(null);
}

function authenticateCognitoUser(user, authDetails, success, failure, newPassword) {
    user.authenticateUser(authDetails, {
        onSuccess: success,
        onFailure: failure,
        newPasswordRequired: newPassword
    });
}

function CognitoInit() {
    elmApp.ports.sendCreds.subscribe(function(loginObj) {
        var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
        var userData = {
            Username : loginObj.username,
            Pool : userPool
        };

        var authenticationData = {
            Username : loginObj.username,
            Password : loginObj.password,
        };

        var authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);

        var npr = function(userAttributes, requiredAttributes) {
            console.log("doing new password");
            cognitoUser.completeNewPasswordChallenge(loginObj.password, false, this);
        };

        var cognitoUser =
                new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);

        authenticateCognitoUser(cognitoUser, authenticationDetails,
                                gotIdToken, nothingToken, npr);
    });
 }

//authenticateCognitoUser(cognitoUser, onSuccess, onFailure, npr);

//authenticateCognitoUser(cognitoUser, gotIdToken, nothingToken, npr);
