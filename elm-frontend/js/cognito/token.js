function TokenInit() {
    console.log("TokenInit()");
    var token = Cookies.get('idToken');

    if (token == undefined)
    { /*console.log("token undefined"); elmApp.ports.freshIdToken.send(null); */}
    else
    { console.log("token defined"); elmApp.ports.freshIdToken.send(token); }

    elmApp.ports.setTokenCookie.subscribe(function(maybeToken) {
//        console.log("Received token: " + maybeToken);
        if (maybeToken == null) {
            Cookies.remove('idToken');
        }
        else {
//            console.log('setting cookie to token');
            Cookies.set('idToken', maybeToken, {expires: 1});
            elmApp.ports.fromJsLoginComplete.send(1);
        }
    });
}
