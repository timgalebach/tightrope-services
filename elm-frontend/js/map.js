var ZOOMLEVEL = 11;
var tightropeMapState = {infoWindow: null};

//randomize lat/lng to avoid bunching
function fuzzifyLatLng(latLng, fuzzFactor) {
    //fuzzFactor is the maximum amount lat/lng can vary from original
    latFuzz = (Math.random()-0.5) * fuzzFactor;
    lngFuzz = (Math.random()-0.5) * fuzzFactor;
    return {  lat: latLng.lat + latFuzz,
              lng: latLng.lng + lngFuzz}
}

function moneyFormat(price, sign = '$') {
    const pieces = parseFloat(price).toFixed(2).split('');
    let ii = pieces.length - 3
    while ((ii-=3) > 0) {
        pieces.splice(ii, 0, ',')
    }
    return sign + pieces.join('')
}

//icon functions
var pinPath = 'M454.848,198.848c0,159.225-179.751,306.689-179.751,306.689c-10.503,8.617-27.692,8.617-38.195,0c0,0-179.751-147.464-179.751-306.689C57.153,89.027,146.18,0,256,0S454.848,89.027,454.848,198.848z';
var circlePath = 'M1664 896q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z';
var crossPath = 'M24 4c-11.05 0-20 8.95-20 20s8.95 20 20 20 20-8.95 20-20-8.95-20-20-20zm10 22h-8v8h-4v-8h-8v-4h8v-8h4v8h8v4z';
function pinSymbol(color, scale) {
    return {
        path: circlePath,
        fillColor: color,
        fillOpacity: 0.6,
        strokeColor: '#fff',
        strokeWeight: 1,
        scale: scale
    };
}

function pinScale(numDocs) {
    var MAX_DOCS = 18000;
    ratio = numDocs/MAX_DOCS;
    if(ratio < 0.01) return 0.19;
    if(ratio < 0.1) return 0.35;
    if(ratio < 0.4) return 0.6;
    else return 1.0;
}

function setMapOnAll(map, markers) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers(markers) {
    setMapOnAll(null, markers);
}

// Shows any markers currently in the array.
function showMarkers(map, markers) {
    setMapOnAll(map, markers);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers(mapData) {
    clearMarkers(mapData.markers);
    mapData.markers = [];
}

function addCityMarkers(map, markers, cityList) {
    for(var i=0; i<cityList.length; i++) {
        scale = 0.03 * pinScale(cityList[i].numDocs);
        var m = new google.maps.Marker({
            position: {lat: cityList[i].lat,
                       lng: cityList[i].lng},
            map: map,
            icon:  pinSymbol('red', scale)
        });
        markerBind(m, 'click', cityList[i], elmApp.ports.cityMapClick);
        infoOpenBind("mouseover", map, m, cityToInfoString(cityList[i]));
        infoCloseBind("mouseout", map, m);
        markers.push(m);
    }
}

function addDoctorMarkers(map, markers, doctorList) {
    for(var i=0; i<doctorList.length; i++) {
        scale = 0.03 * 0.28;
        var m = new google.maps.Marker({
            position: fuzzifyLatLng({lat: doctorList[i].lat,
                                     lng: doctorList[i].lng}, 0.007),
            map: map,
            icon:  pinSymbol('#f79102', scale)
        });

        id = doctorList[i].id;
        markerBind(m, 'click', id, elmApp.ports.doctorClick);
        infoOpenBind("mouseover", map, m, doctorToInfoString(doctorList[i]));
        infoCloseBind("mouseout", map, m);
        markers.push(m);
    }
}

function mapBoundsElmFormat(map) {
    b = map.getBounds();
    return {
        sw_lat: b.getSouthWest().lat(),
        sw_lng: b.getSouthWest().lng(),
        ne_lat: b.getNorthEast().lat(),
        ne_lng: b.getNorthEast().lng()
    };
}
///////////////////////////////////////
function swapInfoWindow(state, infoWindow, map, marker) {
    clearInfoWindow(state);
    state.infoWindow = infoWindow;
    infoWindow.open(map, marker);
}
function clearInfoWindow(state) {
    if (state.infoWindow != null) {
        state.infoWindow.close();
    }
    state.infoWindow = null;
}

function cityToInfoString(cityObj) {
    var docString = "Doctor";
    if (cityObj.numDocs > 1) { docString += "s" }
    return "<strong>" + cityObj.city + "</strong><br>" + cityObj.numDocs + " " +
        docString;
}

function doctorToInfoString(doctorObj) {
    var dName = doctorObj.first + " " + doctorObj.last;
    return "<div class='small'><strong>" + dName + "</strong><br>" +
        doctorObj.city + ", " + doctorObj.state + "<br>"
        + "<em>" + moneyFormat(doctorObj.amount) + "</em></div>";
}

function infoOpenBind(eventName, map, marker, contentString) {
    marker.addListener(eventName, function() {
        var iw = new google.maps.InfoWindow({
            content: contentString
        });
        swapInfoWindow(tightropeMapState, iw, map, marker);
    });
}

function infoCloseBind(eventName, map, marker) {
    marker.addListener(eventName, function() {
        clearInfoWindow(tightropeMapState);
    });
}

function markerBind(marker, eventName, data, portFn) {
    //data can be an object, like a City
    marker.addListener(eventName, function() {
//        console.log("click: " + data);
        portFn.send(data);
    });
}

function bind(map, eventName, portFn) {
    map.addListener(eventName, function() {
        sendMapBounds(map, portFn);
    });
}

function sendMapBounds(map, portFn) {
    //    console.log(newBounds);
    portFn.send(mapBoundsElmFormat(map));
}

////////////////////////////////////////////////////////////////
//Initialization and global state
////////////////////////////////////////////////////////////////

function CityMapInit(mapDiv, bounds) {
    var myLatlng = new google.maps.LatLng(47.600546, -122.293326);

    var mapOptions = {
        zoom: ZOOMLEVEL,
        center: myLatlng,
        mapTypeId: 'terrain',
        disableDefaultUI: true,
        zoomControl: true,
        streetViewControl: false,
        styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-60},{"lightness":55}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
};
    mapData.cityMap.mapObj = new google.maps.Map(mapDiv, mapOptions);
    mapData.cityMap.markers = [];

    var moveFn = elmApp.ports.cityMapMoved;

    elmApp.ports.sendCities.subscribe(function(cityList) {
        deleteMarkers(mapData.cityMap);
        if (cityList.length < 2000) {
            addCityMarkers(mapData.cityMap.mapObj, mapData.cityMap.markers, cityList);
        }
    });

    elmApp.ports.sendMapCenter.subscribe(function(coord) {
        mapData.cityMap.mapObj.setCenter(new google.maps.LatLng(coord.lat, coord.lng));
        mapData.cityMap.mapObj.setZoom(10);
        moveFn.send(mapBoundsElmFormat(mapData.cityMap.mapObj));
    });

    elmApp.ports.sendError.subscribe(function(errorString) {
        console.log(errorString);
    });

    bind(mapData.cityMap.mapObj, 'dragend', moveFn);
    bind(mapData.cityMap.mapObj, 'zoom_changed', moveFn);

    google.maps.event.addListener(mapData.cityMap.mapObj,'bounds_changed',function(){
        moveFn.send(mapBoundsElmFormat(mapData.cityMap.mapObj));
    });

}

function DoctorMapInit(mapDiv, bounds) {
        var myLatlng = new google.maps.LatLng(47.600546, -122.293326);

    var mapOptions = {
        zoom: ZOOMLEVEL,
        center: myLatlng,
        mapTypeId: 'terrain',
        disableDefaultUI: true,
        zoomControl: true,
        streetViewControl: false,
        styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text.fill","stylers":[{"hue":"#ff0000"},{"visibility":"simplified"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"on"},{"weight":"2.07"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#0a6589"},{"visibility":"on"}]}]

    };

    var moveFn = elmApp.ports.doctorMapMoved;

    mapData.docMap.mapObj = new google.maps.Map(mapDiv, mapOptions);
    mapData.docMap.markers = [];
    /*
    b = {north: bounds.ne_lat, east: bounds.ne_lng,
         south: bounds.sw_lat, west: bounds.sw_lng};
    mapData.docMap.mapObj.fitBounds(b);
*/

    elmApp.ports.sendDoctorCity.subscribe(function(city) {
        mapData.docMap.mapObj.setCenter(new google.maps.LatLng(city.lat, city.lng));
        mapData.docMap.mapObj.setZoom(10);
        moveFn.send(mapBoundsElmFormat(mapData.docMap.mapObj));
    });

    elmApp.ports.sendMapCenter.subscribe(function(coord) {
        mapData.docMap.mapObj.setCenter(new google.maps.LatLng(coord.lat, coord.lng));
        mapData.docMap.mapObj.setZoom(10);
        moveFn.send(mapBoundsElmFormat(mapData.docMap.mapObj));
    });

    elmApp.ports.sendDoctors.subscribe(function(docList) {
        deleteMarkers(mapData.docMap);
        addDoctorMarkers(mapData.docMap.mapObj, mapData.docMap.markers, docList);
    });

    bind(mapData.docMap.mapObj, 'dragend', moveFn);
    bind(mapData.docMap.mapObj, 'zoom_changed', moveFn);
}
