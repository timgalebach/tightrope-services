{-# LANGUAGE OverloadedStrings #-}
module Aws where

import           Control.Lens
import           Network.AWS
import           Network.AWS.S3
import           Network.AWS.Data.Body
import           System.IO
import           Conduit
import qualified Data.Conduit.Text        as CT
import           Data.Text                (Text)
import qualified Data.Text                as Text
import qualified Data.Text.IO             as Text
import qualified Data.ByteString          as B

type FileName = String
type Prefix   = String

bucketName :: String -> BucketName
bucketName = BucketName . Text.pack

objectKey  :: String -> ObjectKey
objectKey  = ObjectKey . Text.pack


s3ObjectToFile :: Region -> BucketName -> ObjectKey -> FileName
                 -> ConduitM B.ByteString B.ByteString (ResourceT IO) ()
                 -> IO ()
s3ObjectToFile region bucket key outfile conduit = do
  putStrLn outfile
  env <- newEnv region Discover
  l <- newLogger Debug stdout
  runResourceT . runConduit . runAWS (env & envLogger .~ l) $ do
    rsBody <- request
    sinkBody rsBody $ sinkFile outfile
    where request = (send (getObject bucket key)) >>=
            (\rs -> return $ (rs ^. gorsBody) `fuseStream` conduit)

s3ListObjects :: Region -> BucketName -> Prefix -> IO [ObjectKey]
s3ListObjects region bucket prefix = do
  env <- newEnv region Discover
  l <- newLogger Debug stdout
  runResourceT . runConduit . runAWS (env & envLogger .~ l) $ do
    loRs <- request
    return $ map (view oKey) (loRs ^. lorsContents)
    where request = (send ((listObjects bucket)
                           & loPrefix .~ (Just $ Text.pack prefix)))
