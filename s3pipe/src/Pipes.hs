{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveDataTypeable #-}

module Pipes where

import           Aws
import           Text.Read
import           GHC.Generics
import           Data.Aeson
import           Data.Data
import           Data.Text                     (Text)
import           Data.Text                     as Text
import           Data.String.Utils             as SU
import           Conduit
import qualified Data.Conduit.Text             as CT
import           Control.Monad
import qualified Data.ByteString.Lazy.Char8    as BS
import qualified Data.ByteString               as B

--fields used ["3" "4" "5" "6" "7" "8" "10" "11" "12" "13" "15" "16" "17" "18" "26" "27" "30" "31" "32" "45" "63"]

jsonSample = "{\"58\":\"EMA\",\"9\":\"\",\"3\":\"\",\"51\":\"\",\"50\":\"\",\"34\":\"Gift\",\"49\":\"\",\"22\":\"\",\"26\":\"100000000065\",\"4\":\"\",\"8\":\"JAMES\",\"28\":\"FL\",\"60\":\"\",\"14\":\"64133\",\"59\":\"\",\"61\":\"\",\"57\":\"\",\"30\":\",20.00,\",\"21\":\"\",\"33\":\"Cash or cash equivalent\",\"20\":\"MO\",\"v\":\"cms-10.00\",\"47\":\"Covered\",\"19\":\"\",\"17\":\"\",\"25\":\"National Dentex Corporation\",\"15\":\"United States\",\"42\":\"\",\"7\":\"\",\"44\":\"No\",\"5\":\"734368\",\"48\":\"\",\"53\":\"\",\"18\":\"Doctor of Dentistry\",\"36\":\"\",\"12\":\"RAYTOWN\",\"13\":\"MO\",\"27\":\"National Dentex Corporation\",\"62\":\"\",\"24\":\"\",\"35\":\"\",\"6\":\"RUBELITA\",\"38\":\"No\",\"39\":\"No Third Party Payment\",\"1\":\"Covered Recipient Physician\",\"63\":\"2015\",\"0\":\"NEW\",\"43\":\"\",\"37\":\"\",\"46\":\"No\",\"11\":\"\",\"45\":\"234979880\",\"56\":\"\",\"32\":\"1\",\"55\":\"\",\"2\":\"\",\"54\":\"\",\"16\":\"\",\"41\":\"\",\"10\":\"9500 E 63RD ST STE 103\",\"40\":\"\",\"31\":\"12\\/14\\/2015\",\"64\":\"06\\/30\\/2016\",\"23\":\"\",\"52\":\"\",\"29\":\"United States\"}"

pb :: Maybe PaymentBasic10dot00
pb = decodePB jsonSample

processPayments :: ConduitM B.ByteString B.ByteString (ResourceT IO) ()
processPayments =
  CT.decode CT.utf8
  .| CT.lines
  .| mapC jsonToTabbedLine
  .| CT.encode CT.utf8

takeLines :: Int -> ConduitM B.ByteString B.ByteString (ResourceT IO) ()
takeLines numLines =
  CT.decode CT.utf8
  .| CT.lines
  .| takeC numLines
  .| CT.encode CT.utf8

decodePB :: Text -> Maybe PaymentBasic10dot00
decodePB = decode . BS.pack . Text.unpack

jsonToTabbedLine :: Text -> Text
jsonToTabbedLine input =
  case (fmap tabSeparated) $ decodePB input of
    Nothing    -> Text.pack ""
    Just line  -> line

parsePayment :: Text -> Float
parsePayment s =
  case readMaybe $ SU.replace "," "" (Text.unpack s) of
    Just x   -> x
    Nothing  -> -1.0

parseInt :: Text -> Integer
parseInt s =
  case readMaybe $ Text.unpack s of
    Just x   -> x
    Nothing  -> -1

pBVals :: PaymentBasic10dot00 -> [String]
pBVals (PaymentBasic10dot00 a b c d e f g h i j k l m n o p q r s t u v) =
  [show a, show b, show c, show d, show e, show f, show g, show h, show i, show j,
   show k, show l, show m, show n, show o, show p, show q, show r, show s,
   show t, show u, show v]

tabSeparated :: PaymentBasic10dot00 -> Text
tabSeparated =
  Text.pack . (++ "\n") . (SU.join "\t") . pBVals

data PaymentBasic10dot00 = PaymentBasic10dot00 {
  recordId          :: Integer,  --45
  docId             :: Integer,  --5
  docType           :: Text,  --18
  docFirstName      :: Text,  --6
  docMiddleName     :: Text,  --7
  docLastName       :: Text,  --8
  addrLine1         :: Text,  --10
  addrLine2         :: Text,  --11
  city              :: Text,  --12
  state             :: Text,  --13
  zipCode           :: Text,  --14
  province          :: Text,  --16
  postalCode        :: Text,  --17
  country           :: Text,  --15
  teachingHospID    :: Integer,  --3
  teachingHospName  :: Text,     --4
  paymentMakerID    :: Integer,  --26
  paymentMakerName  :: Text,  --27
  programYear       :: Integer,  --63
  numPayments       :: Integer,  --32
  paymentDate       :: Text, --31
  totalPayment      :: Float --30
} deriving (Show, Data, Typeable)

instance FromJSON PaymentBasic10dot00 where
  parseJSON (Object v) =
    PaymentBasic10dot00 <$>
    liftM parseInt           (v .: "45")    <*>
    liftM parseInt           (v .: "5")     <*>
    v .: "18"    <*>
    v .: "6"     <*>
    v .: "7"     <*>
    v .: "8"     <*>
    v .: "10"    <*>
    v .: "11"    <*>
    v .: "12"    <*>
    v .: "13"    <*>
    v .: "14"    <*>
    v .: "16"    <*>
    v .: "17"    <*>
    v .: "15"    <*>
    liftM parseInt          (v .: "3")     <*>
    v .: "4"     <*>
    liftM parseInt          (v .: "26")    <*>
    v .: "27"    <*>
    liftM parseInt         (v .: "63")    <*>
    liftM parseInt         (v .: "32")    <*>
    v .: "31"    <*>
    liftM parsePayment      (v .: "30")
