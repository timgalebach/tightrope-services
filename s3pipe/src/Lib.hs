{-# LANGUAGE OverloadedStrings #-}

module Lib where

import           Aws
import           Pipes
import           Conduit
import           Data.String.Utils             as SU
import qualified Data.Text                     as Text
import           Network.AWS.Data.Text         (toText)
import           Network.AWS.S3.Types
import           Network.AWS                   (Region)

test = s3ObjectToFile NorthVirginia (bucketName "tightropecp") (objectKey "data/cms/2015/1480614982829-200000") "/Users/blah/Desktop/out.txt" processPayments

test2 = s3ObjectToFile NorthVirginia (bucketName "tightropecp") (objectKey "data/cms/2015/1480614982829-200000") "/Users/blah/Desktop/out.txt" (takeLines 2)

test3 = s3ListObjects NorthCalifornia (bucketName "btcpie") ""

outFile :: String -> ObjectKey -> String
outFile dir key =
  (dir ++ "/" ++ (head . reverse $ SU.split "/" (Text.unpack (toText key)))
  ++ ".txt")


s3ToFiles :: Region -> String -> Prefix -> FileName -> IO ()
s3ToFiles region b prefix outDir = do
  keys <- s3ListObjects region bucket prefix
  runConduit
    $ yieldMany keys
    .| iterMC (\oKey -> s3ObjectToFile region bucket oKey
                        (outFile outDir oKey) processPayments)
    .| sinkNull
    where bucket = bucketName b


someFunc :: IO ()
someFunc = do
  print "timtime"
