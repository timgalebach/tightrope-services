module Main where

import Lib
import System.Environment (getArgs)

main :: IO ()
main = do
  args <- getArgs
  s3ToFiles (args !! 0) (args !! 1) (args !! 2)
