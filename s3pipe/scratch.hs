      where sink =
              CT.decode CT.utf8
              .| CT.lines
              .| takeC 1
              .| CT.encode CT.utf8
              .| printC

let y = x >>= (\rs -> return $ (rs ^. gorsBody) `fuseStream` (CT.decode CT.utf8 .| CT.encode CT.utf8))

s3ObjectToFile (BucketName $ Text.pack "tightropecp") (ObjectKey $ Text.pack "data/cms/2015/1480614982829-200000") (takeLines 10)


{"Recipient_Primary_Business_Street_Address_Line1" "17815 W 160TH ST",
 "Name_of_Third_Party_Entity_Receiving_Payment_or_Transfer_of_Value"
 "",
 "Recipient_State" "KS",
 "Program_Year" "2015",
 "Physician_Profile_ID" "545007",
 "Recipient_Zip_Code" "66062",
 "Physician_Middle_Name" "",
 "Physician_First_Name" "MICHAEL",
 "Teaching_Hospital_Name" "",
 "Recipient_Postal_Code" "",
 "Applicable_Manufacturer_or_Applicable_GPO_Making_Payment_ID"
 "100000000065",
 "Total_Amount_of_Payment_USDollars" ",40.00,",
 "Physician_Last_Name" "HOWELL",
 "Applicable_Manufacturer_or_Applicable_GPO_Making_Payment_Name"
 "National Dentex Corporation",
 "Recipient_Province" "",
 "Physician_License_State_code4" "",
 "Recipient_Country" "United States",
 "Record_ID" "234979872",
 "Recipient_City" "OLATHE",
 "Teaching_Hospital_ID" "",
 "Date_of_Payment" "12/14/2015",
 "Number_of_Payments_Included_in_Total_Amount" "1",
 "Recipient_Primary_Business_Street_Address_Line2" "",
 "Physician_Primary_Type" "Doctor of Dentistry"}

{"3" " | Teaching_Hospital_ID",
 "26"
 "100000000065 | Applicable_Manufacturer_or_Applicable_GPO_Making_Payment_ID",
 "4" " | Teaching_Hospital_Name",
 "8" "HOWELL | Physician_Last_Name",
 "14" "66062 | Recipient_Zip_Code",
 "30" ",40.00, | Total_Amount_of_Payment_USDollars",
 "17" " | Recipient_Postal_Code",
 "15" "United States | Recipient_Country",
 "7" " | Physician_Middle_Name",
 "5" "545007 | Physician_Profile_ID",
 "18" "Doctor of Dentistry | Physician_Primary_Type",
 "12" "OLATHE | Recipient_City",
 "13" "KS | Recipient_State",
 "27"
 "National Dentex Corporation | Applicable_Manufacturer_or_Applicable_GPO_Making_Payment_Name",
 "6" "MICHAEL | Physician_First_Name",
 "63" "2015 | Program_Year",
 "11" " | Recipient_Primary_Business_Street_Address_Line2",
 "45" "234979872 | Record_ID",
 "32" "1 | Number_of_Payments_Included_in_Total_Amount",
 "16" " | Recipient_Province",
 "10"
 "17815 W 160TH ST | Recipient_Primary_Business_Street_Address_Line1",
 "40"
 " | Name_of_Third_Party_Entity_Receiving_Payment_or_Transfer_of_Value",
 "31" "12/14/2015 | Date_of_Payment",
 "23" " | Physician_License_State_code4"}

["3" "4" "5" "6" "7" "8" "10" "11" "12" "13" "15" "16" "17" "18" "26" "27" "30" "31" "32" "45" "63"]


liftM parseYear         (v .: "63")    <*>
    liftM parseNumPayments  (v .: "32")    <*>
    v .: "31"    <*>
    liftM parsePayment      (v .: "30")

    v .: "63"    <*>
    v .: "32"    <*>
    v .: "31"    <*>
    v .: "30"

    sinkBody rsBody printC


s3ToFiles (bucketName "tightropecp") "data/cms" "/mnt/data1/s3pipe"
stack exec s3pipe-exe tightropecp data/cms/2014/14807 /mnt/data1/s3pipe
