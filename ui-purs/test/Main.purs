module BadTest.Main where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE, log)
import Control.Monad.Eff.Class (liftEff)
import Control.Monad.Aff (launchAff, Aff(..))
import Data.Maybe (Maybe(..), maybe')
import Data.Either (Either(..), either)
import Data.List (List(..))
import Config as C
import Types as T
import Network.HTTP.Affjax (affjax, defaultRequest, AJAX)
import Network.Auth.AuthZero (apiTestRequest, apiPostRequest, ErrorMsg)

main = do
  log $ "timtime"
