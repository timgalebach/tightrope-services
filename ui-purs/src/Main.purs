module Main where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Class (liftEff)
import Control.Monad.Aff (launchAff)
import Control.Monad.Eff.Console (CONSOLE, log)
import Data.Maybe (Maybe(..))
import Data.List (List(..))
import Data.Either (Either(..))
import Data.Visual.GMaps as GMaps

import Halogen.Aff as HA
import Halogen.VDom.Driver (runUI)
import Network.Auth.AuthZero (loadOrRefreshToken, apiPostRequest, ErrorMsg(..))

import Config as C
import Container (ui)

main = HA.runHalogenAff $ do
  body <- HA.awaitBody
  token <- loadOrRefreshToken C.clientId C.authDomain (C.apiUrl <> "loginping")
  liftEff $ log $ "Loaded token: " <> token
  runUI (ui token) unit body
