module Filter where

import Tight.Prelude
import Types (Payer(..), MyMonad, PayerSearchQ(..), AuthState(..), DoctorFilter)
import Config (sliderStart, sliderEnd, sliderMax, maxNum, doctorResultLimit, doctorResultLimitMax)
import Control.Monad.Eff.Console (log, logShow)
import Control.Monad.Aff (Aff)
import Network.HTTP.Affjax (AJAX)
import Network.Auth.AuthZero (apiPostRequest, IdToken, ApiUrl, ErrorMsg(..))
import Data.Argonaut (class EncodeJson, encodeJson)
import Data.Filter.Slider (Range, ElementId, SLIDER, initSlider, getRange)
import Data.Array (delete, head, last)
import Data.Int (fromString, toNumber)

import Halogen.HTML.Autocomplete as AC

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP

removeMark = "✖"

type State = { excludes ∷ Array Payer
             , includes ∷ Array Payer
             , excSugg  ∷ Array Payer
             , incSugg  ∷ Array Payer
             , excQuery ∷ String
             , incQuery ∷ String
             , limit    ∷ Int
             , as       ∷ AuthState
             , sliderId ∷ ElementId }

type Input = AuthState
type Message = DoctorFilter

data PayerSearchId
  = ExcludesId
  | IncludesId

data Query a
  = UpdateExcQuery String a
  | UpdateIncQuery String a
  | UpdateLimit Int a
  | SelectExclude Payer a
  | SelectInclude Payer a
  | Remove PayerSearchId Payer a
  | ApplyFilter a
  | HandleInput Input  a
  | Init a

component :: ∀ eff. H.Component HH.HTML Query Input Message (MyMonad eff)
component =
  H.lifecycleComponent
  { initialState: initialState
  , render
  , eval
  , initializer: Just (H.action Init)
  , finalizer: Nothing
  , receiver: HE.input HandleInput
  }

  where
    initialState input = { excludes: [], includes: []
                         , excSugg:  [], incSugg:  []
                         , excQuery: "",  incQuery: ""
                         , as: input
                         , limit: doctorResultLimit
                         , sliderId: "FilterSlider"}

    render :: State -> H.ComponentHTML Query
    render state =
      HH.div_ [
      HH.div_
      [ HH.h5_ [ HH.text "Doctor Filter" ]]
      , moneyFilter state.sliderId
      , payerFilter state
      , limitFilter state.limit
      , applyButton "Apply Filters"
      ]

    eval :: Query ~> H.ComponentDSL State Query Message (MyMonad eff)
    eval = case _ of
      Init next -> do
        sId ← H.gets _.sliderId
        limit ← H.gets _.limit
        H.liftEff $ initSlider sId [0, sliderMax] [sliderStart, sliderEnd] sliderMax
        H.raise { payRange: [sliderStart, sliderEnd]
                , limit: limit
                , excludePayers: []
                , includePayers: [] }
        pure next
      UpdateLimit limit next → do
        if (limit <= doctorResultLimitMax)
          then H.modify (_ { limit = limit })
          else H.modify (_ { limit = doctorResultLimit })
        pure next
      UpdateExcQuery q next -> do
        H.modify (_ { excQuery = q })
        if q == ""
          then do
            H.modify (_ { excSugg = [] })
            pure next
          else do
            updateAutoSelect ExcludesId q
            pure next
      UpdateIncQuery q next -> do
        H.modify (_ { incQuery = q })
        if q == ""
          then do
            H.modify (_ { incSugg = [] })
            pure next
          else do
            updateAutoSelect IncludesId q
            pure next
      SelectExclude newPayer next -> do
        s ← H.get
        H.modify (_ {excQuery="", excSugg = [], excludes = s.excludes<>[newPayer]})
        pure next
      SelectInclude newPayer next -> do
        s ← H.get
        H.modify (_ {incQuery="", incSugg = [], includes = s.includes<>[newPayer]})
        pure next
      Remove psId payer next → do
        deletePayer psId payer
        pure next
      ApplyFilter next → do
        range ← H.liftEff $ getRange
        s ← H.get
        H.raise { payRange: applyMax range sliderMax maxNum
                , limit: s.limit
                , excludePayers: s.excludes
                , includePayers: s.includes }
        pure next
      HandleInput authState next -> do
        H.modify (_ { as = authState })
        pure next

--------------------- View Pieces -------------------------
moneyFilter eId = HH.div [ HP.id_ eId
                         , HP.class_ (HH.ClassName "slider") ] []

--set the last number in the range to the biggest number if it's the range's max
--i.e. $1,000,000 will be $1,000,000+
applyMax ∷ Range → Int → Int → Range
applyMax range rangeMax biggestNumber =
  let first  = fromMaybe 0 (head range)
      second = fromMaybe 0 $ f <$> (last range)
  in [first, second]
  where f x = if x == rangeMax then biggestNumber else x

limitFilter ∷ Int → H.ComponentHTML Query
limitFilter limit =
  HH.input [ HP.type_ HP.InputNumber
           , HP.value $ show limit
           , HP.placeholder "Max doctors"
           , HE.onValueInput
             (HE.input (\val → UpdateLimit $ num val))
           , HP.min $ toNumber 0
           , HP.max $ toNumber doctorResultLimitMax ]
  where num = (fromMaybe 0) ∘ fromString

payerFilter ∷ State → H.ComponentHTML Query
payerFilter state =
  let excConfig = { containerClass: (HH.ClassName "autocomplete-container")
                  , prefix: "autocomplete-suggestion"
                  , placeholder: "Exclude company"
                  , selectQuery: SelectExclude
                  , valueInputQuery: UpdateExcQuery }
      incConfig = { containerClass: (HH.ClassName "autocomplete-container")
                  , prefix: "autocomplete-suggestion"
                  , placeholder: "Include company"
                  , selectQuery: SelectInclude
                  , valueInputQuery: UpdateIncQuery }
  in HH.div [ HP.class_ (HH.ClassName "payerFilterContainer") ]
     [
       HH.div_
       [ HH.span_ [ AC.widget excConfig state.excQuery state.excSugg
                  , payerList ExcludesId state.excludes ]
       ]
     , HH.div_
       [ HH.span_ [ AC.widget incConfig state.incQuery state.incSugg
                  , payerList IncludesId state.includes ]
       ]
     ]

payerList payerSearchId payers =
  HH.ul [] $ map mkLi payers
  where mkLi (Payer p) =
          HH.li [] [ HH.text p.name
                   , HH.a [ HE.onClick (HE.input_ (Remove payerSearchId (Payer p))) ]
                     [ HH.text removeMark ]
                   ]

applyButton label =
  HH.button
  [ HP.title label
  , HE.onClick (HE.input_ $ ApplyFilter)
  , HP.classes [ HH.ClassName "btn"
               , HH.ClassName "btn-primary"
               , HH.ClassName "tight-btn"
               ]
  ]
  [ HH.text label ]

-----------------------         --------------------------
deletePayer ExcludesId payer = do
  s ← H.get
  H.modify (_ { excludes = delete payer s.excludes })
deletePayer IncludesId payer = do
  s ← H.get
  H.modify (_ { includes = delete payer s.includes })

updateAutoSelect payerSearchId query = do
  s <- H.get
  r <- H.liftAff $ getPayers s.as.apiUrl s.as.token
    ( PayerSearchQ { payerName: query } )
  handleError r payerSearchId

handleError (Left error) _ = H.liftEff $ log $ show error
handleError (Right payers) ExcludesId = H.modify (_ { excSugg = payers })
handleError (Right payers) IncludesId = H.modify (_ { incSugg = payers })

getPayers ∷ ∀ aff. ApiUrl -> IdToken → PayerSearchQ
          → Aff ( ajax :: AJAX | aff ) (Either ErrorMsg (Array Payer))
getPayers apiUrl token query = do
  eitherRes <- apiPostRequest (apiUrl <> "payers/search") token $ encodeJson query
  pure $ (fromMaybe []) <$> eitherRes
