module Container where

import Tight.Prelude

import Types (MyMonad, AuthState(..), City(..), Doctor(..))
import Network.Auth.AuthZero (IdToken, ApiUrl, ErrorMsg(..))
import Data.Either.Nested (Either7)
import Control.Monad.Eff.Console (log, logShow, CONSOLE)
import Data.Functor.Coproduct.Nested (Coproduct7)
import Control.Monad.Aff.Bus as Bus

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.Component.ChildPath as CP
import Halogen.Component.Utils (busEventSource)
import Halogen.Query.EventSource as ES

import Config as C
import CitySearch as CS
import DoctorSearch as DS
import CompanyView as CV
import Filter as Filter
import Tabs as Tabs
import DoctorInfo as DoctorInfo

data Query a
  = Init a
  | HandleTabs Tabs.Message a
  | HandleCitySearch CS.Message a
  | HandleDoctorSearch DS.Message a
  | HandleCompany CV.SOMessage a
  | HandleFilter Filter.Message a
  | HandleGlobalError ErrorMsg a
  | ToggleCompanyView a
  | Action ErrorMsg a

type State = { as ∷ AuthState
             , loggedIn ∷ Boolean
             , companyView ∷ Boolean
             , currDoctor ∷ Maybe Doctor }

type ChildQuery = Coproduct7 CS.Query Filter.Query Tabs.Query DoctorInfo.Query DS.Query CV.Query CV.SOQuery
type ChildSlot = Either7 Unit Unit Unit Unit Unit Unit Unit

ui :: ∀ eff. IdToken → H.Component HH.HTML Query Unit Void (MyMonad eff)
ui token =
  H.lifecycleParentComponent
  { initialState: const initialState
  , render
  , eval
  , receiver: const Nothing
  , initializer: Just (H.action Init)
  , finalizer: Nothing
  }
  where

    initialState :: State
    initialState = { as: { apiUrl: C.apiUrl, token: token
                         , errorBus: Nothing }
                   , loggedIn: true
                   , companyView: false
                   , currDoctor: Nothing}

    render :: State → H.ParentHTML Query ChildQuery ChildSlot (MyMonad eff)
    render state =
      HH.div_
      [   HH.img [ HP.src "img/logo1-smaller.png"
                 , HP.id_ "mainLogo" ]
        , HH.div [ HP.class_ (HH.ClassName "row")
               , HP.id_ "container" ]
        if state.loggedIn
        then [
          HH.div [ HP.class_ (HH.ClassName "col-md-6") ]
          [
            HH.div [ HP.class_ $ HH.ClassName "row" ]
            [
              HH.div [ HP.class_ $ HH.ClassName "col-md-4" ]
              [
                HH.slot' CP.cp1 unit CS.component state.as (HE.input HandleCitySearch)
              ]
            , HH.div [ HP.class_ $ HH.ClassName "col-md-4" ]
              [
                HH.slot' CP.cp5 unit DS.component state.as (HE.input HandleDoctorSearch)
              ]
            , HH.div [ HP.class_ $ HH.ClassName "col-md-4" ]
              [
                HH.slot' CP.cp7 unit CV.searchOnlyComponent state.as (HE.input HandleCompany)
              ]
            ]
          , HH.slot' CP.cp3 unit Tabs.component state.as (HE.input HandleTabs)
          ]
          , HH.div [ HP.class_ (HH.ClassName "col-md-6") ]
            if state.companyView then companyView state else filterView state
          ]
        else [ HH.div [ HP.class_ $ HH.ClassName "col-md-3"] [ HH.text "Not logged in" ]]
      ]

    eval :: Query ~> H.ParentDSL State Query ChildQuery ChildSlot Void (MyMonad eff)
    eval = case _ of
      Init next → do
        as ← H.gets _.as
        bus ← H.liftAff $ Bus.make
        H.modify (_ { as = { apiUrl: as.apiUrl, token: as.token
                           , errorBus: Just bus }})
        H.subscribe $ busEventSource (flip HandleGlobalError ES.Listening) bus
        pure next
      HandleTabs (Tabs.DoctorSelected doctor) next → do
        H.modify (_ { currDoctor = Just doctor })
        pure next
      HandleCitySearch (CS.CitySelected city) next → do
        _ ← H.query' CP.cp3 unit (Tabs.LoadCityMap' city unit)
        H.modify (_ { companyView = false })
        pure next
      HandleDoctorSearch (DS.DoctorSelected doctor) next → do
        let (Doctor d) = doctor
            city = City { city: "", state: "", numDocs: 1
                        , lat: fromMaybe 0.0 d.lat, lng: fromMaybe 0.0 d.lng}
        _ ← H.query' CP.cp3 unit (Tabs.LoadCityMap' city unit)
        H.modify (_ { currDoctor = Just doctor, companyView = false })
        pure next
      HandleFilter doctorFilter next → do
        _ ← H.query' CP.cp3 unit (Tabs.FilterDoctors doctorFilter unit)
        pure next
      HandleCompany (CV.SOCompanySelected payer) next → do
        H.modify (_ { companyView = true })
        _ ← H.query' CP.cp6 unit (CV.SelectCompany payer unit)
        pure next
      HandleGlobalError errorMsg next → do
        H.liftEff $ logShow errorMsg
        pure next
      ToggleCompanyView next → do
        cv ← H.gets _.companyView
        H.modify (_ { companyView = not cv })
        pure next
      Action errorMsg next → do
        bus ← H.gets (_.errorBus ∘ _.as)
        case bus of
          Nothing  → pure next
          (Just b) → do
            H.liftAff $ Bus.write errorMsg b
            pure next

filterView state =
  [ toggleCompanyViewButton "Search Doctors by Company"
  , HH.slot' CP.cp2 unit Filter.component state.as (HE.input HandleFilter)
  , HH.slot' CP.cp4 unit DoctorInfo.component { as: state.as, doctor: state.currDoctor } absurd
  ]

companyView state =
  [
    toggleCompanyViewButton "← Back"
  , HH.slot' CP.cp6 unit CV.component state.as absurd
  ]

toggleCompanyViewButton label =
  HH.button [ HP.title label
            , HE.onClick (HE.input_ $ ToggleCompanyView)
            , HP.classes [ HH.ClassName "btn"
                         , HH.ClassName "btn-danger" ]
            ]
  [ HH.text label ]


actionButton label errorMsg =
  HH.button
  [ HP.title label
  , HE.onClick (HE.input_ $ Action errorMsg)
  , HP.classes [ HH.ClassName "btn"
               , HH.ClassName "btn-danger"
--               , HH.ClassName "tight-btn"
               ]
  ]
  [ HH.text label ]
