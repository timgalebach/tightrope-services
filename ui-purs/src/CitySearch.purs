module CitySearch where

import Tight.Prelude

import Types (City(..), MyMonad, CitySearchQ(..), AuthState(..))
import Data.Array (length, mapWithIndex)
import Network.Auth.AuthZero (apiPostRequest, IdToken, ApiUrl, ErrorMsg(..))
import Control.Monad.Aff (Aff)
import Control.Monad.Eff.Console (log, logShow)
import Network.HTTP.Affjax (AJAX)
import Data.Argonaut (class EncodeJson, encodeJson)

import Halogen.HTML.Autocomplete as AC

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP


type State = { cities :: Array City
             , query  :: String
             , as     :: AuthState }

type Input = AuthState

data Message = CitySelected City

data Query a
  = UpdateQuery String a
  | SelectCity City a
  | HandleInput Input a

component :: ∀ eff. H.Component HH.HTML Query Input Message (MyMonad eff)
component =
  H.component
  { initialState: initialState
  , render
  , eval
  , receiver: HE.input HandleInput
  }

  where
    initialState input = { cities: [], query: "", as: input }

    render ∷ State -> H.ComponentHTML Query
    render state =
      let acConfig = { containerClass: (HH.ClassName "autocomplete-container")
                     , prefix: "autocomplete-suggestion"
                     , placeholder: "Search for a city..."
                     , selectQuery: SelectCity
                     , valueInputQuery: UpdateQuery }
      in AC.widget acConfig state.query state.cities

    eval :: Query ~> H.ComponentDSL State Query Message (MyMonad eff)
    eval = case _ of
      UpdateQuery q next -> do
        H.modify (_ { query = q })
        if q == ""
          then do
            H.modify (_ { cities = [] })
            pure next
          else do
            s <- H.get
            r <- H.liftAff $ getCities s.as.apiUrl s.as.token
              ( CitySearchQ { cityName: q } )
            handleError r
            pure next
      SelectCity newCity next -> do
        H.modify (_ { query = "", cities = [] } )
        H.raise (CitySelected newCity)
        pure next
      HandleInput authState next -> do
        H.modify (_ { as = authState })
        pure next

--------------------- Utils ------------------------------
handleError (Left error) = H.liftEff $ log $ show error
handleError (Right cities) = H.modify (_ { cities = cities })

getCities :: ∀ aff. ApiUrl -> IdToken -> CitySearchQ
          -> Aff ( ajax :: AJAX | aff ) (Either ErrorMsg (Array City))
getCities apiUrl token query = do
  eitherRes <- apiPostRequest (apiUrl <> "city/search") token $ encodeJson query
  pure $ (fromMaybe []) <$> eitherRes
