module Config where

import Prelude

clientId   = "Uvc6a2M7XyeENfMXL4G0pqc9wMn6zVtv"

authDomain = "tightropecp.auth0.com"

apiUrl     = "http://35.173.197.207/api/"

citiesCenter  = { lat: 34.05,  lng: -118.24 }

doctorsCenter = { lat: 40.0, lng: -85.0 }

blankBounds = { sw_lat: 0.0, sw_lng: 0.0
              , ne_lat: 0.0, ne_lng: 0.0 }

sliderStart = 0
sliderEnd = 500000
sliderMax = 1000000
maxNum = 2147483647
doctorResultLimit = 200
doctorResultLimitMax = 500
