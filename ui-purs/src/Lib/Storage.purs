module Control.Monad.Eff.Storage where

import Prelude
import Control.Monad.Eff (Eff, kind Effect)
import Control.Monad.Except (runExcept)
import Data.Foreign (Foreign, readString)
import Data.Either (Either(..), either)
import Data.Maybe  (Maybe(..))

foreign import data STORAGE :: Effect

foreign import setItem :: ∀ eff. String -> String -> Eff (storage :: STORAGE | eff) Unit

foreign import getItem :: ∀ eff. String -> Eff (storage :: STORAGE | eff) Foreign

foreign import deleteItem :: ∀ eff. String -> Eff (storage :: STORAGE | eff) Unit

getItemMS :: ∀ eff . String -> Eff (storage :: STORAGE | eff) (Maybe String)
getItemMS key = do
  val <- getItem key
  pure $ either (const Nothing) (Just <<< id) $ runExcept (readString val)
