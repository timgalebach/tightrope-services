module Data.Visual.GMaps
       (
         GMAPS
       , class InfoWindow
       , class Fuzzify
       , class EncodeMarker
       , class EncodeMapId
       , mapId
       , fuzzify
       , content
       , mkMarker
       , Bounds (..)
       , BoundsRecord
       , Marker (..)
       , MarkerRecord
       , Coords
       , ZoomLevel
       , LatLng
       , MapId
       , MarkerShape (..)
       , HTMLMarkup
       , HTMLColor
       , getBounds
       , initMap
       , refreshMaps
       , mapMoved
       , addMarker
       , clearMarkers
       , shiftMap
       ) where

import Prelude
import Data.Foldable (traverse_)
import Control.Monad.Eff (Eff(..), kind Effect)
import Control.Monad.Eff.Random (RANDOM)
import Control.Monad.Eff.Console (log, logShow, CONSOLE)
import Control.Monad.Aff (makeAff, Aff)

foreign import data GMAPS :: Effect

class InfoWindow a where
  content :: a -> HTMLMarkup

class Fuzzify a where
  fuzzify :: ∀ e. a -> Eff (random :: RANDOM | e) LatLng

class (InfoWindow a, Fuzzify a) <= EncodeMarker a where
  mkMarker :: ∀ e. a -> Eff (random :: RANDOM | e) Marker

class EncodeMapId a where
  mapId :: a -> MapId

type LatLng = { lat :: Number, lng :: Number }

type MapId = String
type BoundsRecord = { sw_lat :: Number, sw_lng :: Number
                    , ne_lat :: Number, ne_lng :: Number }
newtype Bounds = Bounds BoundsRecord

instance showBounds :: Show Bounds where
  show (Bounds b) = "[" <> show b.sw_lat <> ", " <> show b.sw_lng <> ", "
    <> show b.ne_lat <> ", " <> show b.ne_lng <> "]"
derive instance eqBounds :: Eq Bounds

type Coords = { lat :: Number, lng :: Number }
type ZoomLevel = Int

type MarkerRecord = { shape     :: MarkerShape
                    , color     :: HTMLColor
                    , lat       :: Number
                    , lng       :: Number
                    , iwContent :: HTMLMarkup
                    , scale     :: Number }

newtype Marker = Marker MarkerRecord

instance showMarker :: Show Marker where
  show (Marker m) = "(" <> show m.lat <> ", " <> show m.lng <> " " <> m.iwContent

data MarkerShape = Circle

type HTMLColor  = String
type HTMLMarkup = String

--takes callback with String just to pass some value
foreign import initMapImpl :: ∀ e. (String -> Eff ( gmaps :: GMAPS | e ) Unit)
                           -> MapId -> Coords -> Eff ( gmaps :: GMAPS | e ) Unit

foreign import refreshMaps :: ∀ eff. Array MapId -> Eff ( gmaps :: GMAPS | eff ) Unit

foreign import getBoundsImpl :: ∀ e. MapId -> Eff ( gmaps :: GMAPS | e ) BoundsRecord

foreign import shiftMap :: ∀ e. MapId -> Coords -> ZoomLevel
                        -> Eff ( gmaps :: GMAPS | e ) Unit

foreign import mapMovedImpl :: ∀ a e. MapId
                 -> (BoundsRecord -> Eff ( gmaps :: GMAPS | e ) a)
                 -> Eff ( gmaps :: GMAPS | e ) (Eff ( gmaps :: GMAPS | e) Unit)

foreign import clearMarkers :: ∀ e. MapId -> Eff (gmaps :: GMAPS | e) Unit

foreign import addMarkerImpl :: ∀ a e. MapId -> MarkerRecord
  -> (Int -> Eff (gmaps :: GMAPS, random :: RANDOM | e) a)
  -> Eff (gmaps :: GMAPS, random :: RANDOM | e) (Eff (gmaps :: GMAPS, random :: RANDOM | e) Unit)

--uses Aff so that it can delay return until map is fully loaded
initMap :: ∀ e. MapId -> Coords -> Aff ( gmaps :: GMAPS | e ) String
initMap mi cs = makeAff (\err succ -> initMapImpl succ mi cs)

getBounds :: ∀ e. MapId -> Eff ( gmaps :: GMAPS | e ) Bounds
getBounds mapId = Bounds <$> getBoundsImpl mapId

mapMoved :: ∀ e. MapId -> (BoundsRecord -> Eff ( gmaps :: GMAPS | e ) Unit)
            -> Eff ( gmaps :: GMAPS | e ) (Eff ( gmaps :: GMAPS | e) Unit)
mapMoved mapId callback = mapMovedImpl mapId callback

addMarker :: ∀ a e. (EncodeMarker a) => MapId -> a
          -> (Int -> Eff (gmaps :: GMAPS, random :: RANDOM | e) Unit)
          -> Eff (gmaps :: GMAPS, random :: RANDOM | e) (Eff (gmaps :: GMAPS, random :: RANDOM | e) Unit)
addMarker mapId em callback = do
  marker <- mkMarker em
  addMarkerImpl mapId (unwrapMarker marker) callback
  where unwrapMarker (Marker m) = m
