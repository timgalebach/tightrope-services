module Network.HTTP.FileUpload
       (
         upload
       , FileElementId
       , RemoteUrl
       ) where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Aff (Aff, makeAff)

type FileElementId = String
type RemoteUrl     = String

foreign import uploadImpl :: ∀ e. (String -> Eff e Unit)
                          -> RemoteUrl -> FileElementId -> Eff e Unit

upload :: ∀ e. RemoteUrl -> FileElementId -> Aff e String
upload u feId = makeAff (\error success -> uploadImpl success u feId)
