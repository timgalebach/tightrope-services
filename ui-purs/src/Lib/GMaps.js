"use strict";

/*
Library to access Google Maps.
Stores states of listeners and maps to avoid duplicate loading or duplicate initializing, to make calls to functions idempotent.
*/

/******************* STATE VARS ********************/
var maps = {};
var listeners = {};
var markers = {};

var globalInfoWindow = null;


/*********************** MARKER FNS *************************/
exports.pinSymbol = function(color, scale) {
    var circlePath = 'M1664 896q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z';
    return {
        path: circlePath,
        fillColor: color,
        fillOpacity: 0.6,
        strokeColor: '#fff',
        strokeWeight: 1,
        scale: scale
    };
};

exports.addMarkerImpl = function (mapId) {
    return function(markerObj) {
        return function(callback) {
            return function() {
                var map = maps[mapId];
                var m = new google.maps.Marker({
                    position: { lat: markerObj.lat, lng: markerObj.lng },
                    map: map,
                    icon: exports.pinSymbol(markerObj.color, markerObj.scale)
                });
                exports.infoOpenBind("mouseover", map, m, markerObj.iwContent);
                exports.infoCloseBind("mouseout", map, m);
                markers[mapId].push(m);

                m.addListener("click", function() {
                    callback(0)(); //dummy value returned
                });
            };
        };
    };
};

exports.clearMarkers = function(mapId) {
    return function() {
        for (var i=0; i < markers[mapId].length; i++) {
            markers[mapId][i].setMap(null);
        }
        markers[mapId] = [];
    };
};

/********************** INFO WINDOW FNS *********************/
exports.swapInfoWindow = function(infoWindow, map, marker) {
    exports.clearInfoWindow(globalInfoWindow);
    globalInfoWindow = infoWindow;
    globalInfoWindow.open(map, marker);
};

exports.clearInfoWindow = function() {
    if (globalInfoWindow != null) {
        globalInfoWindow.close();
    }
    globalInfoWindow = null;
};

exports.infoOpenBind = function(eventName, map, marker, contentString) {
    marker.addListener(eventName, function() {
        var iw = new google.maps.InfoWindow({
            content: contentString
        });
        exports.swapInfoWindow(iw, map, marker);
    });
};

exports.infoCloseBind = function(eventName, map, marker) {
    marker.addListener(eventName, function() {
        exports.clearInfoWindow();
    });
};

exports.cityInfoString = function(cityObj) {
    var docString = "Doctor";
    if (cityObj.numDocs > 1) { docString += "s" }
    return "<div class='infoWindow'><strong>" + cityObj.city + "</strong><br>" + cityObj.numDocs + " " + docString + "</div>";
};

exports.doctorInfoString = function(doctorObj) {
    var dName = doctorObj.first + " " + doctorObj.last;
    return "<div class='infoWindow'><strong>" + dName + "</strong><br>" +
        doctorObj.city + ", " + doctorObj.state + "<br>"
        + "<em>" + doctorObj.amount + "</em></div>";
};

/******************* Purescript Integration **********************/
exports.shiftMap = function(mapId) {
    return function(coords) {
        return function(zoomLevel) {
            return function() {
                maps[mapId].setCenter(coords);
                maps[mapId].setZoom(zoomLevel);
            };
        };
    };
};

exports.boundsToBounds = function(bounds) {
    var b = bounds.toJSON();
    return {sw_lat: b.south,
            sw_lng: b.west,
            ne_lat: b.north,
            ne_lng: b.east };
};

exports.initMapImpl = function(callback) {
    return function(mapId) {
        return function(centerCoord) {
            return function() {
                //make function idempotent -- init iff map doesn't exist
                if(maps[mapId] == undefined) {
                    console.log("Initializing map: " + mapId);
                    var mapDiv = document.getElementById(mapId);
                    var map = new google.maps.Map(mapDiv, {
                        zoom: 10,
                        mapTypeId: 'terrain',
                        disableDefaultUI: true,
                        zoomControl: true,
                        streetViewControl: false,
                        styles: styles[mapId],
                        center: centerCoord
                    });
                    //Initialize global map objects
                    maps[mapId] = map;
                    markers[mapId] = [];
                    listeners[mapId] = {};

                    listeners[mapId]["initialLoad"] = google.maps.event.addListener(map, 'idle', function() {
                        google.maps.event.removeListener(listeners[mapId]["initialLoad"]);
                        callback(mapId + " loaded.")();
                    });
                }
            };
        };
    };
};

exports.refreshMaps = function (mapIds) {
    return function() {
        for(var i=0; i<mapIds.length; i++) {
            google.maps.event.trigger(maps[mapIds[i]], 'resize');
        }
    };
};

exports.getBoundsImpl = function(mapId) {
    return function() {
        return exports.boundsToBounds(maps[mapId].getBounds());
    };
};

exports.mapMovedImpl = function(mapId) {
    return function(callback) {
        return function () {
            var map = maps[mapId];
            var bounds = function() {
                return exports.boundsToBounds(map.getBounds());
            };
            if (listeners[mapId]["mapMoved"] == undefined) {
                google.maps.event.addListener(map,'idle', function() {
                    callback(bounds())();
                });
                console.log("Attached new Listener on mapMoved for: " + mapId);
                listeners[mapId]["mapMoved"] = true;
            }
        };
    };
};

/*********************** Styles *********************/
var styles =
        { "default" : [],
          "MapTabCitiesId": [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-60},{"lightness":55}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}],
          "MapTabDoctorsId": [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text.fill","stylers":[{"hue":"#ff0000"},{"visibility":"simplified"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"on"},{"weight":"2.07"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#0a6589"},{"visibility":"on"}]}]
        };
