module Halogen.HTML.Autocomplete
       ( widget
       , menuString
       , class MenuDisplay
       , ClassPrefix
       , Config
       ) where

import Prelude
import Data.Array (length, mapWithIndex)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.Query as HQ

class MenuDisplay a where
  menuString ∷ a → String

type ClassPrefix = String

--a is the type of the suggestion elements
--f is the query algebra kind
--selectQuery and viQuery are just query algebra constructors
--prefix is like "autocomplete-suggestion", prefix for CSS purposes
type Config a f = { containerClass  ∷ HH.ClassName
                  , prefix          ∷ ClassPrefix
                  , placeholder     ∷ String
                  , selectQuery     ∷ (a → HQ.Action f)
                  , valueInputQuery ∷ (String → HQ.Action f) }

widget ∷ ∀ a q. (MenuDisplay a) => Config a q → String → Array a
       → H.ComponentHTML q
widget config query elts =
  let title = ""
  in HH.div_
     [
       HH.div_ [ HH.text title ]
     , HH.div_
       [ searchBox query config.placeholder config.valueInputQuery ]
     , HH.div [HP.class_ config.containerClass]
       [ suggestions config.prefix config.selectQuery elts]
     ]

searchBox ∷ ∀ f. String → String → (String → HQ.Action f) → H.ComponentHTML f
searchBox query placeholder valueInputQuery =
  HH.input
  [ HP.type_ HP.InputText
  , HP.placeholder placeholder
  , HP.value query
  , HE.onValueInput (HE.input valueInputQuery)
  ]

suggestions ∷ ∀ a f. (MenuDisplay a) => ClassPrefix → (a → HQ.Action f) → Array a
            → H.ComponentHTML f
suggestions prefix selectQuery elts =
  HH.ul_ $ mapWithIndex (display (length elts)) elts
  where display numElts index elt =
          HH.li [ HP.classes $ map HH.ClassName $ [ prefix ]
                  <> liClass prefix numElts index
                , HE.onClick (HE.input_ $ selectQuery elt) ]
          [ HH.text $ menuString elt ]
        liClass base nElts index
          | index == 0      = [ base <> "-first"
                              , base <> "-odd" ]
          | index == (nElts-1) = [ base <> "-last"
                                 , base <>
                                if index `mod` 2 == 1
                                then "-even"
                                else "-odd" ]
          | index `mod` 2 == 1 = [ base <> "-even" ]
          | otherwise          = [ base <> "-odd" ]
