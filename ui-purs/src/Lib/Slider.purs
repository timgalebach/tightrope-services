module Data.Filter.Slider
       (
         SLIDER
       , ElementId
       , Range
       , MinMax
       , SliderMax
       , initSlider
       , getRange
       ) where

import Prelude
import Data.Int (floor)
import Control.Monad.Eff (Eff(..), kind Effect)

foreign import data SLIDER :: Effect

type ElementId = String
type MinMax    = Array Int
type SliderMax = Int
type Range     = Array Int
type Dummy     = Int

foreign import initSlider ∷ ∀ e. ElementId → Range → MinMax → SliderMax
                          → Eff (slider :: SLIDER | e) Unit

foreign import getRangeImpl ∷ ∀ e. Dummy → Eff (slider :: SLIDER | e) (Array Number)

getRange ∷ ∀ e. Eff (slider :: SLIDER | e) (Array Int)
getRange = do
  range ← getRangeImpl 0
  pure $ map floor range
