"use strict";

exports.authenticateImpl = function(callback) {
    return function(clientId) {
        return function(authDomain) {
            var options = {
                languageDictionary: {
                    title: "Log In"
                },
                theme: {
                    logo: "img/logo1-dark.png",
                    primaryColor: "#ba3a4f"
                }
            };
            var lock = new Auth0Lock(clientId, authDomain, options);
            lock.on("authenticated", function(authResult) {
                lock.hide();
                callback(authResult.idToken)();
            });
            return function() {
                lock.show();
            };
        };
    };
};
