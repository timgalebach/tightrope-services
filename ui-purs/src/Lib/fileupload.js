//requires access to jQuery >= 3.1.1

"use strict";

exports.uploadImpl = function(callback) {
    return function(fileElementId) {
        if (document.getElementById(fileElementId) == null) {
            console.log("FileUpload: element id does not exist.");
        }
        return function(remoteUrl) {
            return function() {
                var f = document.getElementById(fileElementId).files[0];
                var fd = new FormData();
                fd.append( "file", f );
                $.ajax({
                    url: remoteUrl,
                    data: fd,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function(data){
                        console.log("success");
                        callback(data)();
                    }
                });
            }
        }
    }
}
