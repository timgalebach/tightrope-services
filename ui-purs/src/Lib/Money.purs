module Data.Format.Money
       (
         formatDollar
       ) where

foreign import formatDollar :: Number → String
