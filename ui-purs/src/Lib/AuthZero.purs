--requires version 10.8 of Auth0 Lock js include
module Network.Auth.AuthZero
       (
         authenticate
       , loadOrRefreshToken
       , logout
       , checkToken
       , apiPostRequest
       , ErrorMsg(..)
       , IdToken
       , ApiUrl
       ) where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Storage(getItemMS, setItem, deleteItem, STORAGE)
import Control.Monad.Eff.Class (liftEff)
import Control.Monad.Aff (Aff, makeAff, attempt)
import Control.Monad.Eff.Console (log, CONSOLE, logShow)
import Data.Maybe (Maybe(..), maybe')
import Data.Either (Either(..), either)
import Data.Bifunctor (bimap)
import Data.Argonaut (class DecodeJson, class EncodeJson, Json, encodeJson, decodeJson, jsonParser)
import Data.HTTP.Method (Method(..))
import Network.HTTP.Affjax (affjax, defaultRequest, AJAX)
import Network.HTTP.RequestHeader (RequestHeader(..))

type ClientId   = String
type AuthDomain = String
type IdToken    = String
type ApiUrl     = String

data ErrorMsg = AuthError | JsonError
instance showErrorMsg :: Show ErrorMsg where
  show AuthError = "AuthError"
  show JsonError = "JsonError"

foreign import authenticateImpl :: ∀ e. (String -> Eff e Unit) -> ClientId
                                -> AuthDomain
                                -> Eff e Unit

authenticate :: ∀ e. ClientId -> AuthDomain -> Aff e IdToken
authenticate cId aDom = makeAff (\error success -> authenticateImpl success cId aDom)

loadOrRefreshToken :: ∀ e. ClientId -> AuthDomain -> ApiUrl -> Aff (storage :: STORAGE, ajax :: AJAX | e ) IdToken
loadOrRefreshToken ci ad apiUrl = do
  maybeToken <- liftEff loadToken
  maybe' (\_ -> authAndStore ci ad)
    (\token -> checkAndStore ci ad apiUrl token) maybeToken

logout :: ∀ e. Aff (storage :: STORAGE, ajax :: AJAX | e ) Unit
logout = deleteToken

checkToken :: ∀ a. ApiUrl -> IdToken -> Aff ( ajax :: AJAX | a ) (Maybe IdToken)
checkToken url token = do
  r <- attempt $ affjax $ defaultRequest
    { headers = [tokenHeader token], url = url, method = Left GET }
  pure $ either (const Nothing) returnToken r
    where returnToken httpResp = (const (Just token)) (httpResp.response :: String)


apiPostRequest :: ∀ a b aff. EncodeJson a => DecodeJson b => ApiUrl -> IdToken -> a
               -> Aff (ajax :: AJAX | aff) (Either ErrorMsg (Maybe b))
apiPostRequest apiUrl token query = do
  r <- attempt $ affjax $ defaultRequest
    { headers = [tokenHeader token], url = apiUrl, method = Left POST
    , content = Just $ encodeJson query }
  pure $ do
    httpResp <- mapErr AuthError r
    json <- mapErr JsonError $ jsonParser (httpResp.response :: String)
    mapErr JsonError $ decodeJson json


---------------------- private functions -----------------------
mapErr :: ∀ a b. ErrorMsg -> Either a b -> Either ErrorMsg b
mapErr err = bimap (const err) id

checkAndStore :: ∀ a. ClientId -> AuthDomain -> ApiUrl -> IdToken
              -> Aff ( ajax :: AJAX, storage :: STORAGE | a ) IdToken
checkAndStore ci ad apiUrl token = do
  maybeToken <- checkToken apiUrl token
  maybe' (\_ -> authAndStore ci ad) (\t -> pure t) maybeToken

tokenHeader :: IdToken -> RequestHeader
tokenHeader token = RequestHeader "Authorization" $ "Bearer " <> token

storeToken :: ∀ e. IdToken -> Eff ( storage :: STORAGE | e) Unit
storeToken = setItem "token"

deleteToken :: ∀ e. Aff (storage :: STORAGE | e) Unit
deleteToken = liftEff $ deleteItem "token"

loadToken :: ∀ e. Eff ( storage :: STORAGE | e) (Maybe IdToken)
loadToken = getItemMS "token"

authAndStore :: ∀ a. ClientId -> AuthDomain -> Aff ( storage :: STORAGE | a ) IdToken
authAndStore ci ad = do
  token <- authenticate ci ad
  liftEff $ storeToken token
  pure token
