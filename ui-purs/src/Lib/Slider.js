///////////////////////////
// uses nouislider.min.js
//      wNumb.js
///////////////////////////
"use strict";

var slider = null;

//not idempotent
exports.initSlider = function(elementId) {
    return function (range) {
        return function(minMax) {
            return function(sliderMax) {
                return function() {
                    if (1 == 1) {
//                    if (slider == null) {
                        var s = document.getElementById(elementId);
                        slider = s;
                        var formatObj  = {prefix: "$", decimals: 0, thousand: ","};
                        var maxDisplay = function(value) {
                            if(wNumb(formatObj).to(sliderMax) == value) {
                                value += "+";
                            }
                            return value;
                        };
                        var formatPrime = Object.assign({}, formatObj);
                        formatPrime["edit"] = maxDisplay;
                        var format = wNumb(formatPrime);
                        noUiSlider.create(slider, {
                            start: minMax,
                            step: 1000,
	                    connect: true,
                            tooltips: [format, format],
	                    range: {
		                'min': range[0],
		                'max': range[1]
                            }
                        });
                    }
                };
            };
        };
    };
};

exports.getRangeImpl = function(dummy) {
    return function() {
        return slider.noUiSlider.get();
    };
};
