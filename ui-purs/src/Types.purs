module Types where

import Tight.Prelude
import Data.String (joinWith)
import Data.Format.Money (formatDollar)
import Data.Visual.GMaps (GMAPS, class InfoWindow, class EncodeMarker, class Fuzzify, MarkerShape(..), Marker(..), HTMLMarkup, HTMLColor, content, fuzzify)
import Data.Filter.Slider (SLIDER)
import Data.Argonaut (class EncodeJson, class DecodeJson, decodeJson, (:=), (~>), (.?), jsonEmptyObject)
import Data.Int (toNumber)
import Network.HTTP.Affjax (AJAX)
import Control.Monad.Eff.Console (log, CONSOLE)
import Control.Monad.Eff.Random (RANDOM, random)
import Control.Monad.Aff.AVar (AVAR)
import Control.Monad.Aff (Aff)
import Control.Monad.Aff.Bus as Bus
import Data.Argonaut.Decode.Combinators ((.??))
import Network.Auth.AuthZero (IdToken, ApiUrl, ErrorMsg(..))
import Halogen.HTML.Autocomplete (class MenuDisplay)

------------------- App Monad(s) ---------------------------
type MyMonad eff = (Aff (random :: RANDOM, avar :: AVAR, gmaps :: GMAPS, console :: CONSOLE, ajax :: AJAX , slider :: SLIDER | eff))

------------------- App State -----------------------------
type AuthState = { apiUrl :: ApiUrl, token :: IdToken
                 , errorBus :: Maybe (Bus.BusRW ErrorMsg)
                 }

type DoctorFilter = { payRange :: Array Int
                    , limit :: Int
                    , excludePayers :: Array Payer
                    , includePayers :: Array Payer }

------------------- API Queries & Instances ----------------
newtype CitySearchQ    = CitySearchQ { cityName :: String }
newtype DoctorSearchQ  = DoctorSearchQ { doctorName :: String }
newtype PayerSearchQ   = PayerSearchQ { payerName :: String }
newtype DoctorsPayerQ  = DoctorsPayerQ { payerId ∷ Number
                                       , limit   ∷ Int
                                       , payRange ∷ Array Int }
newtype DoctorBoundsQ  = DoctorBoundsQ { bounds :: Array Number
                                       , payRange :: Array Int
                                       , limit :: Int
                                       , excludePayers :: Array Number
                                       , includePayers :: Array Number }
newtype CityBoundsQ = CityBoundsQ { bounds :: Array Number }
newtype DoctorDetailQ = DoctorDetailQ { id :: Int }

instance cityqEncodeJson :: EncodeJson CitySearchQ where
  encodeJson (CitySearchQ csq) = "cityName" := csq.cityName ~> jsonEmptyObject

instance doctorSearchqEncodeJson :: EncodeJson DoctorSearchQ where
  encodeJson (DoctorSearchQ dsq) = "doctorName" := dsq.doctorName ~> jsonEmptyObject

instance payerqEncodeJson :: EncodeJson PayerSearchQ where
  encodeJson (PayerSearchQ csq) = "payerName" := csq.payerName ~> jsonEmptyObject

instance encodeJsonDoctorsPayerQ ∷ EncodeJson DoctorsPayerQ where
  encodeJson (DoctorsPayerQ dpq)
    =  "payerId"  := dpq.payerId
    ~> "payRange" := dpq.payRange
    ~> "limit"    := dpq.limit
    ~> jsonEmptyObject

instance encodeJsonDoctorBoundsQ :: EncodeJson DoctorBoundsQ where
  encodeJson (DoctorBoundsQ dbq)
    =  "bounds"          := dbq.bounds
    ~> "payRange"        := dbq.payRange
    ~> "limit"           := dbq.limit
    ~> "excludePayers"   := dbq.excludePayers
    ~> "includePayers"   := dbq.includePayers
    ~> jsonEmptyObject

instance encodeJsonCityBoundsQ :: EncodeJson CityBoundsQ where
  encodeJson (CityBoundsQ cbq)
    =  "bounds" := cbq.bounds
    ~> jsonEmptyObject

instance encodeJsonDoctorDetailQ ∷ EncodeJson DoctorDetailQ where
  encodeJson (DoctorDetailQ ddq) = "id" := ddq.id ~> jsonEmptyObject

-------------------- Datatypes -----------------------------
newtype Money = Money Number
instance showMoney :: Show Money where
  show (Money val) = formatDollar val

newtype City = City { city    :: String
                    , state   :: String
                    , numDocs :: Int
                    , lat     :: Number
                    , lng     :: Number }
instance showCity :: Show City where
  show (City c) = c.city <> ", " <> c.state

newtype Doctor = Doctor { dId :: Int
                        , first :: Maybe String
                        , middle :: Maybe String
                        , last :: Maybe String
                        , line1 :: Maybe String
                        , line2 :: Maybe String
                        , city :: Maybe String
                        , state :: Maybe String
                        , zip :: Maybe String
                        , lat :: Maybe Number
                        , lng :: Maybe Number
                        , amount :: Maybe Money }
instance showDoctor :: Show Doctor where
  show (Doctor d) = joinWith " " $
    (fromMaybe "") <$> [Just $ show d.dId, Just ":" , d.first, d.middle, d.last, Just ";  ", d.city, d.state]

newtype Payer = Payer { id :: Number, name :: String }
instance showPayer :: Show Payer where
  show (Payer p) = p.name
derive instance eqPayer :: Eq Payer

newtype PayerAmount = PayerAmount { payer  ∷ Payer
                                  , amount ∷ Money }
instance showPayerAmount ∷ Show PayerAmount where
  show (PayerAmount pa) = (show pa.payer) <> ", " <> (show pa.amount)
getAmount ∷ PayerAmount → Money
getAmount (PayerAmount pa) = pa.amount

newtype DoctorPayer = DoctorPayer { doctor      ∷ Doctor
                                  , payerAmount ∷ PayerAmount }
instance showDoctorPayer ∷ Show DoctorPayer where
  show (DoctorPayer dp) = (show dp.doctor) <> " " <> (show dp.payerAmount)

newtype DoctorDetail = DoctorDetail { doctor ∷ Doctor
                                    , payerAmounts ∷ Array PayerAmount }
instance showDoctorDetail ∷ Show DoctorDetail where
  show (DoctorDetail dd) = (show dd.doctor) <> " " <> (show dd.payerAmounts)

---------------------- Json Conversions ------------------------
instance decodeJsonCity :: DecodeJson City where
  decodeJson j = do
    o <- decodeJson j
    city    <- o .? "city"
    state   <- o .? "state"
    numDocs <- o .? "num_docs"
    lat     <- o .? "lat"
    lng     <- o .? "lng"
    pure $ City { city, state, numDocs, lat, lng}

instance decodeJsonDoctor :: DecodeJson Doctor where
  decodeJson j = do
    o <- decodeJson j
    dId     <- o .?  "id"
    first   <- o .?? "first"
    middle  <- o .?? "middle"
    last    <- o .?? "last"
    line1   <- o .?? "line1"
    line2   <- o .?? "line2"
    city    <- o .?? "city"
    state   <- o .?? "state"
    zip     <- o .?? "zip"
    lat     <- o .?? "lat"
    lng     <- o .?? "lng"
    am  <- o .?? "amount"
    let amount = Money <$> am
    pure $ Doctor { dId, first, middle, last, line1, line2, city, state, zip, lat, lng, amount }

instance decodeJsonPayer :: DecodeJson Payer where
  decodeJson j = do
    o <- decodeJson j
    id   <- o .? "payerId"
    name <- o .? "payerName"
    pure $ Payer { id, name}

instance decodeJsonPayerAmount ∷ DecodeJson PayerAmount where
  decodeJson j = do
    payer  ← decodeJson j
    o      ← decodeJson j
    amount ← o .? "amount"
    pure $ PayerAmount { payer: payer, amount: Money amount }

instance decodeJsonDoctorPayer ∷ DecodeJson DoctorPayer where
  decodeJson j = do
    payerAmount ← decodeJson j
    doctor      ← decodeJson j
    pure $ DoctorPayer { doctor, payerAmount }

instance decodeJsonDoctorDetail ∷ DecodeJson DoctorDetail where
  decodeJson j = do
    doctor ← decodeJson j
    o      ← decodeJson j
    payerAmounts ← o .? "payers"
    pure $ DoctorDetail { doctor, payerAmounts }


-------------------- GMaps Instances --------------------
instance infoWindowCity :: InfoWindow City where
  content (City city) =
    let doctors = "Doctor" <> (if city.numDocs > 1 then "s" else "")
    in "<div class='infoWindow'><strong>" <> city.city <> "</strong><br>"
       <> show city.numDocs <> " " <> doctors <> "</div>"

instance infoWindowDoctor :: InfoWindow Doctor where
  content (Doctor doctor) =
    let first  = fromMaybe "" doctor.first
        last   = fromMaybe "" doctor.last
        amount = fromMaybe (Money 0.0) doctor.amount
    in "<div class='infoWindow'><strong>" <> first <> " " <> last <> "</strong><br>"
       <> "<em>" <> show amount <> "</em></div>"

instance fuzzifyCity :: Fuzzify City where
  fuzzify (City c) = pure { lat: c.lat, lng: c.lng }

instance fuzzifyDoctor :: Fuzzify Doctor where
  fuzzify (Doctor d) = do
    let fuzzFactor = 0.01
    r1 <- random
    r2 <- random
    pure { lat: (fuzz r1 fuzzFactor) + (fromMaybe 0.0 d.lat)
         , lng: (fuzz r2 fuzzFactor) + (fromMaybe 0.0 d.lng) }
      where fuzz r ff = (r-0.5) * ff

instance encodeMarkerCity :: EncodeMarker City where
  mkMarker (City city) =
    pure $ Marker { shape: Circle
                  , color: "red"
                  , lat: city.lat
                  , lng: city.lng
                  , iwContent: content (City city)
                  , scale: 0.03 * (pinScale $ (toNumber city.numDocs) / 18000.0) }
    where pinScale ratio
            | ratio < 0.01 = 0.19
            | ratio < 0.1  = 0.35
            | ratio < 0.4  = 0.6
            | otherwise    = 1.0

instance encodeMarkerDoctor :: EncodeMarker Doctor where
  mkMarker d = do
    {lat, lng} <- fuzzify d
    pure $ Marker { shape: Circle
                  , color: "#F79102"
                  , lat: lat
                  , lng: lng
                  , iwContent: content d
                  , scale: 0.03 * 0.28 }

instance menuDisplayCity ∷ MenuDisplay City where
  menuString (City c) = c.city <> ", " <> c.state

instance menuDisplayDoctor ∷ MenuDisplay Doctor where
  menuString (Doctor d) = (fromMaybe "" d.last) <> ", " <> (fromMaybe "" d.first)
    <> " - " <> (fromMaybe "" d.city) <> ", " <> (fromMaybe "" d.state)

instance menuDisplayPayer ∷ MenuDisplay Payer where
  menuString (Payer p) = p.name
