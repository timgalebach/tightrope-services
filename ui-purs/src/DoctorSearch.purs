module DoctorSearch where

import Tight.Prelude

import Types (Doctor(..), MyMonad, DoctorSearchQ(..), AuthState(..))
import Data.Array (length, mapWithIndex)
import Network.Auth.AuthZero (apiPostRequest, IdToken, ApiUrl, ErrorMsg(..))
import Control.Monad.Aff (Aff)
import Control.Monad.Eff.Console (log, logShow)
import Network.HTTP.Affjax (AJAX)
import Data.Argonaut (class EncodeJson, encodeJson)

import Halogen.HTML.Autocomplete as AC

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP


type State = { doctors :: Array Doctor
             , query   :: String
             , as      :: AuthState }

type Input = AuthState

data Message = DoctorSelected Doctor

data Query a
  = UpdateQuery String a
  | SelectDoctor Doctor a
  | HandleInput Input a

component :: ∀ eff. H.Component HH.HTML Query Input Message (MyMonad eff)
component =
  H.component
  { initialState: initialState
  , render
  , eval
  , receiver: HE.input HandleInput
  }

  where
    initialState input = { doctors: [], query: "", as: input }

    render ∷ State → H.ComponentHTML Query
    render state =
      let acConfig = { containerClass: (HH.ClassName "autocomplete-container")
                     , prefix: "autocomplete-suggestion"
                     , placeholder: "Search for a doctor..."
                     , selectQuery: SelectDoctor
                     , valueInputQuery: UpdateQuery }
      in AC.widget acConfig state.query state.doctors

    eval :: Query ~> H.ComponentDSL State Query Message (MyMonad eff)
    eval = case _ of
      UpdateQuery q next → do
        H.modify (_ { query = q })
        if q == ""
          then do
            H.modify (_ { doctors = [] })
            pure next
          else do
            s ← H.get
            r ← H.liftAff $ getDoctors s.as.apiUrl s.as.token
              ( DoctorSearchQ { doctorName: q } )
            handleError r
            pure next
      SelectDoctor newDoctor next → do
        H.modify (_ { query = "", doctors = [] } )
        H.raise (DoctorSelected newDoctor)
        pure next
      HandleInput authState next → do
        H.modify (_ { as = authState })
        pure next

--------------------- Utils ------------------------------
handleError (Left error) = H.liftEff $ log $ show error
handleError (Right doctors) = H.modify (_ { doctors = doctors })

getDoctors :: ∀ aff. ApiUrl → IdToken → DoctorSearchQ
          → Aff ( ajax :: AJAX | aff ) (Either ErrorMsg (Array Doctor))
getDoctors apiUrl token query = do
  eitherRes ← apiPostRequest (apiUrl <> "doctors/search") token $ encodeJson query
  pure $ (fromMaybe []) <$> eitherRes
