module Tabs where

import Tight.Prelude
import Types (Doctor(..), City(..), Payer(..), MyMonad, DoctorBoundsQ(..), CityBoundsQ(..), AuthState(..), DoctorFilter)
import Config as C
import Data.Visual.GMaps as GMaps
import Control.Monad.Eff.Console (log, CONSOLE, logShow)
import Control.Monad.Eff (Eff)
import Control.Monad.Aff (Aff)
import Network.HTTP.Affjax (AJAX)
import Network.Auth.AuthZero (apiPostRequest, IdToken, ApiUrl, ErrorMsg(..))
import Data.Argonaut (class EncodeJson, encodeJson)
import Data.Array as Arr

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.Query.EventSource as ES

type State = { as          ∷ AuthState
             , selectedTab ∷ TabId
             , loading     ∷ Boolean
             , cs          ∷ Tab City
             , ds          ∷ Tab Doctor
             , filter      ∷ Maybe DoctorFilter }
type Input = AuthState
data Message
  = DoctorSelected Doctor

data Query a
  = HandleInput Input a
  | Init a
  | Final a
  | TabSelect TabId a
  | BoundsChanged TabId GMaps.Bounds (H.SubscribeStatus → a)
  | NoOp (H.SubscribeStatus → a)
  | LoadDoctorMap City (H.SubscribeStatus → a)
  | LoadDoctorMap' City a
  | LoadCityMap' City a
  | FilterDoctors  DoctorFilter a
  | DoctorClick Doctor (H.SubscribeStatus → a)

data TabId
  = CitiesId
  | DoctorsId
derive instance eqTabId :: Eq TabId
instance showTabId :: Show TabId where
  show CitiesId  = "MapTabCitiesId"
  show DoctorsId = "MapTabDoctorsId"

type Tab a = { id :: TabId, elts :: Array a
             , center :: GMaps.Coords }

component :: ∀ eff. H.Component HH.HTML Query Input Message (MyMonad eff)
component =
  H.lifecycleComponent
  { initialState: initialState
  , render
  , eval
  , initializer: Just (H.action Init)
  , finalizer: Just (H.action Final)
  , receiver: HE.input HandleInput
  }

  where
    initialState input = { as: input, selectedTab: CitiesId
                         , loading: false
                         , cs: blankTab CitiesId  C.citiesCenter
                         , ds: blankTab DoctorsId C.doctorsCenter
                         , filter: Nothing}

    render ∷ State → H.ComponentHTML Query
    render state =
      HH.div [ HP.id_ "tabContainer" ]
      [
        tabHeader state
      , renderTab state.selectedTab state.cs
      , renderTab state.selectedTab state.ds
      ]

    eval ∷ Query ~> H.ComponentDSL State Query Message (MyMonad eff)
    eval = case _ of
      Final next → do
        H.liftEff $ log "Finalizing"
        pure next
      Init next → do
        s ← H.get
        initMaps s next
      TabSelect tabId next → do
        selectTab tabId
        pure next
      BoundsChanged tabId bounds reply → do
        state ← H.get
        case tabId of
          CitiesId  → do
            loadCities state tabId bounds
          DoctorsId → do
            loadDoctors state tabId bounds
        pure (reply H.Listening)
      NoOp reply →  pure (reply H.Listening)
      LoadDoctorMap city reply → do
        loadDoctorMap city
        pure (reply H.Listening)
      LoadDoctorMap' city next → do
        loadDoctorMap city
        pure next
      LoadCityMap' city next → do
        loadCityMap city
        pure next
      FilterDoctors doctorFilter next → do
        H.modify (_ { filter = Just doctorFilter })
        bounds ← H.liftEff $ GMaps.getBounds (GMaps.mapId DoctorsId)
        state ← H.get
        loadDoctors state DoctorsId bounds
        pure next
      DoctorClick doctor reply → do
        H.raise (DoctorSelected doctor)
        pure (reply H.Listening)
      HandleInput authState next → do
        H.modify (_ { as = authState })
        pure next

--------------------- View Pieces -------------------------
tabHeader state =
  HH.div_
  [ tabLink state.selectedTab "Cities"  state.cs
  , tabLink state.selectedTab "Doctors" state.ds
  , HH.div [ HP.class_ (HH.ClassName "tabLink pull-right") ]
    [ HH.span [ HP.class_ (HH.ClassName "") ]
      [ loadButton state.loading ] ]
  ]

loadButton loading =
  if loading --|| not loading
  then HH.img [ HP.src "img/loading.gif"
              , HP.id_  "tabLoadImage" ]
  else HH.span_ []

tabLink selectedTab title tab =
  HH.div [ tabClass $ selectedTab == tab.id
         , HE.onClick $ HE.input_ $ TabSelect tab.id ]
  [ HH.text title ]

renderTab ∷ ∀ a p i. TabId → Tab a → HH.HTML p i
renderTab selectedTab tab =
  HH.div [ HP.id_ $ show tab.id
         , mapViewVisible (selectedTab == tab.id) ]
  [ ]

tabClass selected =
  if selected
  then HP.class_ (HH.ClassName "activeTabLink tabLink")
  else HP.class_ (HH.ClassName "inactiveTabLink tabLink")

mapViewVisible selected =
  if selected
  then HP.class_ (HH.ClassName "mapView mapViewActive")
  else HP.class_ (HH.ClassName "mapView mapViewInactive")

blankTab ∷ ∀ a. TabId → GMaps.Coords → Tab a
blankTab tabId center = { center: center
                        , id: tabId, elts: [] }

---------------------- Eval Pieces ---------------------
selectTab ∷ ∀ eff. TabId → H.ComponentDSL State Query Message (MyMonad eff) Unit
selectTab tabId = do
  H.modify (_ { selectedTab = tabId })
  s ← H.get
  H.liftEff $ GMaps.refreshMaps [ GMaps.mapId s.cs.id, GMaps.mapId s.ds.id ]

loadCities s tabId bounds = do
  H.modify (_ { loading = true })
  H.liftEff $ GMaps.clearMarkers (GMaps.mapId tabId)
  cities ← H.liftAff $ getCities s.as.apiUrl s.as.token bounds
  let cs' = either (const []) id cities
  addMarkersCities (GMaps.mapId s.cs.id) cs'
  H.modify (_ { loading = false })

loadDoctors s tabId bounds = do
  H.modify (_ { loading = true })
  H.liftEff $ GMaps.clearMarkers (GMaps.mapId tabId)
  doctors ← H.liftAff $ getDoctors s.as.apiUrl s.as.token bounds s.filter
  let ds' = either (const []) id doctors
  H.liftEff $ logShow $ Arr.length ds'
  --H.liftEff $ logShow $ ds'
  addMarkersDoctors (GMaps.mapId s.ds.id) ds'
  H.modify (_ { loading = false })

loadDoctorMap ∷ ∀ eff. City → H.ComponentDSL State Query Message (MyMonad eff) Unit
loadDoctorMap (City city) = do
  selectTab DoctorsId
  doctorMapId ← H.gets (\s → GMaps.mapId s.ds.id)
  H.liftEff $ GMaps.shiftMap doctorMapId {lat: city.lat, lng: city.lng} 11

loadCityMap ∷ ∀ eff. City → H.ComponentDSL State Query Message (MyMonad eff) Unit
loadCityMap (City city) = do
  selectTab CitiesId
  cityMapId ← H.gets (\s → GMaps.mapId s.cs.id)
  H.liftEff $ GMaps.shiftMap cityMapId {lat: city.lat, lng: city.lng} 11

--discard all bounds that are invalid or shouldn't be loaded
filterBounds ∷ ∀ a. TabId → GMaps.Bounds → (H.SubscribeStatus → a) → Query a
filterBounds tabId (GMaps.Bounds b) =
  if (b.sw_lat - b.ne_lat == 0.0) || (b.sw_lng - b.ne_lng == 0.0)
  then NoOp
  else BoundsChanged tabId (GMaps.Bounds b)

subscribeMapMoved tabId =
 H.subscribe $ ES.eventSource' (GMaps.mapMoved $ GMaps.mapId tabId)
 (Just ∘ H.request ∘ (filterBounds tabId) ∘ GMaps.Bounds)

--change this into two functions
addMarkersCities ∷ ∀ eff. GMaps.MapId → Array City
                 → H.ComponentDSL State Query Message (MyMonad eff) Unit
addMarkersCities mapId cities = do
  traverse_ (\city → H.subscribe $ ES.eventSource' (GMaps.addMarker mapId city)
                      (Just ∘ H.request ∘ (const (LoadDoctorMap city)))) cities

addMarkersDoctors ∷ ∀ eff. GMaps.MapId → Array Doctor
                  → H.ComponentDSL State Query Message (MyMonad eff) Unit
addMarkersDoctors mapId doctors = do
  traverse_ (\doc → H.subscribe $ ES.eventSource' (GMaps.addMarker mapId doc)
                      (Just ∘ H.request ∘ (const (DoctorClick doc)))) doctors

initMaps s next = do
  let csId = GMaps.mapId s.cs.id
      dsId = GMaps.mapId s.ds.id
  _ ← H.liftAff $ GMaps.initMap csId s.cs.center
  _ ← H.liftAff $ GMaps.initMap dsId s.ds.center
  subscribeMapMoved s.cs.id
  subscribeMapMoved s.ds.id
  b1 ← H.liftEff $ GMaps.getBounds csId
  cities ← H.liftAff $ getCities s.as.apiUrl s.as.token b1
  H.modify (_ { loading = true })
  H.liftEff $ GMaps.clearMarkers csId
  addMarkersCities csId (either (const []) id cities)
  H.modify (_ { loading = false })
  pure next

---------------------- Ajax ----------------------------
boundsToArray ∷ GMaps.Bounds → Array Number
boundsToArray (GMaps.Bounds b) =
  [ b.sw_lat, b.sw_lng, b.ne_lat, b.ne_lng ]

getCities ∷ ∀ aff. ApiUrl → IdToken → GMaps.Bounds
                → Aff ( ajax ∷ AJAX | aff ) (Either ErrorMsg (Array City))
getCities apiUrl token bounds = do
  let query = CityBoundsQ $ { bounds: (boundsToArray bounds) }
  eitherRes ← apiPostRequest (apiUrl <> "city/bybounds") token $ encodeJson query
  pure $ (fromMaybe []) <$> eitherRes

getDoctors ∷ ∀ aff. ApiUrl → IdToken → GMaps.Bounds → Maybe DoctorFilter
                → Aff ( ajax ∷ AJAX | aff ) (Either ErrorMsg (Array Doctor))
getDoctors apiUrl token bounds filter = do
  let df = fromMaybe { limit: 100, includePayers: [], excludePayers: []
                     , payRange: [0, 1999888777] } filter
      query = DoctorBoundsQ { bounds: (boundsToArray bounds)
                            , limit: df.limit
                            , includePayers: map payerId df.includePayers
                            , excludePayers: map payerId df.excludePayers
                            , payRange: df.payRange }
  eitherRes ← apiPostRequest (apiUrl <> "doctors/bybounds") token $ encodeJson query
  pure $ (fromMaybe []) <$> eitherRes
    where payerId (Payer p) = p.id

-------------------- Instances & Typeclasses ------------------
instance encodeMapIdTabId ∷ GMaps.EncodeMapId TabId where
  mapId = show
