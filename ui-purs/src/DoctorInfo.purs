module DoctorInfo where

import Tight.Prelude
import Types (Doctor(..), MyMonad, DoctorDetailQ(..), DoctorDetail(..), AuthState(..))
import Network.Auth.AuthZero (apiPostRequest, IdToken, ApiUrl, ErrorMsg(..))
import Data.Argonaut (class EncodeJson, encodeJson)
import Network.HTTP.Affjax (AJAX)
import Control.Monad.Aff (Aff)
import Control.Monad.Eff.Console (CONSOLE, logShow)

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP

data Query a = HandleInput Input a

type State = { loading ∷ Boolean
             , doctor  ∷ Maybe Doctor
             , doctorDetail ∷ Maybe DoctorDetail
             , as      ∷ AuthState }
type Input = { doctor ∷ Maybe Doctor
             , as     ∷ AuthState }

component ∷ ∀ eff. H.Component HH.HTML Query Input Void (MyMonad eff)
component =
  H.component
  { initialState: initialState
  , render
  , eval
  , receiver: HE.input HandleInput
  }

  where
    initialState input = { loading: false, doctor: input.doctor, as: input.as
                         , doctorDetail: Nothing }

    render ∷ State → H.ComponentHTML Query
    render state =
      HH.div_ [ HH.h4_ [ HH.text "Doctor Detail" ]
              , HH.div_ [ if state.loading
                          then HH.text "Loading..."
                          else
                            case state.doctorDetail of
                              Nothing → HH.text "No doctor selected."
                              Just dd → renderDoctorDetail dd
                        ]
              ]

    eval ∷ Query ~> H.ComponentDSL State Query Void (MyMonad eff)
    eval = case _ of
      HandleInput input next -> do
        case input.doctor of
          Nothing → H.modify (_ { as = input.as, doctor = input.doctor })
          (Just d) → loadDoctor input.as d
        pure next

renderDoctorDetail ∷ DoctorDetail → H.ComponentHTML Query
renderDoctorDetail (DoctorDetail dd) =
  HH.div_ [ HH.text $ show dd.doctor
          , HH.ul_ $ (\pa -> HH.li_ [ HH.text $ show pa ]) <$> dd.payerAmounts
          ]

loadDoctor authState d = do
  H.modify (_ { loading = true })
  doctorDetail ← H.liftAff $ getDoctorDetail authState d
  let dd' = either (const Nothing) id doctorDetail
  H.modify (_ { loading = false, doctorDetail = dd' })

getDoctorDetail ∷ ∀ aff. AuthState → Doctor
                → Aff ( ajax ∷ AJAX | aff ) (Either ErrorMsg (Maybe DoctorDetail))
getDoctorDetail as (Doctor d) = do
  let query = DoctorDetailQ { id: d.dId }
  apiPostRequest (as.apiUrl <> "doctors/detail") as.token $ encodeJson query
