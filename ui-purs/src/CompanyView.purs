module CompanyView where

import Tight.Prelude
import Types (Payer(..), MyMonad, PayerSearchQ(..), AuthState(..), DoctorFilter, DoctorPayer(..), Doctor(..), DoctorsPayerQ(..), getAmount)
import Config (sliderStart, sliderEnd, sliderMax, maxNum)
import Control.Monad.Eff.Console (log, logShow)
import Control.Monad.Aff (Aff)
import Network.HTTP.Affjax (AJAX)
import Network.Auth.AuthZero (apiPostRequest, IdToken, ApiUrl, ErrorMsg(..))
import Data.Argonaut (class EncodeJson, encodeJson)
import Data.Filter.Slider (Range, ElementId, SLIDER, initSlider, getRange)
import Data.Array (delete, head, last, length)
import Data.Int (fromString, toNumber)

import Halogen.HTML.Autocomplete as AC

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP

doctorResultLimit = 50
doctorResultLimitMax = 500

type State = { as            ∷ AuthState
             , loading       ∷ Boolean
             , payers        ∷ Array Payer
             , company       ∷ Maybe Payer
             , query         ∷ String
             , doctorPayers  ∷ Array DoctorPayer
             , limit         ∷ Int
             , lastLimit     ∷ Int
             , sliderId      ∷ ElementId }

type Input = AuthState
--type Message = Void

data SOQuery a
  = SOHandleInput SOInput a
  | SOSelectCompany Payer a
  | SOUpdateQuery String a
type SOState = { as      ∷ AuthState
               , payers  ∷ Array Payer
               , query   ∷ String }
type SOInput = AuthState
data SOMessage = SOCompanySelected Payer

data Query a
  = HandleInput Input a
  | ApplyFilter (Maybe Payer) a
  | UpdateLimit Int a
  | SelectCompany Payer a
  | UpdateCompanyQuery String a
  | Init a

searchOnlyComponent ∷ ∀ eff. H.Component HH.HTML SOQuery SOInput SOMessage (MyMonad eff)
searchOnlyComponent =
  H.component
  { initialState: initialState
  , render
  , eval
  , receiver: HE.input SOHandleInput
  }

  where
    initialState input = { payers: [], query: "", as: input }

    render ∷ SOState → H.ComponentHTML SOQuery
    render state =
      let acConfig = { containerClass: (HH.ClassName "autocomplete-container")
                     , prefix: "autocomplete-suggestion"
                     , placeholder: "Search for a company..."
                     , selectQuery: SOSelectCompany
                     , valueInputQuery: SOUpdateQuery }
      in AC.widget acConfig state.query state.payers

    eval :: SOQuery ~> H.ComponentDSL SOState SOQuery SOMessage (MyMonad eff)
    eval = case _ of
      SOUpdateQuery q next -> do
        H.modify (_ { query = q })
        if q == ""
          then do
            H.modify (_ { payers = [] })
            pure next
          else do
            s <- H.get
            r <- H.liftAff $ getPayers s.as.apiUrl s.as.token
              (PayerSearchQ {payerName: q})
            handleError r
            pure next
      SOSelectCompany payer next -> do
        H.modify (_ { query = "", payers = [] } )
        H.raise (SOCompanySelected payer)
        pure next
      SOHandleInput authState next -> do
        H.modify (_ { as = authState })
        pure next

component ∷ ∀ eff. H.Component HH.HTML Query Input Void (MyMonad eff)
component =
  H.lifecycleComponent
  { initialState: initialState
  , render
  , eval
  , initializer: Just (H.action Init)
  , finalizer: Nothing
  , receiver: HE.input HandleInput
  }

  where
    initialState input = { as: input
                         , loading: false
                         , payers: []
                         , doctorPayers: []
                         , company: Nothing
                         , query: ""
                         , limit: doctorResultLimit
                         , lastLimit: doctorResultLimit
                         , sliderId: "CompanyViewSlider"}

    render :: State → H.ComponentHTML Query
    render state =
      HH.div_
      [
        HH.div_
        [
          HH.h5_ [ HH.text "Search for Company" ]
        , moneyFilter state.sliderId
        , companySearch state
        , companyDisplay state.company
        , limitFilter state.limit
        , applyButton state.company "Update Results"
        , doctorView state
        ]
      ]

    eval :: Query ~> H.ComponentDSL State Query Void (MyMonad eff)
    eval = case _ of
      Init next → do
        sId ← H.gets _.sliderId
        H.liftEff $ initSlider sId [0, sliderMax] [sliderStart, sliderEnd] sliderMax
        pure next
      UpdateLimit limit next → do
        if (limit <= doctorResultLimitMax)
          then H.modify (_ { limit = limit })
          else H.modify (_ { limit = doctorResultLimit })
        pure next
      UpdateCompanyQuery q next → do
        H.modify (_ { query = q })
        if q == ""
          then do
            H.modify (_ { payers = [] })
            pure next
          else do
            s ← H.get
            r ← H.liftAff $ getPayers s.as.apiUrl s.as.token
              (PayerSearchQ {payerName: q})
            handleError r
            pure next
      SelectCompany payer next → do
        loadDoctors payer
        pure next
      ApplyFilter maybePayer next → do
        case maybePayer of
          Nothing    → pure unit
          Just payer → do limit ← H.gets _.limit
                          H.modify (_ { lastLimit = limit })
                          loadDoctors payer
        pure next
      HandleInput authState next → do
        H.modify (_ { as = authState })
        pure next

--------------------- View Pieces -------------------------
companySearch ∷ State → H.ComponentHTML Query
companySearch state =
  let cfg = { containerClass: (HH.ClassName "autocomplete-container")
            , prefix: "autocomplete-suggestion"
            , placeholder: "Pharma Company"
            , selectQuery: SelectCompany
            , valueInputQuery: UpdateCompanyQuery }
  in AC.widget cfg state.query state.payers

companyDisplay ∷ Maybe Payer → H.ComponentHTML Query
companyDisplay mPayer = HH.div_ [ HH.text $ fromMaybe "No company selected." (show <$> mPayer) ]

moneyFilter eId = HH.div [ HP.id_ eId
                         , HP.class_ (HH.ClassName "slider") ] []

--set the last number in the range to the biggest number if it's the range's max
--i.e. $1,000,000 will be $1,000,000+
applyMax ∷ Range → Int → Int → Range
applyMax range rangeMax biggestNumber =
  let first  = fromMaybe 0 (head range)
      second = fromMaybe 0 $ f <$> (last range)
  in [first, second]
  where f x = if x == rangeMax then biggestNumber else x

limitFilter ∷ Int → H.ComponentHTML Query
limitFilter limit =
  HH.input [ HP.type_ HP.InputNumber
           , HP.value $ show limit
           , HP.placeholder "Max doctors"
           , HE.onValueInput
             (HE.input (\val → UpdateLimit $ num val))
           , HP.min $ toNumber 0
           , HP.max $ toNumber doctorResultLimitMax ]
  where num = (fromMaybe 0) ∘ fromString

applyButton company label =
  HH.button
  [ HP.title label
  , HE.onClick (HE.input_ $ ApplyFilter company )
  , HP.classes [ HH.ClassName "btn"
               , HH.ClassName "btn-primary"
               , HH.ClassName "tight-btn"
               ]
  ]
  [ HH.text label ]

doctorView state =
  HH.div_
  [
    HH.h5_ [ HH.text $ doctorCount (length state.doctorPayers) state.lastLimit ]
  , if state.loading
    then HH.text "Loading doctors..."
    else HH.ul_ $ formatDP <$> state.doctorPayers
  ]
  where formatDP (DoctorPayer dp) = HH.li_ [ HH.text $ (show dp.doctor) <> ", " <>
                                           (show $ getAmount dp.payerAmount) ]
        doctorCount numDocs limit
          | numDocs == limit = show limit <> "+ doctors found."
          | numDocs == 1     = "1 doctor found."
          | otherwise        = show numDocs <> " doctors found."


loadDoctors (Payer payer) = do
  s ← H.get
  range ← H.liftEff $ getRange
  H.modify (_ { query = "", payers = [], company = Just (Payer payer)
              , loading = true })
  r ← H.liftAff $ getDoctorsPayer s.as.apiUrl s.as.token
    (DoctorsPayerQ { payerId: payer.id, limit:   s.limit
                   , payRange: applyMax range sliderMax maxNum})
  handleDPError r
  H.modify (_ { loading = false })

handleError (Left error)   = H.liftEff $ log $ show error
handleError (Right payers) = H.modify (_ { payers = payers })

handleDPError (Left error) = H.liftEff $ log $ show error
handleDPError (Right doctorPayers) = H.modify (_ { doctorPayers = doctorPayers })

getPayers ∷ ∀ aff. ApiUrl -> IdToken → PayerSearchQ
          → Aff ( ajax :: AJAX | aff ) (Either ErrorMsg (Array Payer))
getPayers apiUrl token query = do
  eitherRes <- apiPostRequest (apiUrl <> "payers/search") token $ encodeJson query
  pure $ (fromMaybe []) <$> eitherRes

getDoctorsPayer ∷ ∀ aff. ApiUrl -> IdToken → DoctorsPayerQ
                → Aff ( ajax :: AJAX | aff ) (Either ErrorMsg (Array DoctorPayer))
getDoctorsPayer apiUrl token query = do
  eitherRes <- apiPostRequest (apiUrl <> "doctors/bypayer") token $ encodeJson query
  pure $ (fromMaybe []) <$> eitherRes
