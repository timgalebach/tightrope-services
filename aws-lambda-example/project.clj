(defproject aws-lambda-example "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0-alpha14"]
                 [com.amazonaws/aws-lambda-java-core "1.0.0"]
                 [org.clojure/data.json "0.2.6"]]
  :java-source-paths ["src/java"]
  :aot :all)
