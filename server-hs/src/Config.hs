module Config where

import Data.Pool
import Database.PostgreSQL.Simple

myPool :: IO (Pool Connection)
myPool =
  let connectionInfo = defaultConnectInfo {
          connectHost = "127.0.0.1"
        , connectPort = 5432
        , connectUser = "postgres"
        , connectPassword = ""
        , connectDatabase = "postgres" }
  in createPool (connect connectionInfo) close 1 10 10
