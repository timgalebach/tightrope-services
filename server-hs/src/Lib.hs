{-# LANGUAGE OverloadedStrings #-}
module Lib where

import Api
import Config  (myPool)
import Web.Scotty


runServer = do
  pool <- myPool
  scotty 3000 $ routes pool
