{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

module Types where

import Data.Pool  (Pool)
import Database.PostgreSQL.Simple (Connection)
import Database.PostgreSQL.Simple.FromRow
import Data.Aeson


----------------------- API/JSON Types -------------------------
newtype GetDoctor        = GDQ { runGDQ :: (Connection -> IO (Maybe Doctor))}
newtype GetDoctorDetail  = GDD { runGDD :: (Connection -> IO (Maybe DoctorPayers))}
newtype DoctorByName     = DBN { runDBN :: (Connection -> IO [Doctor])}
newtype DoctorsByBounds  = DBB { runDBB :: (Connection -> IO [DoctorMoney])}
newtype DoctorsByPayer   = DBP { runDBP :: (Connection -> IO [DoctorPayer])}
newtype PayerSearchQuery = PSQ { runPSQ :: (Connection -> IO [Payer])}
newtype CityByName       = CBN { runCBN :: (Connection -> IO (Maybe CityNumDocs))}
newtype CitySearchQuery  = CSQ { runCSQ :: (Connection -> IO [CityNumDocs])}
newtype CitiesByBounds   = CBB { runCBB :: (Connection -> IO [CityNumDocs])}

data PayerMoney = PayerMoney Payer Money

data FetchDoctorsBounds = FetchDoctorsBounds
  { fdbBounds :: Bounds
  , fdbPayRange :: PayRange
  , fdbExclude :: [PayerId]
  , fdbInclude :: [PayerId]
  , fdbLimit :: Limit
  } deriving (Show)

----------------------- DB Types -------------------------
newtype Limit = L Int deriving (Show)
data InOrNot = InStr | NotInStr
newtype FieldName = F String
newtype InClause  = InClause String

data DoctorPayer =  DP  Doctor Payer Money deriving (Show)
data DoctorPayers = DPS Doctor [(Payer, Money)] deriving (Show)
data CityNumDocs  = CND Int City deriving (Show)

type PayerId =  Int
newtype PayRange = PR (Int, Int) deriving (Show)
newtype Money = Money Rational deriving (Show)
data DoctorMoney  = DM Doctor Money deriving (Show)

data Bounds = Bounds { swLat :: Double, swLng :: Double
                     , neLat :: Double, neLng :: Double } deriving (Show)

data Doctor = Doctor
  { tgDocId :: Int
  , tgDocFirst :: Maybe String
  , tgDocMiddle :: Maybe String
  , tgDocLast :: Maybe String
  , tgDocLine1 :: Maybe String
  , tgDocLine2 :: Maybe String
  , tgDocState :: Maybe String
  , tgDocCity :: Maybe String
  , tgDocZip :: Maybe String
  , tgDocLat :: Maybe Double
  , tgDocLng :: Maybe Double
  } deriving (Show)

data Payer = Payer
  {
    tgPayerId   :: Int
  , tgPayerName :: Maybe String } deriving (Show)

data City = City
  {
    tgCCity   :: Maybe String
  , tgCState  :: Maybe String
  , tgCLat    :: Maybe Double
  , tgCLng    :: Maybe Double } deriving (Show)

instance FromRow Doctor where
  fromRow = Doctor
    <$> field <*> field <*> field <*> field <*> field <*> field
    <*> field <*> field <*> field <*> field <*> field

instance FromRow DoctorPayer where
  fromRow = combo <$> field <*> field <*> field <*> field <*> field <*> field
    <*> field <*> field <*> field <*> field <*> field
    <*> field <*> field <*> field
    where combo a b c d e f g h i j k l m n =
            DP (Doctor a b c d e f g h i j k) (Payer l m) (Money n)

instance FromRow City where
  fromRow = City <$> field <*> field <*> field <*> field

instance FromRow Payer where
  fromRow = Payer <$> field <*> field

instance FromRow CityNumDocs where
  fromRow = combo <$> field <*> field <*> field <*> field <*> field
    where combo a b c d e = CND a (City b c d e)

instance FromRow DoctorMoney where
  fromRow = combo <$> field <*> field <*> field <*> field <*> field
    <*> field <*> field <*> field <*> field <*> field <*> field <*> field
    where combo a b c d e f g h i j k l =
            DM (Doctor a b c d e f g h i j k) (Money l)
