{-# LANGUAGE OverloadedStrings #-}
module Api where

import Db
import Config (myPool)
import Types
import Web.Scotty             as S
import Web.Scotty.Internal.Types
import Data.Pool
import Data.Monoid
import Data.HashMap.Lazy
import Data.Ratio
import Data.Maybe
import Data.Text.Lazy.Encoding (decodeUtf8)
import Database.PostgreSQL.Simple
import Data.Text.Lazy         (pack, unpack)
import Control.Monad.IO.Class (liftIO)
import Control.Monad          (join)
import Control.Monad.Except (throwError, MonadIO)
import Data.Aeson

----------------------- Instances ------------------------
instance FromJSON Bounds where
  parseJSON jsn = do
    [w, x, y, z] <- parseJSON jsn
    return $ Bounds w x y z

instance FromJSON PayRange where
  parseJSON j = do
    [pMin, pMax] <- parseJSON j
    return $ PR (pMin, pMax)

instance FromJSON Limit where
  parseJSON j = do
    pure . L =<< parseJSON j

instance FromJSON GetDoctor where
  parseJSON (Object v) = GDQ . getDoctor <$> v .: "id"

instance FromJSON GetDoctorDetail where
  parseJSON (Object v) = GDD . doctorDetail <$> v .: "id"

instance FromJSON DoctorByName where
  parseJSON (Object v) = DBN . doctorSearch <$> v .: "doctorName"

instance FromJSON DoctorsByPayer where
  parseJSON (Object v) = do
    payRange <- v .: "payRange"
    limit    <- v .: "limit"
    payerId  <- v .: "payerId"
    DBP <$> (doctorsByPayer <$> parseJSON payRange <*> parseJSON limit <*> parseJSON payerId)

instance FromJSON PayerSearchQuery where
  parseJSON (Object v) = PSQ . payerSearch <$> v .: "payerName"

instance FromJSON CityByName where
  parseJSON (Object v) = CBN . cityByName <$> v .: "cityName"

instance FromJSON CitySearchQuery where
  parseJSON (Object v) = CSQ . citySearch <$> v .: "cityName"

instance FromJSON CitiesByBounds where
  parseJSON (Object v) = CBB . getCities <$> v .: "bounds"

instance FromJSON DoctorsByBounds where
  parseJSON (Object v) = do
    bounds   <- v .: "bounds"
    payRange <- v .: "payRange"
    limit    <- v .: "limit"
    exclude  <- v .: "excludePayers"
    include  <- v .: "includePayers"
    DBB <$>
      (doctorsByBoundsPayers <$> parseJSON bounds <*> parseJSON payRange
       <*> parseJSON exclude <*> parseJSON include <*> parseJSON limit)

instance ToJSON Bounds where
  toJSON (Bounds swlat swlng nelat nelng) =
    toJSON [ swlat, swlng, nelat, nelng ]

instance ToJSON CityNumDocs where
  toJSON (CND numDocs (City city state lat lng)) =
    object [ "num_docs" .= numDocs
           , "city"     .= city
           , "state"    .= state
           , "lat"      .= lat
           , "lng"      .= lng ]

instance ToJSON Doctor where
  toJSON (Doctor id f m l l1 l2 s c z lat lng) =
    object [ "id"     .= id
           , "first"  .= fm f
           , "middle" .= fm m
           , "last"   .= fm l
           , "line1"  .= fm l1
           , "line2"  .= fm l2
           , "state"  .= fm s
           , "city"   .= fm c
           , "zip"    .= fm z
           , "lat"    .= fromMaybe 0.0 lat
           , "lng"    .= fromMaybe 0.0 lng ]
    where fm = fromMaybe ""

instance ToJSON DoctorMoney where
  toJSON (DM doctor money) =
    let (Object doctorO) = toJSON doctor
    in Object $ doctorO <> singleton "amount" (toJSON money)

instance ToJSON Money where
  toJSON (Money r) = toJSON $ (fromRational r :: Double)

instance ToJSON Payer where
  toJSON (Payer id name) = object [ "payerId"   .= id, "payerName" .= name]

instance ToJSON PayerMoney where
  toJSON (PayerMoney p m) =
    let (Object payerO) = toJSON p
    in Object $ payerO <> singleton "amount" (toJSON m)

instance ToJSON DoctorPayer where
  toJSON (DP d p m) =
    let (Object doctorO) = toJSON d
        (Object payerO)  = toJSON p
    in Object $ doctorO <> payerO <> singleton "amount" (toJSON m)

instance ToJSON DoctorPayers where
  toJSON (DPS doc payerList) =
    let (Object doctorO) = toJSON doc
        payerMoneyList = Prelude.map (\(p, m) -> PayerMoney p m) payerList
    in Object $ doctorO <> singleton "payers" (toJSON payerMoneyList)
-------------------------- API Functions ----------------------------

decodeBody :: (FromJSON a) => ActionM a
decodeBody = do
  raw <- body
  case decode raw of
    Nothing          -> throwError $ stringError $ unpack . decodeUtf8 $ raw
    Just parsedJson  -> pure parsedJson

poolMonad :: (MonadIO m) => (Pool Connection) -> (Connection -> IO a) -> m a
poolMonad pool queryFn = liftIO $ withResource pool queryFn

runDb withPool runQuery = do
  query <- decodeBody
  S.json =<< withPool (runQuery query)

----------------------  Routes  --------------------------------
routes :: (Pool Connection) -> ScottyM ()
routes pool = do
  let pm = poolMonad pool

  get  "/loginping"        $ do S.text "success"
  post "/doctors/detail"   $ runDb pm runGDD
  post "/doctors/byid"     $ runDb pm runGDQ
  post "/doctors/bypayer"  $ runDb pm runDBP
  post "/doctors/search"   $ runDb pm runDBN
  post "/doctors/bybounds" $ runDb pm runDBB
  post "/payers/search"    $ runDb pm runPSQ
  post "/city/byname"      $ runDb pm runCBN
  post "/city/search"      $ runDb pm runCSQ
  post "/city/bybounds"    $ runDb pm runCBB
