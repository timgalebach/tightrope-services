{-# LANGUAGE OverloadedStrings #-}
module Db where

import Types
import Config (myPool)
import Database.PostgreSQL.Simple
import Data.Pool
import qualified Control.Exception.Safe as E
import Data.String (fromString)
import Data.List   (intercalate, foldl')
import Data.Ratio
import Safe (headMay)

instance Show InClause where
  show (InClause icStr) = icStr

payerInClause :: FieldName -> InOrNot -> [PayerId] -> InClause
payerInClause _ _ []      = InClause ""
payerInClause (F fn) i ps =
  InClause $ " AND " ++ fn ++ inOrNot ++ " (" ++(intercalate "," $ map show ps)++ ") "
  where inOrNot = case i of
          InStr    -> " IN "
          NotInStr -> " NOT IN "

insertInClauses :: String -> String -> [InClause] -> Query
insertInClauses front back ics =
  fromString $ front ++ " " ++  (intercalate " " (map show ics)) ++ " " ++ back

getDoctor :: Int -> Connection -> IO (Maybe Doctor)
getDoctor id conn = do
  doctor <- query conn "SELECT * FROM doctors WHERE id = ?" [id]
  return $ headMay doctor

getCities :: Bounds -> Connection -> IO [CityNumDocs]
getCities (Bounds swLat swLng neLat neLng) conn = do
  cities <- query conn "SELECT num_docs, city, state, lat, lng FROM city_num_docs WHERE lat > ? AND lat < ? AND lng > ? AND lng < ?" (swLat, neLat, swLng, neLng)
  return cities

doctorSearch :: String -> Connection -> IO [Doctor]
doctorSearch doctorName conn = do
  query conn "SELECT * FROM doctors WHERE CONCAT(last, ', ', first) ILIKE ? LIMIT 10" [ doctorName ++ "%" ]

citySearch :: String -> Connection -> IO [CityNumDocs]
citySearch cityName conn = do
  query conn "SELECT num_docs, city, state, lat, lng FROM city_num_docs WHERE CONCAT(city, ', ', state) ILIKE ? LIMIT 10" [cityName ++ "%"]

cityByName :: String -> Connection -> IO (Maybe CityNumDocs)
cityByName cityName conn = do
  cities <-   query conn "SELECT num_docs, city, state, lat, lng FROM city_num_docs WHERE CONCAT(city, ', ', state) ILIKE ? LIMIT 10" [cityName]
  return . headMay $ cities

payerSearch :: String -> Connection -> IO [Payer]
payerSearch payerName conn = do
  query conn "SELECT id, name FROM payers WHERE name ILIKE ? LIMIT 10"
    [payerName ++ "%"]

doctorPayerList :: Int -> Connection -> IO [DoctorPayer]
doctorPayerList doctorId conn = do
  dps <- query conn "SELECT doctors.*, payer AS payerid, payers.name AS payername, sum AS amount FROM doctors, payers, doc_payer WHERE doctors.id = ? AND doctors.id = docId AND payer = payers.id ORDER BY sum DESC" [doctorId]
  return dps

doctorsByPayer :: PayRange -> Int -> Int -> Connection -> IO [DoctorPayer]
doctorsByPayer (PR (pmin, pmax)) limit payerId conn = do
  dps <- query conn "SELECT doctors.*, payer AS payerid, payers.name AS payername, sum AS amount FROM doctors, payers, doc_payer WHERE payers.id = ? AND doctors.id = docId AND payer = payers.id AND sum > ? AND SUM < ? ORDER BY sum DESC LIMIT ? " [payerId, pmin, pmax, limit]
  return dps

doctorPayers :: Int -> Connection -> IO (Maybe DoctorPayers)
doctorPayers id conn = do
  res <- doctorPayerList id conn
  case res of
    [] -> return Nothing

    ((DP d payer money):dps) -> return $
      Just $ foldl' f (DPS d [(payer, money)]) dps
      where f (DPS doc payerMoneyList) (DP _ payer2 money2) =
              DPS doc $ payerMoneyList ++ [(payer2, money2)]

doctorDetail :: Int -> Connection -> IO (Maybe DoctorPayers)
doctorDetail id conn = do
  dps <- doctorPayers id conn
  if null dps
    then do
      doctor <- (getDoctor id conn)
      return $ DPS <$> doctor <*> (Just [])
    else
      return dps


doctorsByBoundsPayers :: Bounds -> PayRange -> [PayerId] -> [PayerId] -> Limit ->
                         Connection -> IO [DoctorMoney]
doctorsByBoundsPayers (Bounds swLat swLng neLat neLng) (PR (pmin, pmax)) excludePayers includePayers (L limit) conn = do
  let q = insertInClauses "SELECT id, first, middle, last, line1, line2, city, state, zip, lat, lng, SUM(sum) AS amount FROM doctors, doc_payer WHERE lat > ? AND lat < ? AND lng > ? AND lng < ? AND id = docid"
        "GROUP BY doctors.id, first, middle, last, line1, line2, city, state, zip, lat, lng HAVING SUM(sum) > ? AND SUM(sum) < ? ORDER BY amount DESC LIMIT ?"
        [ (payerInClause (F "doc_payer.payer") NotInStr excludePayers)
        , (payerInClause (F "doc_payer.payer") InStr    includePayers)]

  dms <- query conn q (swLat, neLat, swLng, neLng, pmin, pmax, limit)
  return dms


test = do
  p <- myPool
  --cs <- withResource p (getCities (Bounds 43 (-85) 45 (-83)))
--  cities <- withResource p (citySearch "ANDO")
--  payers <- withResource p (payerSearch "Pfi")
--  city   <- withResource p (cityByName "ANDOVER, MA")
--  dms <- withResource p (doctorsByBoundsPayers (Bounds 43 (-85) 45 (-83)) (PR (50000, 500000)) [] [100000000232] (L 10))
  --dps <- withResource p (doctorPayers 10)
--  dPayers <- withResource p (doctorDetail 5000)
--  dNoPayers <- withResource p (doctorDetail 1197613 )
  docs <- withResource p $ doctorSearch "RAMIREZ, M"
  --dps <- withResource p $ doctorsByPayer (PR (0, 500)) 10 100000000144
  putStrLn $ show $ docs
--  putStrLn $ show $ dNoPayers
