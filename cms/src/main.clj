(ns main
  (:gen-class)
  (:require [clojure.spec :as s]
            [clojure.java.io :as io]
            [clj-time.coerce :as c]
            [clojure.data.json :as json]
            [clj-time.core :as t]
            [amazonica.aws.s3 :as s3]
            [clojure.edn :refer [read-string]]
            [clojure.string :refer [split replace]])
  (:refer-clojure :exclude [read-string replace]))

(defn FIELDS ["3" "4" "5" "6" "7" "8" "10" "11" "12" "13" "15" "16" "17" "18" "26" "27" "30" "31" "32" "45" "63"])

(defn conform! [spec x & debug-vals]
  (let [c-val (s/conform spec x)]
    (if (= c-val :clojure.spec/invalid)
      (do (s/explain spec x)
          (pr debug-vals)
          (throw (AssertionError. "failed spec")))
      c-val)))

(s/def ::cms-row (s/and (s/* string?) #(= (count %) 65)))

(defn parse-cms-row-string [cms-string error-file]
  (let [res (->> (split cms-string #"\"")
                 (mapcat #(if (every? #{\,} %)
                            (repeat (dec (count %)) "")
                            [%])))
        res (concat (when (= (first cms-string) \,) [""])
                    res
                    (when (= (last cms-string) \,) [""]))]
    (when (not (s/valid? ::cms-row res cms-string))
      (println "bad row")
      (spit error-file (str cms-string "\n") :append true))
    res))

(defn format-line [cms-string version error-file]
  (->> (parse-cms-row-string cms-string error-file)
       (zipmap (map str (range 0 1000)))
       (merge {"v" version})
       json/write-str))

(defn csv-to-file [version year csv-file out-file error-file
                   num-records-file start-position]
  "start-position is 0-indexed, doesn't include first line"
  {:pre [(s/valid? int? num-records-file)
         (s/valid? int? start-position)]}
  (with-open [rdr (io/reader csv-file)]
    (println (str "Starting at record " start-position))
    (loop [n num-records-file
           lines-left (drop start-position (rest (line-seq rdr)))
           total (+ num-records-file start-position)]
      (when (not-empty lines-left)
        (let [ts (c/to-long (t/now))]
          (with-open [w (io/writer out-file)]
            (doseq [l (take n lines-left)]
              (.write w (str (format-line l version error-file) "\n"))))
          (println "Starting S3 upload...")
          (s3/put-object :bucket-name "tightropecp"
                         :key (str "data/cms/" year "/" ts "-" total)
                         :file out-file)
          (println "Processed " total " records.")
          (recur n (drop n lines-left) (+ total n)))))))

(defn file)

(defn print-over-65 [cms-row]
  {:pre [(s/valid? ::cms-row cms-row)]}
  (when (not= (count cms-row) 65) (println (count cms-row))))

(defn -main [& args]
  (if (not= (count args) 7)
    (println "cms VERSION YEAR CSV_FILE OUT_FILE ERROR_FILE NUM_RECORDS_PER_FILE START_POSITION(ZERO INDEXED, NOT INCLUDING FIRST)")
    (apply csv-to-file (concat (take 5 args) (map read-string (drop 5 args))))))


(comment
  "java -jar target/uberjar/cms-0.1.0-SNAPSHOT-standalone.jar cms-10.00 2015 /Users/blah/Downloads/PGYR15_P063016/OP_DTL_GNRL_PGYR2015_P06302016.csv tmp.txt 20 0"
  (def path "/Users/blah/Downloads/PGYR15_P063016/OP_DTL_GNRL_PGYR2015_P06302016.csv")

  (def f "/Users/blah/Desktop/tmp.txt")

  (def path "/home/ubuntu/data/files/OP_DTL_GNRL_PGYR2015_P06302016.csv")

  (def f "/home/ubuntu/tmp.txt")

  (csv-to-file "cms-10.00" 2015 "/Users/blah/Downloads/PGYR15_P063016/OP_DTL_GNRL_PGYR2015_P06302016.csv" "/Users/blah/Desktop/tmp.txt" 200000 6000000)

  "nohup java -jar /home/ubuntu/tightrope-services/cms/target/uberjar/cms-0.1.1-SNAPSHOT-standalone.jar cms-10.00 2013 /home/ubuntu/data/files/OP_DTL_GNRL_PGYR2013_P06302016.csv tmp.txt errors.txt 200000 0 &"
  (with-open [rdr (io/reader path)] (doseq [l (take 100 (rest (line-seq rdr)))] (println (count (parse-cms-row-string l)))))
  "11132001 records in 2015"
  {"v" "cms-2015-01.00"}
  (-> (slurp "/Users/blah/Desktop/10.00.txt") json/read-str)
  ["3" "4" "5" "6" "7" "8" "10" "11" "12" "13" "15" "16" "17" "18" "26" "27" "30" "31" "32" "45" "63"]  )
