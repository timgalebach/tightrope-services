module Main where

import Lib
import System.Environment (getArgs)

main :: IO ()
main = do
  args <- getArgs
  if length args /= 4
  then putStrLn "process_name (cmsCanon, noCanon, getNoCanonZips) start end batchSize"
  else someFunc (args !! 0) (args !! 1) (args !! 2) (args !! 3)
