{-# LANGUAGE OverloadedStrings #-}
module Db where

import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Data.List (maximumBy, groupBy)
import Data.String     (fromString)
import Safe
import Control.Applicative (liftA2, liftA3)
import Config (connStr)

type DocId = Int
type Zip   = String

data Doctor = Doctor
  { tgDocId :: Int
  , tgDocFirst :: Maybe String
  , tgDocMiddle :: Maybe String
  , tgDocLast :: Maybe String
  , tgDocLine1 :: Maybe String
  , tgDocLine2 :: Maybe String
  , tgDocCity :: Maybe String
  , tgDocState :: Maybe String
  , tgDocZip :: Maybe String } deriving (Show)

data DoctorName = DoctorName
  { dnId :: Int
  , dnFirst :: Maybe String
  , dnMiddle :: Maybe String
  , dnLast :: Maybe String } deriving (Show)

data NoAddrDoctor = NADoc
  {
    tgNADId :: Int
  , tgNADCity :: String
  , tgNADState :: String
  , tgNADZip :: String
  } deriving (Show)

data Location = Location { tgLocId :: Int
                         , tgLat :: Double
                         , tgLng :: Double
                         } deriving (Show)

data TableName = TN String
data IdCityState = IdCityState Int String String deriving Show
data Coords = Coords Double Double deriving Show

blankDoctor = Doctor 0 Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing

instance FromRow Doctor where
  fromRow = Doctor
    <$> field <*> field <*> field <*> field <*> field
    <*> field <*> field <*> field <*> field

instance FromRow DoctorName where
  fromRow = DoctorName <$> field <*> field <*> field <*> field

instance FromRow Location where
  fromRow = liftA3 Location field field field

instance FromRow NoAddrDoctor where
  fromRow = NADoc <$> field <*> field <*> field <*> field

instance FromRow IdCityState where
  fromRow = IdCityState <$> field <*> field <*> field

instance FromRow Coords where
  fromRow = Coords <$> field <*> field

getBadLatIds :: IO [IdCityState]
getBadLatIds = do
  conn <- connectPostgreSQL connStr
  query_ conn "(SELECT id, city, state FROM canon_addr_doctors WHERE lng IS NOT NULL AND state IS NOT NULL AND city IS NOT NULL AND city != 'APO' AND lng > 0) UNION (SELECT id, city, state FROM no_canon_addr_doctors WHERE lng IS NOT NULL AND state IS NOT NULL AND city IS NOT NULL AND city != 'APO' AND lng > 0)"

getGoodCoords :: String -> String -> IO (Maybe Coords)
getGoodCoords city state = do
  conn <- connectPostgreSQL connStr
  coords <- query conn "SELECT lat, lng FROM doctors WHERE city = ? AND state = ? AND lng < 0 LIMIT 1" (city, state)
  return . headMay $ coords

updateBadCoords :: Int -> Coords -> IO ()
updateBadCoords id (Coords lat lng) = do
  conn <- connectPostgreSQL connStr
  execute conn "UPDATE canon_addr_doctors SET lat = ?, lng = ? WHERE id = ?" (lat, lng, id)
  execute conn "UPDATE no_canon_addr_doctors SET lat = ?, lng = ? WHERE id = ?" (lat, lng, id)
  putStrLn $ "Updated id: " ++ show id

getDoctorsDb :: TableName -> [Int] -> IO [Doctor]
getDoctorsDb (TN tn) ids = do
  let q = fromString $
          "SELECT id, first, middle, last, line1, line2, city, state, zip FROM "
          ++  tn
          ++ " WHERE id IN ? AND lat IS NULL"
  conn <- connectPostgreSQL connStr
  query conn q $ Only $ In ids

getZipLocation :: TableName -> Doctor -> IO (Maybe Location)
getZipLocation (TN tn) doctor = do
  let q = fromString $
          "SELECT ?, lat, lng FROM "
          ++ tn ++ " WHERE zip LIKE ? AND lat is not NULL AND lng is not NULL "
  conn <- connectPostgreSQL connStr
  res <- query conn q (tgDocId doctor, tgDocZip doctor)
  return $ headMay res

updateLocations :: TableName -> [Location] -> IO ()
updateLocations (TN tn) locs = do
  conn <- connectPostgreSQL connStr
  mapM_ (\loc -> execute conn q (dbFormat loc)) locs
  --executeMany conn q (map dbFormat locs)
  putStrLn $ "inserted maxId " ++ (show . maxId) locs
  where q = fromString $ "UPDATE " ++ tn ++ " SET lat = ?, lng = ? WHERE id = ?"
        dbFormat :: Location -> (Double, Double, Int)
        dbFormat = liftA3 (,,) tgLat tgLng tgLocId
        maxId = (maybe 0 tgLocId . lastMay)

insertNoCanonDoctor :: DocId -> Zip -> IO ()
insertNoCanonDoctor id zipCode = do
  conn <- connectPostgreSQL connStr
  let q = "INSERT INTO no_canon_addr_doctors SELECT docid AS id, docfirstname AS first, docmiddlename AS middle, doclastname AS last, addrline1 AS line1, addrline2 AS line2, state, city, zipcode AS zip, NULL as lat, NULL as lng FROM payments WHERE docid = ? AND zipcode = ? LIMIT 1"
  execute conn q (id, zipCode) >> return ()

insertFurtherReview :: DocId -> IO ()
insertFurtherReview id = do
  conn <- connectPostgreSQL connStr
  let q = "INSERT INTO further_review (id) VALUES (?) "
  execute conn q [id] >> return ()

getNoAddrDoctors :: [Int] -> IO [NoAddrDoctor]
getNoAddrDoctors ids = do
  let q = "SELECT id, city, state, zip FROM no_address_doc_zips WHERE id IN ?"
  conn <- connectPostgreSQL connStr
  query conn q $ Only $ In ids

firstNOfZip :: Int -> [NoAddrDoctor] -> [String]
firstNOfZip n = map (take n.tgNADZip)

uniqueZips :: [NoAddrDoctor] -> [Either DocId (DocId, Zip)]
uniqueZips docs =
  let idGroups = groupBy (\d1 d2 -> tgNADId d1 == tgNADId d2) docs
  in  map zipCodeFromIdGroup idGroups
  where
    zipCodeFromIdGroup :: [NoAddrDoctor] -> Either DocId (DocId, Zip)
    zipCodeFromIdGroup idGroup =
          if allSame $ (firstNOfZip 3) idGroup
          then Right $ ((tgNADId . head) idGroup, bestZip idGroup)
          else Left $  (tgNADId . head) idGroup
          where allSame []     = True
                allSame (x:xs) = and $ map (==x) xs
                bestZip = tgNADZip . maximumBy
                  (\d1 d2 -> ((length.tgNADZip) d1) `compare`
                             ((length.tgNADZip) d2))

processNoCanonDoctors :: [NoAddrDoctor] -> IO ()
processNoCanonDoctors docs = do
  mapM_ process (uniqueZips docs)
  where process :: Either DocId (DocId, Zip) -> IO ()
        process eitherIdZip =
          case eitherIdZip of
            Left id             -> insertFurtherReview id
            Right (id, zipCode) -> insertNoCanonDoctor id zipCode
