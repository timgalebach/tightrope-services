{-# LANGUAGE OverloadedStrings #-}
module Lib where

import Control.Lens
import Control.Applicative
import Control.Monad
import Data.Maybe
import Data.List
import Data.Aeson
import Data.Aeson.Lens
import Control.Concurrent
import Control.Monad (forever)
import Control.Concurrent.Chan
import Data.ByteString.Lazy.Internal
import Network.HTTP.Client
import Network.HTTP.Types.Status
import Network.HTTP.Types.Header
import Control.Monad.Catch (SomeException(..))
import Network.URI.Encode                      as UE
import qualified Control.Exception             as E
import qualified Data.Map                      as Map
import qualified Data.Text                     as T
import qualified Network.Wreq                  as W
import Data.List.Utils (replace)
import Data.String.Utils (strip)
import Db

baseApiUrl = "https://maps.googleapis.com/maps/api/geocode/json?address="
baseCmsDocUrl = "https://openpaymentsdata.cms.gov/resource/h5vq-v5cf.json?$where=physician_profile_id%20%3D%20%27"

newtype ApiKey = ApiKey { tgApiKey :: String } deriving (Show)
newtype GeocodeUrl = GeocodeUrl { tgUrl :: String } deriving (Show)
newtype CmsUrl = CmsUrl { tgCmsUrl :: String } deriving (Show)

formatAddress :: Doctor -> String
formatAddress =
  UE.encode .
  (combine <$> g.tgDocLine1 <*> g.tgDocCity <*> g.tgDocState <*> g.tgDocZip)
  where combine a b c d = a ++ "," ++ b ++ "," ++ c ++ "," ++ d
        removeSpace = replace " " "+"
        g = fromMaybe ""

doctorLocationGeocode :: ApiKey -> Doctor -> (ApiKey -> Doctor -> GeocodeUrl)
                      -> IO (Either Doctor (Maybe Location))
doctorLocationGeocode ak doc urlFn = do
  let (GeocodeUrl geoUrl) = urlFn ak doc
  r <- getNoHttpErrors geoUrl
  case r ^? W.responseBody . key "status" ._String of
    Just "OVER_QUERY_LIMIT" -> return $ Left doc
    _ ->
      return $ Right $ liftA2 (Location (tgDocId doc))
      (r ^? (loc . key "lat" . _Double))
      (r ^? (loc . key "lng" . _Double))
  where loc = W.responseBody . key "results" . nth 0 . key "geometry" .
          key "location"
        getNoHttpErrors url = W.getWith (set W.checkStatus (Just $ \_ _ _ -> Nothing)
                                     W.defaults) url

doctorLocation :: ApiKey -> Doctor -> IO (Either Doctor (Maybe Location))
doctorLocation ak doc = do
  loc <- getZipLocation (TN "doctors") doc
  case loc of
    Nothing -> doctorLocationGeocode ak doc urlFromZip
    _       -> return $ Right loc

mkUrl :: ApiKey -> Doctor -> GeocodeUrl
mkUrl (ApiKey apikey) doc =
  GeocodeUrl $ baseApiUrl ++ (formatAddress doc) ++ "&key=" ++ apikey

urlFromZip :: ApiKey -> Doctor -> GeocodeUrl
urlFromZip (ApiKey ak) doc =
  GeocodeUrl $ baseApiUrl ++ (fromMaybe "" $ tgDocZip doc) ++ "&key=" ++ ak

mkCmsUrl :: Int -> CmsUrl
mkCmsUrl docId =
  CmsUrl $ baseCmsDocUrl ++ show docId ++ "%27"

locationWorker :: ApiKey -> Chan (Either Doctor (Maybe Location)) -> Doctor
               -> IO ()
locationWorker ak chan doctor = do
  loc <- doctorLocationGeocode ak doctor urlFromZip
  writeChan chan loc >> return ()

collectLocations :: ApiKey -> [Doctor] -> IO [Either Doctor (Maybe Location)]
collectLocations ak doctors = do
  let batchSize = 10
  accChan <- newChan
  processDocs accChan doctors batchSize
  go accChan (length doctors) []
  where go chan expectedResults resultAcc = do
          result <- readChan chan
          let newResults = resultAcc ++ [result]
          if (length newResults) == expectedResults
            then return newResults
            else go chan expectedResults newResults
        processDocs c docs batchSize =
          if (not.null) docs
          then do
            mapM_ (locationWorker ak c) (take batchSize docs)
            processDocs c (drop batchSize docs) batchSize
          else return ()


processLocations :: [ApiKey] -> [Doctor] -> [Location]
                 -> IO ([ApiKey], [Location])
processLocations [] _ locs = return ([], locs)
processLocations (ak:aks) docs locs = do
  newLocs <- mapM (doctorLocation ak) docs
  let (good, bad) = ( filter (not.apiErrorP) newLocs
                    , filter apiErrorP newLocs)
      goodLocs = locs ++ (catMaybes $ map locFromEither good)
      badDocs  = map docFromEither bad

  case length badDocs of
    0 -> return (ak:aks, goodLocs)
    _ -> processLocations aks badDocs goodLocs
    -- return ([], [])

  where apiErrorP (Left _) = True
        apiErrorP _        = False
        locFromEither (Right val) = val
        locFromEither _           = Nothing
        docFromEither (Left doc)  = doc
        docFromEither _           = blankDoctor

updateRange :: [ApiKey] -> TableName -> Int -> Int -> Int -> IO ()
updateRange aks tn start end batchSize =
  go aks start (start + batchSize)
  where go apiKeys firstId lastId = do
          if firstId > end then return ()
            else do
            docs <- getDoctorsDb tn [firstId..lastId]
            (newAks, newLocs) <- processLocations aks docs []
            updateLocations tn newLocs

            if null newAks then return ()
            else go newAks (lastId+1) (lastId+batchSize)

processNoAddrDoctors :: Int -> Int -> Int -> IO ()
processNoAddrDoctors start end batchSize =
 go start (start + (batchSize-1))
 where go s e = do
         if s > end
           then return ()
           else do
           docs <- getNoAddrDoctors [s..e]
           processNoCanonDoctors docs
           go (e+1) (e+batchSize)

apiKeys = map ApiKey [ "AIzaSyBlr75ocRBry008x1rRpJk_BcoV6z8zqjU" --Kub
                     , "AIzaSyARsGAR4G8sNpDisYJOgRsaz0Ixe8I3BAs" --My Project
                     , "AIzaSyBE3knLAVnFeZQgZAb_dNjT1s-G9VX3oAM" --tightrope
                     , "AIzaSyA7qeXRg0_AbytVRi97pjioXTCnNSaGQWI"
                     , "AIzaSyDgL1ib4QDrxb58xNpLdSMjePfWWovyyRw"
                     , "AIzaSyCypN-yuD-wJuvGKdhzoCKjqhVUJQo6yUY"
                     , "AIzaSyCrcUev0URXc7dcKAssNp7vMpE6vCc-OWo"
                     , "AIzaSyDlayki1ByclkPxHQSVZN7Y5k5a_SB_MVA"
                     , "AIzaSyBvbrYCP88XohzcnhrsaGtE5yfxcsNfSRo"
                     , "AIzaSyDP1e0Dr73hdq5aotsz7DaRX0XqkFhUgq4"
                     , "AIzaSyBA-pvbT6y2h1F64UwBez93wFPxyutCXqg"
                     , "AIzaSyD0tFUCxiaol76Tm8Ps-hksiiKithNEfGU" --tightrope9
                     , "AIzaSyCdcAMNMMRGRMnwe8PzpVBC5srQytzYl_Y" --airepaisa
                     ]

sample = "https://maps.googleapis.com/maps/api/geocode/json?address=1901+SW+172ND+AVENUE,MIRAMAR,FL,33029-5592&key=AIzaSyBE3knLAVnFeZQgZAb_dNjT1s-G9VX3oAM"

someFunc :: String -> String -> String -> String -> IO ()
someFunc process start end batchSize = do
  let s = read start      :: Int
      e = read end        :: Int
      bs = read batchSize :: Int
  case process of
    "getNoCanonZips"   -> processNoAddrDoctors s e bs
    "cmsCanon"   -> updateRange apiKeys (TN "canon_addr_doctors") s e bs
    "noCanon"    -> updateRange apiKeys (TN "no_canon_addr_doctors") s e bs

{-

StatusCodeException (Status {statusCode = 500, statusMessage = "Internal Server Error"}) [("Content-Type","application/json; charset=UTF-8"),("Date","Thu, 05 Jan 2017 02:05:07 GMT"),("Pragma","no-cache"),("Expires","Fri, 01 Jan 1990 00:00:00 GMT"),("Cache-Control","no-cache, must-revalidate"),("Vary","Accept-Language"),("Access-Control-Allow-Origin","*"),("Content-Encoding","gzip"),("Server","mafe"),("X-XSS-Protection","1; mode=block"),("X-Frame-Options","SAMEORIGIN"),("Alt-Svc","quic=\":443\"; ma=2592000; v=\"35,34\""),("Transfer-Encoding","chunked"),("X-Response-Body-Start","{\n   \"results\" : [],\n   \"status\" : \"UNKNOWN_ERROR\"\n}\n"),("X-Request-URL","GET https://maps.googleapis.com:443/maps/api/geocode/json?address=1220%20AVENUE%20P%2CBROOKLYN%2CNY%2C11229-1009&key=AIzaSyBE3knLAVnFeZQgZAb_dNjT1s-G9VX3oAM")] (CJ {expose = []})


{
   "error_message" : "You have exceeded your daily request quota for this API.",
   "results" : [],
   "status" : "OVER_QUERY_LIMIT"

}

r ^? W.responseBody . key "status" ._String
Just "OVER_QUERY_LIMIT"

-}
