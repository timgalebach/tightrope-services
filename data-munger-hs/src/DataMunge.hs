{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module DataMunge where

import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToRow
import Data.List (maximumBy, groupBy)
import Data.String     (fromString, IsString)
import Data.Maybe (fromMaybe)
import Data.List (maximum, sort, map)
import Data.Char (toUpper, toLower)
import Data.String     (fromString)
import qualified Data.Vector as V
import Safe (headMay)
import Control.Applicative (liftA2, liftA3)
import Control.Monad (foldM_)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Primitive (PrimBase)
import Config (connStr)
import Conduit
import Control.Monad.Trans.Resource (allocate, release)
import Data.Conduit.PostgreSQL (sourceQuery_, sourceQuery, sinkUpdate)
import Db

upperDocName :: DoctorName -> DoctorName
upperDocName =
  DoctorName <$> dnId <*> (g . dnFirst) <*> (g . dnMiddle) <*> (g . dnLast)
  where g = fmap (map toUpper)

updateDocNames :: Connection -> V.Vector DoctorName -> IO ()
updateDocNames conn doctors = do
  executeMany conn
    "UPDATE canon_addr_doctors SET first = upd.f, middle = upd.m, last = upd.l FROM (values (?, ?, ?, ?)) AS upd(f, m, l, i) WHERE id = upd.i"
    (V.toList $ V.map docTuple doctors)
  print $ "Updated " ++  (show $ V.length doctors) ++ " doctors."
  where docTuple (DoctorName i f m l) = ( fromMaybe "" f, fromMaybe "" m
                                        , fromMaybe "" l, i)

test :: IO ()
test = do
  runConduitRes $
    sourceQuery_ connStr "SELECT id, first, middle, last FROM doctors LIMIT 10"
    .| mapC (fromMaybe "" . dnLast)
    .| sinkFile "/Users/blah/Desktop/tmp.txt"

checkDocNames :: Int -> String -> IO ()
checkDocNames vecSize table =
  let q = fromString $ "SELECT id, first, middle, last FROM " ++ table ++ " LIMIT 20"
  in runConduitRes $ do
    sourceQuery_ connStr q
     .| mapC upperDocName
     .| conduitVector vecSize
     .| mapM_C (liftIO . print . (V.map dnFirst))
