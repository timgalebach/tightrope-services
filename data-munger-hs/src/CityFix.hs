{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module CityFix where

import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Data.List (maximumBy, groupBy)
import Data.String     (fromString, IsString)
import Data.List (maximum, sort)
import Safe (headMay)
import Control.Applicative (liftA2, liftA3)
import Control.Monad (foldM_)
import Config (connStr)
import Text.EditDistance (levenshteinDistance, defaultEditCosts)


type City  = String
type State = String
type Count = Int
newtype Zip = Zip String deriving (Show, Eq, Ord, IsString)
data CityState = CityState City State Count deriving Show
data GoodCityState = GCS CityState deriving Show

instance Eq CityState where
  (==) (CityState _ _ count1) (CityState _ _ count2) = count1 == count2
instance Ord CityState where
  (<=) (CityState _ _ count1) (CityState _ _ count2) = count1 <= count2

instance FromRow Zip where
  fromRow = Zip <$> field

instance FromRow CityState where
  fromRow = CityState <$> field <*> field <*> field

doAll :: IO ()
doAll = do
  zs <- dupZips
  cityLists <- mapM badCities (sort zs)
  go $ citiesToFix <$> cityLists
  where go [] = pure ()
        go ((toFix, goodCity):xs) = mapM_ (replaceFixer goodCity) toFix >> (go xs)

dupZips :: IO [Zip]
dupZips = do
  conn <- connectPostgreSQL connStr
  query_ conn "SELECT zip FROM city_zip_count WHERE length(zip) > 0 GROUP BY zip HAVING COUNT(city) > 1"

badCities :: Zip -> IO [CityState]
badCities (Zip zip) = do
  print zip
  conn <- connectPostgreSQL connStr
  query conn "SELECT city, state, SUM(count :: int) FROM city_zip_count WHERE state IS NOT NULL AND zip = ? GROUP BY city, state" $ Only zip

replaceFixer :: GoodCityState -> CityState -> IO ()
replaceFixer (GCS (CityState goodCity goodState _)) (CityState badCity badState _) = do
  conn <- connectPostgreSQL connStr
  let q = "UPDATE canon_addr_doctors SET city = ?, state = ? WHERE city = ? AND state = ?"
      q2 = "UPDATE no_canon_addr_doctors SET city = ?, state = ? WHERE city = ? AND state = ?"
  execute conn q  (goodCity, goodState, badCity, badState)
  execute conn q2 (goodCity, goodState, badCity, badState)
  print $ "Fixed: " ++ badCity ++ ", " ++  badState

fix :: GoodCityState -> [CityState] -> IO ()
fix gcs = mapM_ (replaceFixer gcs)

--list of bad city names along with their replacement
citiesToFix :: [CityState] -> ([CityState], GoodCityState)
citiesToFix css =
  let goodCity = maximum css
  in ( filter (\cs -> (cityMatch goodCity cs) && (not . (cityNameEq goodCity)) cs) css
     , GCS goodCity)

cityMatch :: CityState -> CityState -> Bool
cityMatch (CityState c1 s1 _) (CityState c2 s2 _ ) =
  (s1 == s2) && --states have to match
  ((backAndFront 2 c1 c2) || (backOrFront 3 c1 c2) ||
  ((levenshteinDistance defaultEditCosts c1 c2) <= 2))
  where takeEnd n = reverse . (take n) . reverse
        backAndFront n c1 c2 = (take n c1 == take n c2)
          && (takeEnd n c1 == takeEnd n c2)
        backOrFront n c1 c2 = (take n c1 == take n c2)
          || (takeEnd n c1 == takeEnd n c2)

cityNameEq :: CityState -> CityState -> Bool
cityNameEq (CityState c1 s1 _) (CityState c2 s2 _) = (c1 == c2) && (s1 == s2)

run =
  let gcs = (GCS (CityState "KANSAS CITY" "MO" 0))
      bcs = (CityState "NORTH KANSAS CITY" "MO" 0)
  in replaceFixer gcs bcs
