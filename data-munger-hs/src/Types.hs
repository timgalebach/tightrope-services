{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

module Types where

import Database.PostgreSQL.Simple (Connection)
import Database.PostgreSQL.Simple.FromRow
import Data.Aeson
