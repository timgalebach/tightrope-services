module Extra where

import Db

fixId :: IdCityState -> IO ()
fixId (IdCityState id city state) = do
  maybeCoords <- getGoodCoords city state
  case maybeCoords of
    Nothing -> putStrLn $ "no coords work for id: " ++ show id
    Just coords ->
      updateBadCoords id coords

fixAllBad :: IO ()
fixAllBad = do
  ids <- getBadLatIds
  mapM_ fixId ids
