--add fuzzy text searching
CREATE EXTENSION pg_trgm;

CREATE TABLE payments (
  recordId          bigint,
  docId             bigint,
  docType           varchar(150),
  docFirstName      varchar(150),
  docMiddleName     varchar(150),
  docLastName       varchar(150),
  addrLine1         varchar(150),
  addrLine2         varchar(150),
  city              varchar(150),
  state             varchar(20),
  zipCode           varchar(15),
  province          varchar(150),
  postalCode        varchar(20),
  country           varchar(150),
  teachingHospID    bigint,
  teachingHospName  varchar(150),
  paymentMakerID    bigint,
  paymentMakerName  varchar(150),
  programYear       int,
  numPayments       int,
  paymentDate       varchar(30),
  totalPayment      decimal
  );

UPDATE payments SET city = UPPER(city);
UPDATE payments SET (docType, docFirstName, docMiddleName, docLastName, addrLine1,
                    addrLine2, city, state, zipCode, province, postalCode, country,
                    teachingHospName, paymentMakerName, paymentDate) =
                    (REPLACE(docType, '"', ''), REPLACE(docFirstName, '"', ''),
                    REPLACE(docMiddleName, '"', ''), REPLACE(docLastName, '"', ''),
                    REPLACE(addrLine1, '"', ''), REPLACE(addrLine2, '"', ''),
                    REPLACE(city, '"', ''), REPLACE(state, '"', ''),
                    REPLACE(zipCode, '"', ''), REPLACE(province, '"', ''),
                    REPLACE(postalCode, '"', ''), REPLACE(country, '"', ''),
                    REPLACE(teachingHospName, '"', ''),
                    REPLACE(paymentMakerName, '"', ''), REPLACe(paymentDate, '"', ''));

UPDATE payments SET paymentMakerName = UPPER(paymentMakerName);

CREATE INDEX ON payments (docId);

CREATE TABLE canon_addr_doctors (
  id bigint,
  first varchar(100),
  middle varchar(100),
  last varchar(100),
  line1 varchar(200),
  line2 varchar(200),
  state varchar(4),
  city varchar (100),
  zip varchar (20),
  lat double precision,
  lng double precision,
  primary key (id)
  );

--doctors whose addresses aren't in CMS
CREATE TABLE no_canon_addr_doctors AS SELECT * FROM doctors LIMIT 0;
CREATE TABLE further_review ( id bigint );

--union of all doctors, canonical and no canonical addresses
CREATE MATERIALIZED VIEW doctors AS (SELECT * FROM canon_addr_doctors) UNION (SELECT * FROM no_canon_addr_doctors);
CREATE INDEX ON doctors (id);

--total money and payments by doctor
CREATE MATERIALIZED VIEW doc_money AS SELECT docId, SUM(totalPayment) FROM payments where docId != -1 GROUP BY docId;

--Doctor, payer with total amount per payer
CREATE MATERIALIZED VIEW doc_payer AS SELECT docId, paymentMakerId AS payer, SUM(totalPayment) FROM payments where docId != -1 AND paymentMakerId != 1 GROUP BY docId, paymentMakerId;

--all payers
CREATE MATERIALIZED VIEW tmp_payers AS SELECT payer AS id, SUM(sum) AS amount FROM doc_payer WHERE payer != -1 GROUP BY payer;

--version that de-duplicates names
CREATE MATERIALIZED VIEW payers AS SELECT id, amount, MIN(payments.paymentMakerName) AS name FROM tmp_payers, payments WHERE paymentMakerId = tmp_payers.id GROUP BY id, amount;

--number of doctors in each city
CREATE MATERIALIZED VIEW city_num_docs AS SELECT COUNT(DISTINCT id) AS num_docs, city, state, avg(lat) AS lat, avg(lng) AS lng FROM doctors WHERE state IS NOT NULL AND lat IS NOT NULL GROUP BY city, state;

--all doctors that we don't have CMS addresses for
CREATE MATERIALIZED VIEW all_doc_ids AS SELECT DISTINCT(docid) id FROM payments;
CREATE MATERIALIZED VIEW no_address_doc_ids AS select docid as id from all_doc_ids except select id from doctors;
CREATE MATERIALIZED VIEW no_address_doc_zips AS SELECT docid as id, city, state, zipcode AS zip from payments, no_address_doc_ids where docid=id AND docid != -1 AND zipcode IS NOT NULL AND city IS NOT NULL AND state IS NOT NULL GROUP BY docid, zipcode, city, state;

------------ START DATA MUNGING ----------------------
CREATE MATERIALIZED VIEW city_zip_count AS SELECT COUNT(city), city, state, substring(zip from 1 for 5) AS zip FROM doctors GROUP BY city, state, substring(zip FROM 1 for 5);

SELECT COUNT(city), zip FROM city_zip_count GROUP BY zip HAVING COUNT(city) > 1;
------------   END DATA MUNGING ----------------------


-----TROUBLESHOOTING
--get all bad cities
SELECT COUNT(*) FROM original_cities WHERE city ~ '.*,.*';

-----all doctor ids that we don't have city/state coordinates for
CREATE MATERIALIZED VIEW no_location_doctor AS
SELECT docid, city, state FROM payments
WHERE docid IN
((SELECT DISTINCT docid FROM city_doc)
EXCEPT
(SELECT DISTINCT cd.docId FROM city_doc AS cd, city_state AS cc WHERE cd.city = cc.city AND cd.state=cc.state));


---get the cities that have the most doctors we don't have a location for
SELECT state, city, COUNT(DISTINCT docId) AS num_docs FROM no_location_doctor GROUP BY city, state ORDER BY num_docs desc LIMIT 10;

--dump and backup certain tables
sudo -u postgres pg_dump -d postgres -t no_canon_addr_doctors > 06_03_2017_no_canon_addr_doctors.sql
aws s3 cp ./06_03_2017_no_canon_addr_doctors.sql s3://tightropecp/data/doctor_info/06_03_2017_no_canon_addr_doctors.sql

sudo -u postgres pg_dump -d postgres -t canon_addr_doctors > 06_03_2017_canon_addr_doctors.sql
aws s3 cp ./06_03_2017_canon_addr_doctors.sql s3://tightropecp/data/doctor_info/06_03_2017_canon_addr_doctors.sql
--sudo -u postgres pg_dump -d postgres -t further_review > 01_09_2017_further_review.sql
--sudo -u postgres pg_dump -d postgres -t doctors > 01_09_2017_doctors_no_lat_lng.sql

-----DROP commands
DROP MATERIALIZED VIEW city_num_docs;
DROP MATERIALIZED VIEW payers;
DROP MATERIALIZED VIEW tmp_payers;
DROP MATERIALIZED VIEW doc_payer;
DROP MATERIALIZED VIEW doc_money;
DROP MATERIALIZED VIEW doctors;
