Time: 109.348 ms
postgres=# select count(distinct docid) from city_doc;
 810801

Time: 320.446 ms
postgres=# SELECT COUNT(*) FROM city_doc AS cd, citycoords AS cc WHERE cd.city = cc.city AND cd.state=cc.state;
 1049022

Time: 559.822 ms
postgres=# SELECT COUNT(DISTINCT cd.docId) FROM city_doc AS cd, citycoords AS cc WHERE cd.city = cc.city AND cd.state=cc.state;
 740076

Missing 70K doc ids -- get the difference




SELECT COUNT(*) FROM city_lat_lng WHERE lat > 35 AND lat < 45 AND lng > -120 AND lng < -80;

SELECT COUNT(DISTINCT cd.docId) FROM city_lat_lng AS cll, city_doc AS cd WHERE lat > 40 AND lat < 45 AND lng > -120 AND lng < -110 AND cll.city = cd.city AND cll.state = cd.state;

"""SELECT num_doc, city, state, lat, lng FROM city_num_docs WHERE lat > %s AND lat < %s AND lng > %s AND lng < %s"""


----functions to do doctors by lat/lng box
SELECT DISTINCT cd.docId FROM city_state_lat_lng3 AS cll, city_doc AS cd WHERE cll.lat > 40 AND cll.lat < 45 AND cll.lng > -120 AND cll.lng < -110 AND cll.city = cd.city AND cll.state = cd.state;


--get all cities that sort of match
SELECT city, state FROM city_num_docs WHERE CONCAT(city, ', ', state) % 'ALBANY NY' LIMIT 10;

CREATE MATERIALIZED VIEW no_address_doc_ids AS select docid as id from all_doc_ids except select id from doctors;

CREATE MATERIALIZED VIEW no_address_doc_zips AS SELECT docid as id, zipcode AS zip from payments, no_address_doc_ids where docid=id AND docid != -1 group by docid, zipcode;

INSERT INTO no_canon_addr_doctors SELECT docid AS id, docfirstname AS first, docmiddlename AS middel, doclastname AS last, addrline1 AS line1, addrline2 AS line2, state, city, zipcode AS zip, NULL as lat, NULL as lng FROM payments WHERE docid = 126154 AND zipcode = '02601-3845' LIMIT 1;

ALTER TABLE no_canon_addr_doctors ALTER COLUMN state TYPE varchar(20);


SELECT * FROM doctors WHERE lat > $1 AND lat < $2 AND lng > $3 AND lng < $4 LIMIT $5


--select concat(line1, ', ', city, ', ', state, ' ', zip)


SELECT doctors.*, SUM(sum) AS amount FROM doctors, doc_payer WHERE lat > $1 AND lat < $2 AND lng > $3 AND lng < $4 AND id = docid AND doc_payer.sum > $5 AND doc_payer.sum < $6 AND doc_payer.payer != 100000010575 GROUP BY doctors.* LIMIT $7;

SELECT doctors.id, SUM(sum) AS amount FROM doctors, doc_payer WHERE lat > 30 AND lat < 40 AND lng > -80 AND lng < -70 AND id = docid AND doc_payer.sum > 0 AND doc_payer.sum < 200000 AND doc_payer.payer != 100000010575 GROUP BY doctors.id LIMIT 50;

SELECT doctors.*, sum AS amount FROM doctors, doc_money WHERE lat > $1 AND lat < $2 AND lng > $3 AND lng < $4 AND id = docid AND doc_money.sum > $5 AND doc_money.sum < $6 LIMIT $7;

SELECT *,
(SELECT COUNT(*) FROM doctors WHERE lat > 40 AND lat < 45 AND lng > -110 AND lng < -75) AS count
FROM doctors WHERE lat > 40 AND lat < 45 AND lng > -110 AND lng < -75;

--SELECT *, (SELECT COUNT(*) FROM doctors) AS count FROM doctors WHERE lat > $1 AND lat < $2 AND lng > $3 AND lng < $4 LIMIT $5

--SELECT *, (SELECT COUNT(*) FROM doctors) AS count FROM doctors LIMIT 100;
