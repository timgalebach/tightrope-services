#!/bin/bash

if [ $# -ne 1 ]; then
    echo "usage: backup-doctors.sh FILE_PREFIX"
    exit 1
fi


sudo -u postgres pg_dump -d postgres -t no_canon_addr_doctors > $1_no_canon_addr_doctors.sql
sudo -u postgres pg_dump -d postgres -t canon_addr_doctors > $1_canon_addr_doctors.sql

aws s3 cp $1_no_canon_addr_doctors.sql s3://tightropecp/data/doctor_info/$1_no_canon_addr_doctors.sql
aws s3 cp $1_canon_addr_doctors.sql s3://tightropecp/data/doctor_info/$1_canon_addr_doctors.sql
