(defproject node-example "0.1.0-SNAPSHOT"
  :description "FIXME: write this!"
  :url "http://example.com/FIXME"

  :min-lein-version "2.5.3"

  :dependencies [[org.clojure/clojure "1.9.0-alpha14"]
                 [org.clojure/clojurescript "1.9.293"]
                 [org.clojure/core.async "0.2.395"]]

  :plugins [[lein-cljsbuild "1.1.4" :exclusions [[org.clojure/clojure]]]
            [lein-figwheel "0.5.8"]
            [lein-npm "0.6.2"]]

  :npm {:dependencies [[request "2.79.0"]
                       [aws-sdk "2.7.7"]]}

  :source-paths ["src"]

  :clean-targets ["server.js"
                  "target"]

  :cljsbuild {
    :builds [{:id "dev"
              :source-paths ["src"]
              :figwheel true
              :compiler {
                         :main node-example.core
                         :output-to "target/server_dev/node_example.js"
                         :output-dir "target/server_dev"
                         :target :nodejs
                         :optimizations :none
                         :source-map true}}
             {:id "prod"
              :source-paths ["src"]
              :compiler {
                         :main node-example.core
                         :output-to "server.js"
                         :output-dir "target/server_prod"
                         :target :nodejs
                         :optimizations :simple}}]})
