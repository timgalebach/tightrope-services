(ns node-example.core
  (:require [cljs.nodejs :as nodejs]
            [clojure.spec :as s]
            [cljs.core.async :refer [>! <! chan]]))

(def aws (js/require "aws-sdk"))


(nodejs/enable-util-print!)

(defn s3-put-object [bucket key data]
  (let [s3 (aws.S3.)
        params (doto (js/Object.)
                 (aset "Bucket" bucket)
                 (aset "Key" key)
                 (aset "Body" data))]
    (.putObject s3 params (fn [err data]))))

(defn handler [event context]
  (println "got an event")
  (println (.stringify js/JSON event nil 2))

  ;;return a value
  (.succeed context "timtime is here"))

(set! (.-handler js/exports) handler)

(defn -main []
  (println "Hello world!"))

(set! *main-cli-fn* -main)

(comment
  )
