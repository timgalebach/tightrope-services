(ns pdfparse.core
  (:gen-class)
  (:require [clojure.tools.cli :refer [parse-opts]]
            [clojure.java.io :as io]
            [clojure.string :as str])
  (:import [com.snowtide.pdf Document Page OutputHandler OutputTarget
            VisualOutputTarget]
           [com.snowtide.pdf.layout Table]
           [com.snowtide.pdf.util TableUtils]
           [com.snowtide.pdf.layout Rectangle]
           [com.snowtide PDF]))

(def DELIM \д)
(def LINEBREAK "л")
(def STARTSTRING "STARTPDFPROCESS")

(def cli-options
  ;; An option with a required argument
  [["-s" "--start-page START" "Page to start with, 0-indexed"
    :default 0
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ["-e" "--end-page START" "Page to end with, 0-indexed"
    :default 1
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 1 % 0x10000) "Must be a number between 1 and 65536"]]
   ["-f" "--file-name FILENAME" "PDF Input File"
    :validate [#(> (count %) 0) "Filename must have more than 0 characters"]]])

(defn visual-output-target-handler [s] (VisualOutputTarget. s))
(defn output-target-handler [s] (OutputTarget. s))

(def output-handlers
  {:normal output-target-handler
   :visual visual-output-target-handler})

(defn get-page [^PDF pdf page-num]
  (.getPage pdf page-num))

(defn page-with-tables->strings [^Page page]
  (let [csv (TableUtils/convertToCSV
             (first (TableUtils/getAllTables page)) DELIM LINEBREAK)]
    (->> (str/split csv (re-pattern LINEBREAK))
;         (map #(str/replace % (re-pattern (str \" DELIM \")) (str DELIM)))
;         (map #(str/replace % #"^\"" ""))
;         (map #(str/replace % #"\"$" ""))
                                        ;         (map #(str/split % (re-pattern (str DELIM))))
         identity
         )))

;;use one of the handlers above
(defn tmp-write-page [^PDF pdf target-type page-num]
  (with-open [w (io/writer "/Users/blah/Desktop/tmp.txt" :append false)
              s (java.io.StringWriter.)]
    (.pipe (get-page pdf page-num) ((target-type output-handlers) s))
    (.write w (.toString s))
    (.toString s)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println STARTSTRING)
  (let [{:keys [start-page end-page file-name]}
        (:options (parse-opts args cli-options))
        pdf (PDF/open (io/file file-name))]
    (doseq [page-num (range start-page (inc end-page))]
      (doseq [l (page-with-tables->strings (get-page pdf page-num))]
        (println l)))
    (.close pdf)))

(comment

  (def pdf (PDF/open (io/file "/Users/blah/Downloads/2012-2014-boehringer-ingelheim.pdf")))
  (def pdf (PDF/open (io/file "/Users/blah/Downloads/fortherecordpayments-gsk.pdf")))
  (def pdf (PDF/open (io/file "/Users/blah/Downloads/Eli-Lilly.pdf")))
  ;;doesn't work currently
  (def eli-crop (Rectangle. 0.0 237.84 791.0 677.0))

  (.close pdf)


  "NOTES: use VisualOutputTarget for the files with no lines. Table stuff works better for others"

  )
