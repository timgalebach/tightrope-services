(defproject pdfparse "0.1.0-SNAPSHOT"
  :description "cli PDF parser"
  :url "http://deepvector.net"
  :license {:name "all rights reserved DeepVector LLC"
            :url "none"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/tools.cli "0.3.5"]
                 [com.snowtide/pdfxstream "3.4.0"]]
  :repositories {"snowtide-releases" "http://maven.snowtide.com/releases"}
  :main ^:skip-aot pdfparse.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
