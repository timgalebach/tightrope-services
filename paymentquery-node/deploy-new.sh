#!/bin/bash

if [ $# -eq 0 ]; then
    echo "deploy.sh <FN NAME> <HANDLER>"
    exit 1
fi

zip -r deploy.zip index.js node_modules/ && aws lambda create-function --function-name $1 --runtime nodejs4.3 --role arn:aws:iam::468195979091:role/lambda_basic_execution --handler $2 --timeout 10 --zip-file fileb://deploy.zip && rm deploy.zip
