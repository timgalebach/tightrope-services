var pg = require('pg');

var config = {
  user: 'postgres', //env var: PGUSER
  database: 'postgres', //env var: PGDATABASE
  password: 'ms29lkw234obntimiscool', //env var: PGPASSWORD
  host: '52.91.68.170', // Server hosting the postgres database
  port: 5432, //env var: PGPORT
  max: 10, // max number of clients in the pool
  idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
};

var pool = new pg.Pool(config);

// to run a query we can acquire a client from the pool,
// run a query on the client, and then return the client to the pool
function test() {
    pool.connect(function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query('SELECT $1::int AS number', ['1'], function(err, result) {
            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }
            console.log(result.rows[0].number);
        });
    });
}

function cities(context, bounds) {
    bounds = bounds.map(parseFloat);
    pool.connect(function(err, client, done) {
         if(err) {
            return console.error('error fetching client from pool', err);
         }
        q = "SELECT num_docs, city, state, lat, lng FROM city_num_docs WHERE lat > $1 AND lat < $2 AND lng > $3 AND lng < $4";
        client.query({text: q,  values: [bounds[0], bounds[2], bounds[1], bounds[3]]},
                     function(err, result) {
                         console.log(JSON.stringify(result.rows));
                         for (i=0; i<result.rows.length; i++) {
                             result.rows[i]["num_docs"] = parseInt(result.rows[i]["num_docs"]);
                         }
                         context.succeed(result.rows);
//                         callback(null, result.rows);
                     });
    });
}

function doctorsByBounds(context, bounds, payMin, payMax, limit) {
    bounds = bounds.map(parseFloat);
    pool.connect(function(err, client, done) {
         if(err) {
            return console.error('error fetching client from pool', err);
         }
        //q = "SELECT doctors.*, sum AS amount, (SELECT COUNT(*) FROM doctors, doc_money WHERE lat > $1 AND lat < $2 AND lng > $3 AND lng < $4 AND id = docid AND doc_money.sum > $5 AND doc_money.sum < $6) AS num_docs FROM doctors, doc_money WHERE lat > $1 AND lat < $2 AND lng > $3 AND lng < $4 AND id = docid AND doc_money.sum > $5 AND doc_money.sum < $6 LIMIT $7";
        var q = "SELECT doctors.*, sum AS amount FROM doctors, doc_money WHERE lat > $1 AND lat < $2 AND lng > $3 AND lng < $4 AND id = docid AND doc_money.sum > $5 AND doc_money.sum < $6 LIMIT $7";
        console.log(q);
        client.query({text: q,  values: [bounds[0], bounds[2],
                                         bounds[1], bounds[3],
                                         payMin, payMax, limit]},
                     function(err, result) {
                         for(var i=0; i<result.rows.length; i++) {
                             result.rows[i].amount =
                                 parseFloat(result.rows[i].amount);
//                             result.rows[i].num_docs =
//                                 parseInt(result.rows[i].num_docs);
                         }
                         console.log(result.rows);
                         context.succeed(result.rows);
                     });
    });
}

function doctorById(context, id) {
    pool.connect(function(err, client, done) {
         if(err) {
            return console.error('error fetching client from pool', err);
         }
        q = "SELECT * FROM doctors WHERE id = $1";
        client.query({text: q,  values: [id]},
                     function(err, result) {
                         console.log(JSON.stringify(result.rows));
                         context.succeed(result.rows[0]);
                     });
    });
}

function doctorDetailById(context, id, noPayersP) {
    pool.connect(function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        if(noPayersP) {
            var q2 = "SELECT doctors.* FROM doctors WHERE doctors.id = $1";
            client.query({text: q2, values: [id]},
                         function(err, result) {
                             result.rows[0].payers = [];
                             result.rows[0].id = parseInt(result.rows[0].id);
                             console.log(JSON.stringify(result.rows[0]));
                             context.succeed(result.rows[0]);
                         });
            return true;
        }
        var q = "SELECT doctors.*, payers.name AS payername, payer AS payerid, sum AS amount FROM doctors, payers, doc_payer WHERE doctors.id = $1 AND doctors.id = docId AND payer = payers.id ORDER BY sum DESC";
        client.query({text: q,  values: [id]},
                     function(err, result) {
                         if(typeof result.rows[0] == "undefined")
                         {
                             console.log("no payers found");
                             doctorDetailById(context, id, true);
                             return true;
                         }
                         var res = result.rows[0];
                         res.payers = []
                         for(var i=0; i<result.rows.length; i++) {
                             res.payers.push(
                                 {payerId: parseInt(result.rows[i].payerid),
                                  payerName : result.rows[i].payername,
                                  amount: parseFloat(result.rows[i].amount)});
                         }
                         delete res.payerid;
                         delete res.amount;
                         delete res.payername;
                         res.id = parseInt(res.id);
                         console.log(JSON.stringify(res));
                         context.succeed(res);
                     });
    });
}


function fuzzyCitySearch(context, searchString) {
    pool.connect(function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
         }
        q = "SELECT city, state, lat, lng FROM city_num_docs WHERE CONCAT(city, ' ', state) % $1 LIMIT 15;"
        client.query({text: q,  values: [searchString]},
                     function(err, result) {
                         console.log(JSON.stringify(result.rows));
                         context.succeed(result.rows);
                     });
    });
}

function citySearch(context, searchString) {
    pool.connect(function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
         }
        q = "SELECT city, state, lat, lng FROM city_num_docs WHERE CONCAT(city, ', ', state) ILIKE $1 LIMIT 15;"
        client.query({text: q,  values: [searchString + "%"]},
                     function(err, result) {
                         console.log(JSON.stringify(result.rows));
                         context.succeed(result.rows);
                     });
    });
}

function payerSearch(context, searchString) {
    pool.connect(function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
         }
        q = "SELECT id, name FROM payers WHERE name ILIKE $1 LIMIT 10;"
        client.query({text: q,  values: [searchString + "%"]},
                     function(err, result) {
                           for(var i=0; i<result.rows.length; i++) {
                             result.rows[i].id =
                                 parseInt(result.rows[i].id);
                           }
                         console.log(JSON.stringify(result.rows));
                         context.succeed(result.rows);
                     });
    });
}

function cityByName(context, cityName) {
    pool.connect(function(err, client, done) {
         if(err) {
            return console.error('error fetching client from pool', err);
         }
        q = "SELECT num_docs, city, state, lat, lng FROM city_num_docs WHERE CONCAT(city, ', ', state) = $1";
        client.query({text: q,  values: [cityName]},
                     function(err, result) {
                         console.log(JSON.stringify(result.rows));
                         result.rows[0]["num_docs"] = parseInt(result.rows[0]["num_docs"]);
                         context.succeed(result.rows[0]);
                     });
    });
}

function doctorsByBoundsPayers(context, bounds, payMin, payMax, exclude_payers, include_payers, limit) {
    bounds = bounds.map(parseFloat);
    pool.connect(function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        var incPStr = " AND doc_payer.payer IN ( ";
        var excPStr = " doc_payer.payer NOT IN ( ";
        var finalClause = "";
        for (var x = 0; x < include_payers.length; x++) {
            incPStr += ( include_payers[x] + ", ");
        }
        incPStr = incPStr.substring(0, incPStr.length - 2) + ") ";
        for (x = 0; x < exclude_payers.length; x++) {
            excPStr += ( exclude_payers[x] + ", ");
        }
        excPStr = excPStr.substring(0, excPStr.length - 2) + ") ";

        if (include_payers.length != 0) { finalClause += incPStr }
        if (exclude_payers.length != 0) { finalClause += (" AND " + excPStr); }

        //console.log(finalClause);

        var q = "SELECT id, first, middle, last, line1, line2, city, state, zip, lat, lng, SUM(sum) AS amount FROM doctors, doc_payer WHERE lat > $1 AND lat < $2 AND lng > $3 AND lng < $4 AND id = docid " +
                finalClause +
                "GROUP BY doctors.id " +
                " HAVING SUM(sum) > $5 AND SUM(sum) < $6 " +
                " ORDER BY amount DESC LIMIT $7";
        console.log(q);
        client.query({text: q,  values: [bounds[0], bounds[2],
                                         bounds[1], bounds[3],
                                         payMin, payMax, limit]},
                     function(err, result) {
                         for(var i=0; i<result.rows.length; i++) {
                             result.rows[i].amount =
                                 parseFloat(result.rows[i].amount);
                             result.rows[i].id =
                                 parseInt(result.rows[i].id);
                         }
                         console.log(result.rows);
                         context.succeed(result.rows);
                     });
    });

}

pool.on('error', function (err, client) {
  // if an error is encountered by a client while it sits idle in the pool
  // the pool itself will emit an error event with both the error and
  // the client which emitted the original error
  // this is a rare occurrence but can happen if there is a network partition
  // between your application and the database, the database restarts, etc.
  // and so you might want to handle it and at least log it out
  console.error('idle client error', err.message, err.stack)
})

exports.handler = function(event, context, callback) {
    cities(context, event["bounds"]);
    console.log('Here is the event:', JSON.stringify(event, null, 2));
    //console.log("finished");
    //callback(null, "Success");

};

exports.doctorByIdHandler = function(event, context, callback) {
    doctorById(context, event["id"]);
    console.log('Here is the event:', JSON.stringify(event, null, 2));
};

exports.doctorDetailByIdHandler = function(event, context, callback) {
    doctorDetailById(context, event["id"]);
    console.log('Here is the event:', JSON.stringify(event, null, 2));
};

exports.fuzzyCitySearchHandler = function(event, context, callback) {
    fuzzyCitySearch(context, event["searchString"]);
    console.log('Here is the event:', JSON.stringify(event, null, 2));
};

exports.citySearchHandler = function(event, context, callback) {
    citySearch(context, event["searchString"]);
    console.log('Here is the event:', JSON.stringify(event, null, 2));
};

exports.payerSearchHandler = function(event, context, callback) {
    payerSearch(context, event["searchString"]);
    console.log('Here is the event:', JSON.stringify(event, null, 2));
};


exports.doctorsByBoundsHandler = function(event, context, callback) {
    console.log("vars: bounds, limit");
    console.log('Here is the event:', JSON.stringify(event, null, 2));
    doctorsByBounds(context, event["bounds"], event["payMin"],
                    event["payMax"], event["limit"]);
};

exports.doctorsByBoundsPayersHandler = function(event, context, callback) {
    console.log("vars: bounds, limit");
    console.log('Here is the event:', JSON.stringify(event, null, 2));
    doctorsByBoundsPayers(context, event["bounds"], event["payMin"],
                    event["payMax"], event["exclude_payers"],
                    event["include_payers"], event["limit"]);
};

exports.cityByNameHandler = function(event, context, callback) {
    cityByName(context, event["cityName"]);
    console.log('Here is the event:', JSON.stringify(event, null, 2));
};

//doctorDetailById({succeed: function(){}}, 11, false)
//doctorsByBoundsPayers({succeed: function(){}}, [47.6212163, -122.1860832, 47.6212165, -122.1860830], 10000, 200000, [], [], 20);
//doctorsByBounds({succeed: function(){}}, [42, -120, 45, -80], 50000, 200000, 5);
//cityByName({succeed: function(){}}, "ANDOVER, MA");
//payerSearch({succeed: function(){}}, "PFI");
